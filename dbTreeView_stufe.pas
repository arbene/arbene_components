unit dbTreeView_stufe;
interface

uses
	{Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ComCtrls,WinTypes,  ZMySqlQuery,listen,math;}
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons,  Mask,
	Menus, ImgList,db,
	listen, Spin, vcfi, registry,
	CheckLst, ExtDlgs,math,  zdataset,variants{, ZTransact, ZMySqlTr, ZConnect, ZMySQLCon};

type tedit_tabelle = procedure(sender:tobject) of object;

type
	TdbTreeView_stufe = class(TTreeView)
	private
	  { Private-Deklarationen }
		//TDragOverEvent = procedure(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean) of object;
		fonedit_tabelle : tedit_tabelle;
		ftabelle: tzquery;
		fdatasource: tdatasource;
		ffeldname:string;
		fbereich_s:string;
		fbereich_s2:string;
		fbereich_i:integer;
		fbereich_i2:integer;
		drag:ttreenode;
		fparentfeld, fkindfeld:string;
		procedure liste_loeschen;
		function getbigint(tabelle: tdataset;feldname:string):int64;
		procedure setbigint(tabelle: tdataset;feldname:string;wert:int64);

	protected
	  { Protected-Deklarationen }
	  procedure DragOver( Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);override;


	public
	  { Public-Deklarationen }
		change_allowed:boolean;
	  akt_nummer:integer;
	  constructor create(AOwner: TComponent); override;
	  destructor Destroy; override;
	  procedure dragdrop( Source: TObject; X, Y: Integer);override;
	  procedure neu(s_text:string;command:integer);
	  procedure loeschen;
	  procedure reihenfolge_neu;
	  procedure liste_lesen;
	  procedure liste_lesen_parent(parentFeld,KindFeld:string);
	  procedure tabelle_post;
	  procedure markieren(sindex:integer);
	  procedure alles_markieren(sindex:integer);
	  procedure alles_demarkieren;
	  procedure tabelle_feld_abgleich;
	  procedure Change(Node: TTreeNode);override; //der knoten hat sich ge�nder die Tabelle muss akt. weden.
	  procedure hoch;
	  procedure runter;
	  procedure copy;
	  procedure insert;
	published
	  { Published-Deklarationen }
		property bereich_feldname: string read fbereich_s write fbereich_s;   //zum filtern �ber einen unterbereich
		property bereich_wert: integer read fbereich_i write fbereich_i;     //   s.o.
 		property bereich_feldname2: string read fbereich_s2 write fbereich_s2;   //zum filtern �ber einen unterbereich
		property bereich_wert2: integer read fbereich_i2 write fbereich_i2;     //   s.o.
		property OnEdit_tabelle : tedit_tabelle read fonedit_tabelle write fonedit_tabelle;
		property mysqlquery: tzquery read ftabelle write ftabelle;
		property mysql_feld: string read ffeldname write ffeldname;
		property mysqldatasource :tdatasource read fdatasource write fdatasource;
	  //procedure Changing(Node: TTreeNode; var AllowChange: Boolean); override;

	end;

type
		pnodepointer=^nodewert;
		nodewert = record
		nummer: int64;

end;
procedure Register;

implementation

constructor tdbtreeview_stufe.create(AOwner: TComponent);
begin
inherited;
change_allowed:=true;
end;

destructor tdbtreeview_stufe.Destroy;
begin
liste_loeschen;

inherited;
end;

procedure tdbtreeview_stufe.liste_loeschen;
var i:integer;
begin
	for i:=0 to items.Count-1 do dispose(items[i].data);
	items.Clear;
end;

procedure tdbtreeview_stufe.liste_lesen;
var
		 i_parent, i_index,i: integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
begin
try
	change_allowed:=false;
	fparentfeld:='';
	fkindfeld:='';
	liste_loeschen;
	ftabelle.first;
	if  ftabelle.Eof then exit;
	//erster datensatz von hand
	knoten:=Items.Add(Selected, ftabelle[ffeldname]);      //tree_bereich
	new(daten);
	daten^.nummer:=getbigint(ftabelle,'nummer');
	knoten.Data:=daten;
	knoten.SelectedIndex:=1;
	ftabelle.Next;

	while not ftabelle.eof do
	begin
			i_parent:= ftabelle['parent'];
			i_index:=-1; //#-1
			for i:=0 to Items.count -1 do
			begin
				daten:=Items[i].Data;
				if daten^.nummer=i_parent then
				begin
					i_index:=i;
					break;
				end;
			end;

			if i_index>-1 then
				  knoten:=Items.Addchild(items[i_index], ftabelle[ffeldname])
		  else
					knoten:=Items.Add(items[0], ftabelle[ffeldname]);
			new(daten);
			daten^.nummer:=getbigint(ftabelle,'nummer');
			knoten.Data:=daten;
			knoten.SelectedIndex:=1;
			ftabelle.Next;
		 end;
finally
		change_allowed:=true;
end;
end;

procedure tdbtreeview_stufe.liste_lesen_parent(ParentFeld,KindFeld:string);
var
		 parent_inhalt:variant;
		 knoten,knotenParent: ttreenode;
		 daten:pnodepointer;
begin
try
	change_allowed:=false;
	if assigned(fdatasource) then fdatasource.DataSet:=nil;
	visible:=false;

	fparentfeld:=parentfeld;
	fkindfeld:=kindfeld;
	liste_loeschen;
	ftabelle.first;
	if  ftabelle.Eof then exit;
	//erster datensatz von hand

	if ftabelle[parentFeld]=null then exit;

	  knotenParent:=Items.Add(Selected, ftabelle[parentFeld]);
	  //datumParent:=ftabelle[parentFeld];
    parent_inhalt:=ftabelle[parentFeld];
	  new(daten);
	  daten^.nummer:=-1; //parent-felder
	  knotenparent.Data:=daten;
	  knotenParent.SelectedIndex:=0;

	if ftabelle[KindFeld]<>null then
	begin
	  knoten:=Items.Addchild(KnotenParent, ftabelle[KindFeld]);
	  new(daten);
	  daten^.nummer:=getbigint(ftabelle,'nummer');
	  knoten.Data:=daten;
	  knoten.SelectedIndex:=1;
	end;
	ftabelle.Next;

	while not ftabelle.eof do
	begin
		  if (ftabelle[parentfeld]<>null) and (ftabelle[parentfeld]<>parent_inhalt) then
		  begin
			knotenParent:=Items.Add(knotenParent, ftabelle[parentFeld]);
        parent_inhalt:=ftabelle[parentFeld];
			//datumParent:=ftabelle[parentFeld];
			new(daten);
			daten^.nummer:=-1;
			knotenparent.Data:=daten;
			knotenParent.SelectedIndex:=0;
		  end;
			if ftabelle[KindFeld]<>null then
			begin
			  knoten:=Items.Addchild(KnotenParent, ftabelle[KindFeld]);
			  new(daten);
			  daten^.nummer:=getbigint(ftabelle,'nummer');
			  knoten.Data:=daten;
			  knoten.SelectedIndex:=1;
			end;
			ftabelle.Next;
		 end;
finally
		change_allowed:=true;
		if assigned(fdatasource) then fdatasource.DataSet:=ftabelle;
		visible:=true

end;
end;

procedure tdbtreeview_stufe.copy;
begin
 if (drag=nil)  then
	 begin
	 drag:=selected; // Ausgangswert vom Ziehen
	 end;
end;


procedure tdbtreeview_stufe.insert;
var
	AnItem: TTreeNode;
	AttachMode: TNodeAttachMode;
	parentnum, i_nummer:int64;
	parentdata:pnodepointer;
begin
	anitem:=selected;
	if (drag <> nil) and (drag<>anitem) then
	begin
		AttachMode := nainsert;
	  drag.MoveTo(AnItem, attachmode);
	  parentdata:=drag.data;
	  i_nummer:=parentdata^.nummer;
	  ftabelle.first;
	  //ftabelle.locate('nummer',inttostr(i_nummer),[]);
    ftabelle.locate('nummer',(i_nummer),[]);
	  ftabelle.edit;
	  setbigint(ftabelle,'parent',parentnum);

	  ftabelle.Post;
	  reihenfolge_neu;
	  drag:=nil;
 end;
end;

procedure tdbtreeview_stufe.dragdrop(Source: TObject; X, Y: Integer);
var
	AnItem: TTreeNode;
	AttachMode: TNodeAttachMode;
	HT: THitTests;
	parentnum, i_nummer:int64;
	parentdata:pnodepointer;
begin
	HT := GetHitTestInfoAt(X, Y);
	AnItem := GetNodeAt(X, Y);
	attachMode:=naAddChild; //zur Definition
 if source is ttreeview then
 begin
	//if (TreeView1.Selected = nil) or (treeview1.selected=anitem) then Exit;
	if (drag <> nil) and (drag<>anitem) then
	begin
		  if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent, htonright, htonlabel,htonbutton] <> HT) then  //Mengenlehre
			 begin
			  if (anitem<>nil) and (anitem.parent<>nil) then
					begin
						parentdata:=anitem.parent.data;
						 parentnum:=parentdata^.nummer;
					end
					else parentnum:=0;//#-1
			  if (htOnItem in HT) or (htOnIcon in HT) or(htonlabel in ht) then
				begin
					AttachMode := naAddChild;
					parentdata:=anitem.data;
					parentnum:=parentdata^.nummer;
				end
			  else if (htNowhere in ht) or (htonright in ht) then
					begin
					AttachMode := naAdd;

					end
			  else if (htOnIndent in HT) or (htonbutton in HT) then
				 begin
					AttachMode := naInsert;
				 end;

			  drag.MoveTo(AnItem, AttachMode);

			  //bereich_parent_eintragen(drag,parentnum);
			  parentdata:=drag.data;
			  i_nummer:=parentdata^.nummer;
			  ftabelle.first;
			  //ftabelle.locate('nummer',inttostr(i_nummer),[]);
        ftabelle.locate('nummer',(i_nummer),[]);
			  ftabelle.edit;
			  setbigint(ftabelle,'parent',parentnum);

			  ftabelle.Post;
			  reihenfolge_neu;
			 end;
	 end;
	end;

	drag:=nil;
	inherited ;
end;

procedure tdbtreeview_stufe.DragOver(Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);
begin
 if (drag=nil) and (source is tdbtreeview_stufe) then
	 begin
	 drag:=tdbtreeview_stufe(source).selected; // Ausgangswert vom Ziehen
	 end;
	 Accept := (source is tdbtreeview_stufe);
end;

procedure tdbtreeview_stufe.reihenfolge_neu;
//neueinlesen der Reihenfolge
var
i: integer;
knoten, parent: ttreenode;
daten:pnodepointer;
i_nummer, p_nummer:int64;
begin
	 //datamodul.q_bereich.IndexName:='';
	 if ftabelle.FindField('reihenfolge')=nil then exit;
	 //k_alt:=selected.absoluteindex;
	 for i:=0 to items.Count-1 do
	 begin
		knoten:=items[i];
		daten:=knoten.Data;
		i_nummer:=daten^.nummer;
		if assigned(knoten.parent) then
		begin
		  parent:=knoten.Parent;
		  daten:=parent.Data;
		  p_nummer:=daten^.nummer;
		end
		else p_nummer:=0;
		ftabelle.first;
		//ftabelle.locate('nummer', inttostr(i_nummer),[]);
    ftabelle.locate('nummer', (i_nummer),[]);
		ftabelle.edit;
		ftabelle['reihenfolge']:=i;
		setbigint(ftabelle,'parent',p_nummer);
		ftabelle.Post;
	 end;
	 //selected:=items[k_alt];
	 change(selected);

inherited ;
end;

procedure tdbtreeview_stufe.Change(Node: TTreeNode);
var
i_nummer: int64;
daten:pnodepointer;
begin
try
	 if (not change_allowed) or (node=nil) then exit;

  daten:=node.Data;
  i_nummer:=daten^.nummer;
  akt_nummer:=i_nummer;
  //ftabelle.Locate('nummer',inttostr(i_nummer),[]);
  ftabelle.Locate('nummer',(i_nummer),[]);
  node.Expand(false);

finally
	inherited;
end
end;


procedure tdbtreeview_stufe.neu(s_text:string;command:integer);
var
nodedata,parentdata:pnodepointer;
parentnum:integer;
knoten:TTreeNode;
begin
try
change_allowed:=false;
	if items.Count=0 then command:=1; //neuer Basisknoten

 if (Selected=nil) and (items.Count>0) then
	begin
		showmessage('Bitte zuerst ausw�hlen.');
		change_allowed:=true;
		exit;
	end;

	try
		//nodedata:=Selected.data;
		//bernum:=nodedata^.nummer;
	 except
		//bernum:=0;
	 end;

	  case command of
	  1:
		begin
			knoten:= Items.insert( Selected, '');
		end;
		2: begin
			 knoten:= Items.Addchildfirst( Selected, '');
			  Selected.Expand(false);
			end;

	  end;  //case
		Selected:=knoten;
	  try   //neu bei 1 oder 2
		if knoten.parent<>nil then
		 begin
			parentdata:=knoten.Parent.data;
			parentnum:=parentdata^.nummer;
			end
		 else
		 parentnum:=0  ;
	  except
		parentnum:=0;
	  end;
	  ftabelle.append;
	  if ftabelle.FindField(ffeldname)<>nil then ftabelle[ffeldname]:=s_text;
	  if ftabelle.FindField('parent')<>nil then ftabelle['parent']:=parentnum;
	  if ftabelle.FindField('reihenfolge')<>nil then ftabelle['reihenfolge']:=knoten.absoluteindex;
	  if ftabelle.FindField(fbereich_s)<>nil then ftabelle[fbereich_s]:=fbereich_i;
	  if ftabelle.FindField(fbereich_s2)<>nil then ftabelle[fbereich_s2]:=fbereich_i2;

	  //neuer_datensatz(datamodul.q_bereich,[nil,knoten.text,parentnum,knoten.absoluteindex,0, '','']);
	  ftabelle.post;

	  //richspeichern(datamodul.q_bereich,form_bereich.richedit_bereich);
	  new(nodedata);
	  nodedata.nummer:=getbigint(ftabelle,'nummer');
	  knoten.Data:=nodedata;
	  knoten.SelectedIndex:=1;
	  knoten.Text:=ftabelle[ffeldname];

	  //bereich_edit;          //hier aufruf des formulars
    if assigned(fonedit_tabelle) then fonedit_tabelle(self);
    tabelle_feld_abgleich;

	  reihenfolge_neu;
finally
	change_allowed:=true;
	change(selected);
end
end;


procedure tdbtreeview_stufe.tabelle_post;
var
nodedata:pnodepointer;
begin
	if ftabelle.State in [dsedit,dsinsert] then
	begin
		ftabelle.post;
     ftabelle.Refresh;
		nodedata:=Selected.data;
		nodedata.nummer:=getbigint(ftabelle,'nummer');
     selected.data:=nodedata;
		selected.Text:=ftabelle[ffeldname];
	end;
end;

procedure tdbtreeview_stufe.tabelle_feld_abgleich;
var
nodedata:pnodepointer;
begin
	if ftabelle.State in [dsedit,dsinsert] then
	begin
		nodedata:=Selected.data;
		nodedata.nummer:=getbigint(ftabelle,'nummer');
		selected.Text:=ftabelle[ffeldname];
	end;
end;



procedure tdbtreeview_stufe.loeschen;
var
nodedata:pnodepointer;
bernum:int64;
parent:TTreeNode;
begin
	if (Selected=nil) and (items.Count>0) then
	begin
		showmessage('Bitte zuerst ausw�hlen.');
		exit;
	end;
 if Selected.HasChildren then
	 begin
		showmessage('Zuerst untergeordnete Eintr�ge l�schen.');
		exit;
	 end;
	if MessageDlg('L�schen des Eintrags?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then
	begin
		exit;
	end;
try
	change_allowed:=false;
	if items.Count=0 then exit;

	try
		parent:=selected.parent;
		nodedata:=Selected.data;
		bernum:=nodedata^.nummer;
		dispose(nodedata);
	 except
		bernum:=0;
	 end;
		if getbigint(ftabelle,'nummer')=bernum then
		begin
			ftabelle.delete;
			Selected.Delete;
 	    if fparentfeld='' then reihenfolge_neu       //bei laborlist mit datum als parent
		 else
		 begin
			if not parent.HasChildren then
			begin
				nodedata:=parent.data;
				//bernum:=nodedata^.nummer;
				dispose(nodedata);
				parent.Delete;
			end;
		 //liste_lesen_parent(fparentfeld,fkindfeld);
		 end;
	 end;

finally
	change_allowed:=true;
	change(selected);
end;
end;

procedure tdbtreeview_stufe.markieren(sindex:integer);  //markiert untergeordnete knoten
var i:integer;
knoten:ttreenode;

function hasparent(p,f:integer):boolean;
var j:integer;
begin
	 j:=f;
	 while (items[j].parent<>nil) and(j>p) do
	 begin
		 j:=items[j].parent.absoluteindex;
	 end;
	 if j=p then result:=true else result:=false;

end;
  
begin //main
	selected.StateIndex:=sindex;
	knoten:=selected.parent;
	while knoten<>nil do
	begin
		knoten.StateIndex:=sindex;
		knoten:=knoten.Parent;
	end;
	i:=selected.absoluteindex;
	//knoten:=selected;
	inc(i);
	//while (i<items.count) and(items[i].parent=selected) do
  while (i<items.count) and hasparent(selected.absoluteindex,i) do
	begin
	  items[i].StateIndex:=sindex;
	  inc(i);
	end;
end;


procedure tdbtreeview_stufe.alles_markieren(sindex:integer);
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=sindex;
	end;
end;

procedure tdbtreeview_stufe.alles_demarkieren;
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=-1;
	end;
end;

procedure tdbtreeview_stufe.runter;
begin
	if selected.absoluteindex >0 then  selected:=items[selected.absoluteindex-1];
end;

procedure tdbtreeview_stufe.hoch;
begin
	if selected.absoluteindex <items.count-1 then  selected:=items[selected.absoluteindex+1];
end;

function tdbtreeview_stufe. getbigint(tabelle: tdataset;feldname:string):int64;
var r:string;
begin
try
result:=0;
if tabelle.findfield(feldname)<>nil then
   r:= tabelle.FindField(feldname).AsString;
except
end;
if r='' then result:=0 else result:=strtoint64(r);
end;

procedure tdbtreeview_stufe.setbigint(tabelle: tdataset;feldname:string;wert:int64);
begin
try
if tabelle.findfield(feldname)<>nil then //tabelle[feldname]:=inttostr(wert);
   tabelle.FindField(feldname).AsString := inttostr(wert);
		//tabelle.SetFieldData(tabelle.fieldbyname(feldname),@wert)
except
end;
end;

procedure Register;
begin
	RegisterComponents('Datensteuerung', [tdbtreeview_stufe]);
end;

end.
