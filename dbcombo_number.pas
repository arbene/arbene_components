unit dbcombo_number;

interface

uses Windows, SysUtils, Messages, Classes, Controls, Forms,
		Graphics, Menus, StdCtrls, ExtCtrls, Mask, Buttons, ComCtrls, Db, dbtables, DBCtrls,
	ZConnection, ZAbstractRODataset, ZAbstractDataset, ZDataset,variants;


{ TDBCombo_num }

type
		pint_list=^int_list_wert;
		int_list_wert= record
				int1: integer;
end;

type
 tint_list = class(tlist)
		function lesen(index: integer): integer;
		procedure hinzufuegen(i1: integer);
		procedure clear;
		procedure loeschen(index: integer);
		function suchen(i1: integer): integer;
		destructor free;
	end;


	TDBCombo_num = class(TCustomComboBox)
	private
	  Fnumbers: tint_list;
	  FDataLink: TFieldDataLink;
	  FPaintControl: TPaintControl;
    falle:boolean;
    fkeines:boolean;
	  fconnection:TZconnection;
	  fzfield_text:string;
	  fzfield_index:string;
	  fztab_name:string;
	  fzorder: string;
	  fzstorno:boolean;
	  procedure DataChange(Sender: TObject);
	  procedure EditingChange(Sender: TObject);
	  function GetComboText: integer;
	  function GetDataField: string;
	  function GetDataSource: TDataSource;
	  function GetField: TField;
	  function GetReadOnly: Boolean;
	  procedure SetComboText(const Value: integer);
	  procedure SetDataField(const Value: string);
	  procedure SetDataSource(Value: TDataSource);
	  procedure SetEditReadOnly;
	  procedure SetItems(Value: TStrings);
	  procedure SetReadOnly(Value: Boolean);
	  procedure UpdateData(Sender: TObject);
	  procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
	  procedure CMExit(var Message: TCMExit); message CM_EXIT;
	  procedure CMGetDataLink(var Message: TMessage); message CM_GETDATALINK;
	  procedure WMPaint(var Message: TWMPaint); message WM_PAINT;

	protected
	  procedure Change; override;
	  procedure Click; override;
	  procedure ComboWndProc(var Message: TMessage; ComboWnd: HWnd;
		 ComboProc: Pointer); override;
	  procedure CreateWnd; override;
	  procedure DropDown; override;
	  procedure KeyDown(var Key: Word; Shift: TShiftState); override;
	  procedure KeyPress(var Key: Char); override;
	  procedure Loaded; override;
	  procedure Notification(AComponent: TComponent;
		 Operation: TOperation); override;
	  procedure SetStyle(Value: TComboboxStyle); override;
	  procedure WndProc(var Message: TMessage); override;
	public
	  constructor Create(AOwner: TComponent); override;
	  destructor Destroy; override;
	  procedure hinzufuegen(text: string;i1:integer);
	  function getnummer:integer;
	  procedure loeschen;
	  procedure lookup_laden;
	  function last_int:integer;
	  property Field: TField read GetField;
	  property Text;

	published
	  property Style; {Must be published before Items}
	  property Color;
	  property Ctl3D;
    property alle_einfuegen:boolean read falle write falle;
    property keines_einfuegen:boolean read fkeines write fkeines;
	  property DataField: string read GetDataField write SetDataField;
	  property DataSource: TDataSource read GetDataSource write SetDataSource;
	  property zconnection: TZconnection read fconnection write fconnection ;
	  property ztab_name :string read fztab_name write fztab_name;
	  property zfield_text : string read fzfield_text write fzfield_text;
	  property zfield_index : string read fzfield_index write fzfield_index;
	  property zorder: string read fzorder write fzorder;
	  property zstorno:boolean read fzstorno write fzstorno;
	  property DragMode;
	  property DragCursor;
	  property DropDownCount;
	  property Enabled;
	  property Font;
	  property ImeMode;
	  property ImeName;
	  property ItemHeight;
	  property Items write SetItems;
	  property ParentColor;
	  property ParentCtl3D;
	  property ParentFont;
	  property ParentShowHint;
	  property PopupMenu;
	  property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
	  property ShowHint;
	  property Sorted;
	  property TabOrder;
	  property TabStop;
	  property Visible;
	  property OnChange;
	  property OnClick;
	  property OnDblClick;
	  property OnDragDrop;
	  property OnDragOver;
	  property OnDrawItem;
	  property OnDropDown;
	  property OnEndDrag;
	  property OnEnter;
	  property OnExit;
	  property OnKeyDown;
	  property OnKeyPress;
	  property OnKeyUp;
	  property OnMeasureItem;
	  property OnStartDrag;
	end;




procedure Register;

implementation


destructor tint_list.free;
begin
	clear;

end;


procedure tint_list.loeschen(index: integer);
var
	listenwert: pint_list;
begin
	listenwert:= items[index];
	delete(index);
	dispose(listenwert);
end;

procedure tint_list.clear;
begin
	while count>0 do loeschen(count-1);
	inherited clear;
end;


function tint_list.lesen(index: integer):integer;
var
	listenwert: pint_list;
begin
	if (index>=0) and (index< count) then
	begin
		listenwert:= items[index];
		result:=listenwert^.int1;
	end
	else
	result:=-1;
end;

procedure tint_list.hinzufuegen(i1: integer);
var
	neuereintrag: pint_list;
begin
	new(neuereintrag);
	neuereintrag^.int1:=i1;
	add(neuereintrag);
end;


function tint_list.suchen(i1: integer): integer;
var
listenwert: pint_list;
i: integer;
begin
	result:=-1;
	if (i1>=0) and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.int1 =i1 then
			  begin
				result:=i;
				break;
			  end;
			end;
	end;
end;



{ TDBCombo_num }

constructor TDBCombo_num.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	ControlStyle := ControlStyle + [csReplicatable];
	FDataLink := TFieldDataLink.Create;
	FDataLink.Control := Self;
	FDataLink.OnDataChange := DataChange;
	FDataLink.OnUpdateData := UpdateData;
	FDataLink.OnEditingChange := EditingChange;
	FPaintControl := TPaintControl.Create(Self, 'COMBOBOX');
	fnumbers:=tint_list.Create ;
end;

destructor TDBCombo_num.Destroy;
begin
	fnumbers.free;
	FPaintControl.Free;
	FDataLink.Free;
	FDataLink := nil;
	inherited Destroy;
end;


procedure TDBCombo_num.hinzufuegen(text: string;i1:integer);

begin
items.Add(text);
fnumbers.hinzufuegen(i1);
end;


procedure TDBCombo_num.loeschen;
begin

	items.Clear;
  fnumbers.clear;
end;


procedure tdbcombo_num.lookup_laden;
var
q:TZQuery;
query,t:string;
i:integer;
begin

query:=format('select %s,%s from %s ',[fzfield_index,fzfield_text,fztab_name]);
if fzstorno then query:=query+' where storno=0';
if fzorder<>'' then query:=query+' order by '+fzorder;

q:=tzquery.create(self);
q.connection:=fconnection;
q.Sql.Text:=query;
q.Open;

q.First;
loeschen;
if falle then hinzufuegen('alle',0);
if fkeines then hinzufuegen('keine Auswahl',-1); // vorher 0 statt -1
while not q.eof do
begin
	if q[fzfield_text]<>null then t:=q.findfield(fzfield_text).asstring else t:='';
	if q[fzfield_index]<>null then i:=q[fzfield_index] else i:=-1;
	hinzufuegen(t, i);
	q.next;
end;

q.Close;
q.free;
datachange(self);
end;




function tdbcombo_num.last_int:integer;
begin
	result:=fnumbers.lesen(fnumbers.count-1);
end;


procedure TDBCombo_num.Loaded;
begin
	inherited Loaded;
	if (csDesigning in ComponentState) then DataChange(Self);
end;

procedure TDBCombo_num.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
try
	inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and
    (AComponent = DataSource) then DataSource := nil;
except
end;    
end;

procedure TDBCombo_num.CreateWnd;
begin
	inherited CreateWnd;
  SetEditReadOnly;
end;

procedure TDBCombo_num.DataChange(Sender: TObject);
begin
	if DroppedDown then Exit;
	if FDataLink.Field <> nil then
	  SetComboText(FDataLink.Field.asinteger  )
	else
	  if csDesigning in ComponentState then
		 SetComboText(-1)
	  else
		 SetComboText(-1);
end;

procedure TDBCombo_num.UpdateData(Sender: TObject);
begin
	//FDataLink.Field.AsInteger  := GetComboText;    //??
end;

procedure TDBCombo_num.SetComboText(const Value: integer);
var
	I: Integer;
	Redraw: Boolean;
begin
if value<0 then
begin
   itemindex:=-1; //sonst bleibt das feld ggf gef�llt
   exit;
end;

if name='DBComboBox_u_aus'then
begin
	i:=-2;
end;
	if Value <> GetComboText then
	begin
	 if Style <> csDropDown then
	  begin
		 Redraw := (Style <> csSimple) and HandleAllocated;
		 if Redraw then SendMessage(Handle, WM_SETREDRAW, 0, 0);
		 try
			if Value = -1 then I := -1 else I := fnumbers.suchen(value);//  Items. IndexOf(Value);
			ItemIndex := I;
		 finally
			if Redraw then
			begin
			  SendMessage(Handle, WM_SETREDRAW, 1, 0);
			  Invalidate;
			end;
		 end;
		 if I >= 0 then Exit;
	  end;

	  if Style in [csDropDown, csSimple] then
	  begin
		I := fnumbers.suchen(value);
		itemindex:=I;
		//Text := items[i];
	  end;
	end;
end;

function TDBCombo_num.GetComboText: integer;
begin
	if Style in [csDropDown, csSimple] then Result :=fnumbers.lesen(itemindex)
	else
	begin
	  result:=fnumbers.lesen(itemindex)
	end;
end;

function TDBCombo_num.getnummer:integer;
begin
	result:=fnumbers.lesen(itemindex);
end;

//############################################## hier scheiben vom Feld
procedure TDBCombo_num.Change;
begin
	FDataLink.Edit;  //integer schreiben??
	inherited Change;
    FDataLink.Field.AsInteger  := GetComboText;   //2015-11-25

	FDataLink.Modified;
end;

procedure TDBCombo_num.Click;
begin
	//FDataLink.Edit;
	inherited Click;
  //FDataLink.Field.AsInteger  := GetComboText;        wird in change aufgefuren
	//FDataLink.Modified;
end;

procedure TDBCombo_num.DropDown;
begin
	inherited DropDown;
end;

function TDBCombo_num.GetDataSource: TDataSource;
begin
	Result := FDataLink.DataSource;
end;

procedure TDBCombo_num.SetDataSource(Value: TDataSource);
begin
	FDataLink.DataSource := Value;
	if Value <> nil then Value.FreeNotification(Self);
end;

function TDBCombo_num.GetDataField: string;
begin
	Result := FDataLink.FieldName;
end;

procedure TDBCombo_num.SetDataField(const Value: string);
begin
	FDataLink.FieldName := Value;
end;

function TDBCombo_num.GetReadOnly: Boolean;
begin
	Result := FDataLink.ReadOnly;
end;

procedure TDBCombo_num.SetReadOnly(Value: Boolean);
begin
	FDataLink.ReadOnly := Value;
end;

function TDBCombo_num.GetField: TField;
begin
	Result := FDataLink.Field;
end;

procedure TDBCombo_num.KeyDown(var Key: Word; Shift: TShiftState);
var i: integer;
	  b:string;
begin
	if key in [32..255] then  //springen auf entsprechendes Feld
	begin
	  b:=chr(key);
	  for i:=0 to items.count-1 do
	  begin
		if copy(lowercase(items[i]),1,1)=lowercase(b) then
		begin
			itemindex:=  i;

			//application.ProcessMessages;
			change;
			exit;
			//break;
		end;
	  end;
	end;

	if key in  [13] then
	begin
		 change;
		 exit;
	end;


	inherited KeyDown(Key, Shift);


	if Key in [VK_BACK, VK_DELETE, VK_UP, VK_DOWN, 32..255] then
	begin
	  if not FDataLink.Edit and (Key in [VK_UP, VK_DOWN]) then
		 Key := 0;
	end;


end;

procedure TDBCombo_num.KeyPress(var Key: Char);
begin
	inherited KeyPress(Key);

	if (Key in [#32..#255]) and (FDataLink.Field <> nil) and
	  not FDataLink.Field.IsValidChar(Key) then
	begin
	  MessageBeep(0);
	  Key := #0;
	end;
	case Key of
	  ^H, ^V, ^X, #32..#255:
		 FDataLink.Edit;
	  #27:
		 begin
			FDataLink.Reset;
			SelectAll;
		 end;
	end;
end;

procedure TDBCombo_num.EditingChange(Sender: TObject);
begin
	SetEditReadOnly;
end;

procedure TDBCombo_num.SetEditReadOnly;
begin
	if (Style in [csDropDown, csSimple]) and HandleAllocated then
	  SendMessage(EditHandle, EM_SETREADONLY, Ord(not FDataLink.Editing), 0);
end;

procedure TDBCombo_num.WndProc(var Message: TMessage);
begin
	if not (csDesigning in ComponentState) then
	  case Message.Msg of
		 WM_COMMAND:
			if TWMCommand(Message).NotifyCode = CBN_SELCHANGE then
			  if not FDataLink.Edit then
			  begin
				 if Style <> csSimple then
					PostMessage(Handle, CB_SHOWDROPDOWN, 0, 0);
				 Exit;
			  end;
		 CB_SHOWDROPDOWN:
			if Message.WParam <> 0 then FDataLink.Edit else
			  if not FDataLink.Editing then DataChange(Self); {Restore text}
		 WM_CREATE,
		 WM_WINDOWPOSCHANGED,
		 CM_FONTCHANGED:
			FPaintControl.DestroyHandle;
	  end;
	inherited WndProc(Message);
end;

procedure TDBCombo_num.ComboWndProc(var Message: TMessage; ComboWnd: HWnd;
	ComboProc: Pointer);
begin
	if not (csDesigning in ComponentState) then
	  case Message.Msg of
		 WM_LBUTTONDOWN:
			if (Style = csSimple) and (ComboWnd <> EditHandle) then
			  if not FDataLink.Edit then Exit;
	  end;
	inherited ComboWndProc(Message, ComboWnd, ComboProc);
end;

procedure TDBCombo_num.CMEnter(var Message: TCMEnter);
begin
	inherited;
	if SysLocale.FarEast and FDataLink.CanModify then
	  SendMessage(EditHandle, EM_SETREADONLY, Ord(False), 0);
end;

procedure TDBCombo_num.CMExit(var Message: TCMExit);
begin
	try
	  FDataLink.UpdateRecord;
	except
	  SelectAll;
	  SetFocus;
	  raise;
	end;
	inherited;
end;

procedure TDBCombo_num.WMPaint(var Message: TWMPaint);
var
	S: string;
	R: TRect;
	P: TPoint;
	Child: HWND;
begin
	if csPaintCopy in ControlState then
	begin
	  if FDataLink.Field <> nil then S := FDataLink.Field.Text else S := '';
	  if Style = csDropDown then
	  begin
		 SendMessage(FPaintControl.Handle, WM_SETTEXT, 0, Longint(PChar(S)));
		 SendMessage(FPaintControl.Handle, WM_PAINT, Message.DC, 0);
		 Child := GetWindow(FPaintControl.Handle, GW_CHILD);
		 if Child <> 0 then
		 begin
			Windows.GetClientRect(Child, R);
			Windows.MapWindowPoints(Child, FPaintControl.Handle, R.TopLeft, 2);
			GetWindowOrgEx(Message.DC, P);
			SetWindowOrgEx(Message.DC, P.X - R.Left, P.Y - R.Top, nil);
			IntersectClipRect(Message.DC, 0, 0, R.Right - R.Left, R.Bottom - R.Top);
			SendMessage(Child, WM_PAINT, Message.DC, 0);
		 end;
	  end else
	  begin
		 SendMessage(FPaintControl.Handle, CB_RESETCONTENT, 0, 0);
		 if Items.IndexOf(S) <> -1 then
		 begin
			SendMessage(FPaintControl.Handle, CB_ADDSTRING, 0, Longint(PChar(S)));
			SendMessage(FPaintControl.Handle, CB_SETCURSEL, 0, 0);
		 end;
		 SendMessage(FPaintControl.Handle, WM_PAINT, Message.DC, 0);
	  end;
	end else
	  inherited;
end;

procedure TDBCombo_num.SetItems(Value: TStrings);
begin
	Items.Assign(Value);
	DataChange(Self);
end;

procedure TDBCombo_num.SetStyle(Value: TComboboxStyle);
begin
	if (Value = csSimple) and Assigned(FDatalink) and FDatalink.DatasourceFixed then
	  DatabaseError('SNotReplicatable');
	inherited SetStyle(Value);
end;

procedure TDBCombo_num.CMGetDatalink(var Message: TMessage);
begin
	Message.Result := Integer(FDataLink);
end;




procedure Register;
begin
	RegisterComponents('Datensteuerung', [TDBCombo_num]);
end;

end.
