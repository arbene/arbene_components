 unit listen;

interface
uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, Buttons, ExtCtrls;

type
		pintegerliste=^intlistenwert;
		intlistenwert= record
				int1: integer;
				int2: integer;
		 end;

type
		pstringintegerliste=^stringintegerlistenwert;
		stringintegerlistenwert= record
				str1: string;
				int1: integer;
		 end;

type
		pstringintegerintegerliste=^stringintegerintegerlistenwert;
		stringintegerintegerlistenwert= record
				str1: string;
				int1: integer;
           int2: integer;
		 end;


type pstrstrliste=^pstrstrlistenwert;
		pstrstrlistenwert= record
			str1: string;
			str2: string;
			str3: string;
			str4: string;
		 end;

 type pint_stringlist_liste=^intstrlistenwert;
		intstrlistenwert= record
			int1: integer;
			stringlist: tstringlist;
		 end;

 type pstr_int_stringlist_liste=^strintstrlistenwert;
		 strintstrlistenwert= record
			str1: string;
			int1: integer;
			stringlist: tstringlist;
		 end;

 type rsuchergebnis =record
			index: integer;
			knoten: integer;
			anzahl: integer;
	end;


type
 tintegerlist = class(tlist)
		function suchen(i_knoten: integer): rsuchergebnis;
		procedure aktualisieren(i_knoten: integer; i_anzahl: integer);
		procedure hinzufuegen(knoten, anzahl: integer);
		function auslesen : tstringlist;
		procedure normieren(ngrammanzahl: integer);
		procedure listeanfuegen(liste: tintegerlist);
		procedure clear;
		procedure loeschen(index: integer);
    destructor destroy; override;
	end;

tint_stringlist_list=class (tlist)
	 procedure hinzufuegen(knoten: integer; strings: tstringlist);
	 function listeausgabe(knoten: integer): tstringlist;
	 procedure clear;
   destructor destroy;  override;
end;

tstr_int_stringlist_list=class(tlist)
	 procedure hinzufuegen(str: string);
	 procedure aktualisieren(str1: string; int: integer;  str2: string; b_intaddieren: boolean);
	 function str1_listeausgeben: tstringlist;
	 function str2_listeausgeben(str: string): tstringlist;
	 procedure clear;
   destructor destroy;   override;
end;


type
    tstringintegerlist = class(tlist)
		function suchen(s_wort: string): integer;
		function suchen_int(s_wort: string): integer;
		procedure aktualisieren(s_wort: string; i_anzahl: integer);
		procedure int_aktualisieren(s_wort : string;i_anzahl: integer);
		procedure hinzufuegen(s_wort: string; i_anzahl: integer);
		function auslesen : tstringlist;
		procedure clear;
		destructor destroy;override;
end;

type
 tstrstrlist = class(tlist)
		function aktualisieren( s_text1, s_text2, s_text3, s_text4: string): string;
		procedure hinzufuegen(s_text1, s_text2, s_text3, s_text4: string);
     procedure auslesen(nummer: integer;var text1, text2, text3, text4: string);
		procedure loeschen(s_text:string);
		procedure clear;
    destructor destroy;  override;
end;

implementation

destructor tintegerlist.destroy;
begin
	clear;
  inherited;
end;


procedure tintegerlist.loeschen(index: integer);
var
	listenwert: pintegerliste;
begin
	listenwert:= items[index];
	delete(index);
	dispose(listenwert);
end;

procedure tintegerlist.clear;
begin
	while count>0 do loeschen(count-1);
	inherited;// clear;
end;


function tintegerlist.suchen(i_knoten: integer): rsuchergebnis;
var
listenwert: pintegerliste;
i: integer;
begin
	result.knoten:=i_knoten;
	result.index:=-1;
	if (result.knoten>0) and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.int1 =result.knoten then
			  begin
				result.index:=i;
				result.knoten:=listenwert^.int1;
				result.anzahl:= listenwert^.int2;
				break;
			  end;

			end;

	end;
end;


procedure tintegerlist.listeanfuegen(liste: tintegerlist);
var i,i_knoten, i_anzahl: integer;
	  listenwert: pintegerliste;
begin
for i:=0 to liste.Count-1 do
begin
	listenwert:=liste.Items[i];
	i_knoten:=listenwert.int1;
	i_anzahl:=listenwert.int2;
	aktualisieren(i_knoten,i_anzahl);
end;
end;


procedure tintegerlist.aktualisieren(i_knoten : integer;i_anzahl: integer);
var
i,index,knoten,anzahl: integer;
listenwert: pintegerliste;
begin
	index:=-1;
	knoten:=i_knoten;
	anzahl:=i_anzahl;
	if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
			  listenwert:=items[i];
			  if listenwert^.int1 =knoten then
				begin  //#3
					index:=i;
					listenwert^.int2:=anzahl+listenwert^.int2;
					items[i]:=listenwert;
					break;
				end;   //#3
			end;      //#2
 end;      //#1
		if index=-1 then hinzufuegen(knoten, anzahl);

end;

procedure tintegerlist.hinzufuegen(knoten, anzahl: integer);
var
	neuereintrag: pintegerliste;
begin
	new(neuereintrag);
	neuereintrag^.int1:=knoten;
	neuereintrag^.int2:=anzahl;
	add(neuereintrag);
end;


function tintegerlist.auslesen : tstringlist;
var
	s_knoten, s_anzahl : string;
	i: integer;
	listenwert: pintegerliste;
begin
result:=tstringlist.create;
if count>0 then
	  begin // #1  gef�llte liste
			for i:=count-1 downto 0 do
			begin //#2
				listenwert:=items[i];
				s_knoten:=inttostr(listenwert^.int1);
				s_anzahl:=inttostr(listenwert^.int2);
				result.add(s_knoten+' , '+s_anzahl);
			end;      //#2
end
end;




procedure tintegerlist.normieren(ngrammanzahl: integer);
var                 //gebraucht um die knotenliste
	i: integer;
	listenwert: pintegerliste;
begin
if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
				listenwert:=items[i];
				listenwert^.int2:=round(100*(listenwert^.int2 /ngrammanzahl));
				items[i]:=listenwert;
			end;      //#2
end;
end;

//####################################################################



procedure tstringintegerlist.clear;
var
listenwert: pstringintegerliste;
begin
 while count>0 do begin
	listenwert:= items[count-1];
	delete(count-1);
	dispose(listenwert);
 end;
 inherited;// clear;
end;


function tstringintegerlist.suchen(s_wort: string): integer;
var
i, index: integer;
eintrag: string;
listenwert: pstringintegerliste;
begin
	eintrag:=s_wort;
	index:=-1;
	if (eintrag<>'') and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.str1 =s_wort then
			  begin
				index:=i;
				break;
			  end;
			end;
	end;
	result:=index;
end;

function tstringintegerlist.suchen_int(s_wort: string): integer;
var
i: integer;
eintrag: string;
listenwert: pstringintegerliste;
begin
try
  eintrag:=s_wort;
  result:=-1;
	if (eintrag<>'') and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.str1 =s_wort then
			  begin
				result:=listenwert^.int1;
				break;
			  end;
			end;
	end;
except
	result:=-1;
end;
end;


procedure tstringintegerlist.aktualisieren(s_wort : string;i_anzahl: integer);
var
i,index: integer;
listenwert: pstringintegerliste;
begin
	index:=-1;
	if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
			  listenwert:=items[i];
			  if listenwert^.str1 =s_wort then
				begin  //#3
					index:=i;
					listenwert^.int1:=i_anzahl+listenwert^.int1;
					items[i]:=listenwert;
					break;
				end;   //#3
			end;      //#2
 end;      //#1
 if index=-1 then hinzufuegen(s_wort, i_anzahl);
end;

procedure tstringintegerlist.int_aktualisieren(s_wort : string;i_anzahl: integer);
var
i,index: integer;
listenwert: pstringintegerliste;
begin
	index:=-1;
	if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
			  listenwert:=items[i];
			  if listenwert^.int1 =i_anzahl then
				begin  //#3
					index:=i;
					listenwert^.str1:=s_wort;
					items[i]:=listenwert;
					break;
				end;   //#3
			end;      //#2
 end;      //#1
 if index=-1 then hinzufuegen(s_wort, i_anzahl);
end;

procedure tstringintegerlist.hinzufuegen(s_wort: string; i_anzahl: integer);
var
	neuereintrag: pstringintegerliste;
begin
	new(neuereintrag);
	neuereintrag^.str1:=s_wort;
	neuereintrag^.int1:=i_anzahl;
	add(neuereintrag);
end;


function tstringintegerlist.auslesen : tstringlist;
var
	s_eintrag, s_anzahl : string;
	i: integer;
	listenwert: pstringintegerliste;
begin
result:=tstringlist.create;
if count>0 then
	  begin // #1  gef�llte liste
			for i:= count-1 downto 0 do
			begin //#2
				listenwert:=items[i];
				s_eintrag:=listenwert^.str1;
				s_anzahl:=copy(inttostr(listenwert^.int1)+'     ',1,4);
				//result.add(s_anzahl+ ', '+s_eintrag);
				result.add(s_eintrag);
			end;      //#2
		end;
end;

destructor tstringintegerlist.destroy;
begin
	clear;
  inherited;
end;


//########################################################################

procedure tint_stringlist_list.hinzufuegen(knoten: integer; strings: tstringlist);
var
	neuereintrag: pint_stringlist_liste;
begin
	new(neuereintrag);
	neuereintrag^.int1:=knoten;
	neuereintrag^.stringlist:=tstringlist.create;
	neuereintrag^.stringlist.AddStrings(strings); //:=liste;
	add(neuereintrag);
end;


procedure tint_stringlist_list.clear;
var
listenwert: pint_stringlist_liste;
begin
 while count>0 do begin
	listenwert:= items[count-1];
	delete(count-1);
	listenwert^.stringlist.Free;
	dispose(listenwert);
 end;
inherited;// clear;
end;


function tint_stringlist_list.listeausgabe(knoten: integer): tstringlist;
var
liste: tstringlist;
listenwert: pint_stringlist_liste;
i: integer;
begin
result:=tstringlist.create;
	if (knoten>0) and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.int1 =knoten then
			  begin
				liste:=listenwert^.stringlist;
				result.AddStrings(liste);
				break;
			  end;
			end;
	end;
end;

destructor tint_stringlist_list.destroy;
begin
	clear;
  inherited ;
end;
//################################################################################


procedure tstr_int_stringlist_list.hinzufuegen(str: string);
var
	neuereintrag: pstr_int_stringlist_liste;
begin
	new(neuereintrag);
	neuereintrag^.str1:=str;
	neuereintrag^.int1:=0;
	neuereintrag^.stringlist:=tstringlist.create;//.Assign(liste);
	add(neuereintrag);
end;


procedure tstr_int_stringlist_list.aktualisieren(str1:string; int: integer; str2: string; b_intaddieren: boolean );
var
	eintrag: pstr_int_stringlist_liste;
	liste: tstringlist;
	i, pos, i_a: integer;
begin
	//durchsuchen der Liste nach str, wenn nicht vorhanden dann  hinzufuegen.
	//laden der entsprechenden Stringliste, suchen nach der Knotennummer
	for i:=0 to count-1 do  //suchen von str1
	begin
	  eintrag:=items[i];
	  if eintrag^.str1 =str1 then break;
	end;
	if (count=0) or (eintrag^.str1<>str1) then //leere liste oder nicht gefunden
		begin
		hinzufuegen(str1);
		i:=count-1;
		eintrag:=items[i];
		end;

	liste:=eintrag^.stringlist;
	pos:=liste.IndexOf(str2);
	if pos <0 then liste.Add(str2);
	if b_intaddieren then i_a:=eintrag^.int1 + int else i_a:=int;
	// ben�tigt bei link_knotenliste erstellen; jeder Linkbegriff soll entsprechend seiner
	// ngrammzahl gewertet werden und nicht nach h�ufigkeit des vorkommens.
	eintrag^.int1:=i_a;
	eintrag^.stringlist:=liste;
	items[i]:=eintrag;
end;



procedure tstr_int_stringlist_list.clear;
var
listenwert:  pstr_int_stringlist_liste;
begin
 while count>0 do begin
	listenwert:= items[count-1];
	delete(count-1);
	listenwert^.stringlist.Free;
	dispose(listenwert);
 end;
inherited;// clear;
end;


function tstr_int_stringlist_list.str1_listeausgeben : tstringlist;
var
	i: integer;
	str, leer: string;
	listenwert: pstr_int_stringlist_liste;
begin
leer :='                                                                     ';
result:=tstringlist.create; //wird nach dem Funktionsaufruf freigegeben

for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  str:=copy(listenwert^.str1+leer,0,40)+inttostr(listenwert^.int1)+leer;
			  str:=copy(str, 0, 50);
			  result.Add(str);
//			  main.chart1.Series[1].AddXY(i+1,listenwert^.int1,'',clblue);
			end;
end;

function tstr_int_stringlist_list.str2_listeausgeben(str: string): tstringlist;
var
i: integer;
liste: tstringlist;
listenwert: pstr_int_stringlist_liste;
begin
str:=trim(copy(str, 0, 40));
result:=tstringlist.create;
	if (str<>'') and (count>0) then
	begin
		for i:=0 to count-1 do
			begin
			  listenwert:=items[i];
			  if listenwert^.str1 =str then
			  begin
				liste:=listenwert^.stringlist;
				result.AddStrings(liste);
				break;
			  end;
			end;
	end;
end;

destructor tstr_int_stringlist_list.destroy;
begin
	clear;
  inherited;
end;

//###############################################################################



procedure tstrstrlist.clear;
var
listenwert: pstrstrliste;
begin
 while count>0 do begin
	listenwert:= items[count-1];
	delete(count-1);
	dispose(listenwert);
 end;
inherited;// clear;
end;


function tstrstrlist.aktualisieren(s_text1, s_text2, s_text3, s_text4: string): string;
var
listenwert: pstrstrliste;
i,index: integer;
begin
	result:='';
	index:=-1;
	if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
			  listenwert:=items[i];
			  if listenwert^.str1 =s_text1 then
				begin  //#3
					index:=i;
					result:=listenwert^.str2;
					listenwert^.str2:=s_text2;
					items[i]:=listenwert;
					break;
				end;   //#3
			end;      //#2
 end;      //#1
 if index=-1 then hinzufuegen(s_text1, s_text2, s_text3, s_text4);
end;

procedure tstrstrlist.hinzufuegen(s_text1, s_text2, s_text3, s_text4: string);
var
	neuereintrag: pstrstrliste;
begin
	new(neuereintrag);
	neuereintrag^.str1:=s_text1;
	neuereintrag^.str2:=s_text2;
	neuereintrag^.str3:=s_text3;
	neuereintrag^.str4:=s_text4;
	add(neuereintrag);
end;

procedure tstrstrlist.auslesen(nummer: integer;var text1, text2, text3, text4: string);
var
	eintrag: pstrstrliste;
begin
  eintrag:=items[nummer];
  text1:=eintrag^.str1;
  text2:=eintrag^.str2;
  text3:=eintrag^.str3;
  text4:=eintrag^.str4;
end;



procedure tstrstrlist.loeschen(s_text:string);
var
i: integer;
listenwert: pstrstrliste;
begin
 if count>0 then
	  begin // #1  gef�llte liste
			for i:=0 to count-1 do
			begin //#2
			  listenwert:=items[i];
			  if listenwert^.str1 =s_text then
				begin  //#3
					delete(i);
					break;
				end;   //#3
			end;      //#2
 end;      //#1
end;


destructor tstrstrlist.destroy;
begin
	clear;
  inherited;
end;

end.
