(*
////////////////////////////////////////////////////////////////////////////////
// DBLUTEXT.PAS : DATABASE LOOKUP TEXT COMPONENT                              //
// Copyright � 1996-1999 Steve Flynn. All Rights Reserved                     //
//----------------------------------------------------------------------------//
// By using this source code you agree to be bound by the terms of the        //
// following Software License Agreement. Any breach of this agreement will be //
// prosecuted to the full extent of the law.                                  //
////////////////////////////////////////////////////////////////////////////////

LICENSE AGREEMENT - MARCH 1999

USE OF THE SOFTWARE

Steven M. Flynn (the "Author") grants you, the end user, a non-exclusive
single-user license to use the supplied software program and all associated
materials including the Source Code (the "Software"). The Software and all
Intellectual Property embodied in the Software always remains the property of
the Author. Your use of the Software indicates your acceptance in full of the
terms and conditions of this agreement.


COPYING THE SOFTWARE

You may freely copy the Software provided that such copies do not infringe upon
any other part of this agreement.


DISTRIBUTION OF THE SOFTWARE

The Software may be re-distributed royalty free providing that it is not
modified in any way and that no charge is made for the Software itself. Only a
nominal fee for media and/or copying services is allowed. All files, including
the License Agreement, that are supplied by the Author with the Software must
also be included in any such distribution.


DERIVED WORKS

You may NOT use the Software to derive any works which are based substantially
in part or in full on the Software or any portion of the Software. Any such
derived works will remain the property of the Author.


MODIFIED VERSIONS OF THE SOFTWARE

All updated, revised, modified or improved versions of the Software are also
subject to the conditions of this license agreement and shall remain the
property of the Author. You are required under the terms of this agreement to
submit to the Author any and all improvements or additions made by you to the 
Software for future inclusion in the general distribution of the Software by
the Author.


TERMINATION OF THIS AGREEMENT

You may terminate this License Agreement at any time by destroying all copies
of the Software in your possession.


LIMITED WARRANTY

The Software is distributed and licensed "AS IS". The Author specifically
disclaims all other warranties, express or implied, including but not limited
to, implied warranties of merchantability and fitness for a particular purpose,
with regard to the Software.


DAMAGES

By using the Software you do so at your own risk. In no event shall the Author
be responsible for any damages whatsoever (including but not limited to,
damages for loss of business profits, business interruption, loss of business
information, or any other pecuniary loss or any other real or consequential 
damages) arising out of the use or inability to use this product.


LIABILITY

In the event of failure of the Software, for any reason, the Author's sole
liability shall be to refund to you the amount paid by you to the Author for
the use of the Software.


COPYRIGHT

The Software and the Intellectual Property embodied in the Software shall
always remain the property of the Author and is protected by Australian
Copyright Law and International Treaty Provisions.


AGREEMENT

Your use of the Software indicates your agreement with the above terms and
conditions. If you do not agree with these terms and conditions then you must
destroy all copies of the software in your possession.


CONSUMER RIGHTS

If any clause in the above Software License Agreement is in violation of your
rights under your local laws then you must cease using the Software
immediately. It is your own responsibility to be aware of your legal rights
under your own local laws.


END OF SOFTWARE LICENSE AGREEMENT
*)

{$A-,B-,D-,F-,G+,I-,L-,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y-,Z-}
{$IFDEF WIN32}
{$H+,J+}
{$M 16384,1048576}
{$IMAGEBASE $00400000}
{$R DBLUTEXT.R32}
{$ELSE}
{$K-}
{$M 16384,8192}
{$R DBLUTEXT.R16}
{$ENDIF}
{$C PRELOAD}

unit DBLUText;

interface

uses
{$IFDEF VER100}
	DBCtrls,
{$ENDIF}
{$IFDEF VER120}
	DBCtrls,
{$ENDIF}
	SysUtils, WinProcs, Classes, Controls, Graphics, Menus, StdCtrls, DB, DBTables;

type
{TDBLookupText}
	TDBLookupText = class(TCustomLabel)
	private
		FDataLink: TFieldDataLink;
		FLookupSource: TDataSource;
		FDisplayFld, FLookupFld: TField;
		FLookupSrc: TDataSet;
		FDisplayField, FLookupField: pstring;
		FDataBaseOK: boolean;
		procedure DataChange(Sender: TObject);
		function GetDataField: string;
		function GetDataSource: TDataSource;
		function GetLookupField: string;
		function GetDisplayField: string;
		procedure SetDataField(const Value: string);
		procedure SetDataSource(Value: TDataSource);
		procedure SetLookupSource(Src: TDataSource);
		procedure SetLookupField(Str: string);
		procedure SetDisplayField(Str: string);
		procedure AssignFields;

	protected
		procedure Notification(AComponent: TComponent; Operation: TOperation); override;
		procedure Loaded; override;

	public
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;

	published
		property Align;
		property Alignment;
		property AutoSize default False;
		property Color;
		property DataField: string read GetDataField write SetDataField;
		property DataSource: TDataSource read GetDataSource write SetDataSource;
		property LookupField: string read GetLookupField write SetLookupField;
		property LookupDisplay: string read GetDisplayField write SetDisplayField;
		property LookupSource: TDataSource read FLookupSource write SetLookupSource;
		property DragCursor;
		property DragMode;
		property Enabled;
		property Font;
		property ParentColor;
		property ParentFont;
		property ParentShowHint;
		property PopupMenu;
		property Transparent;
		property ShowHint;
		property Visible;
		property WordWrap;
		property OnClick;
		property OnDblClick;
		property OnDragDrop;
		property OnDragOver;
		property OnEndDrag;
		property OnMouseDown;
		property OnMouseMove;
		property OnMouseUp;
		end;

procedure Register;

implementation


{TDBLookupText}
constructor TDBLookupText.Create(AOwner: TComponent);
	begin
	inherited Create(AOwner);
	AutoSize := False;
	ShowAccelChar := False;
	FDataLink := TFieldDataLink.Create;
	FDataLink.OnDataChange := DataChange;
	FDisplayField := NullStr;
	FLookupField := NullStr;
	end;


destructor TDBLookupText.Destroy;
	begin
	FDataLink.OnDataChange := nil;
	FDataLink.Free;
	FDataLink := nil;
	DisposeStr(FDisplayField);
	DisposeStr(FLookupField);
	inherited Destroy;
	end;


procedure TDBLookupText.Loaded;
	begin
	inherited Loaded;
	AssignFields;
	end;


procedure TDBLookupText.AssignFields;
	begin
	FDataBaseOK := False;
	if (FLookupSource <> nil) then
		FLookupSrc := FLookupSource.Dataset
	else
		FLookupSrc := nil;
	if (FLookupSrc <> nil) and (FDisplayField <> NullStr) then
		FDisplayFld := FLookupSrc.FieldByName(FDisplayField^)
	else
		FLookupFld := nil;
	if (FLookupSrc <> nil) and (FLookupField <> NullStr) then
		FLookupFld := FLookupSrc.FieldByName(FLookupField^)
	else
		FLookupFld := nil;
	if (FDataLink.Datasource <> nil) and (FDataLink.FieldName <> '') and
		(FDisplayFld <> nil) and (FLookupFld <> nil) then
		FDataBaseOK := True;
	end;


procedure TDBLookupText.Notification(AComponent: TComponent; Operation: TOperation);
	begin
	inherited Notification(AComponent, Operation);
	if (Operation = opRemove) and (FDataLink <> nil) then
		begin
		if (AComponent = LookupSource) then
			begin
			LookupSource := nil;
			FLookupSource := nil;
			AssignFields;
			end;
		if (AComponent = DataSource) then
			begin
			DataSource := nil;
			FDataLink.DataSource := nil;
			AssignFields;
			end
		end;
	end;


procedure TDBLookupText.DataChange(Sender: TObject);
	var
	OldIndexName, OldIndexFieldNames: string;
	begin
	if (not FDataBaseOK) or (not (FLookupSrc is TTable)) or (FDataLink.Field = nil) or (csLoading in ComponentState) then
		exit;
	with TTable(FLookupSrc) do
		begin
		OldIndexName := IndexName;
		OldIndexFieldNames := IndexFieldNames;
		IndexFieldNames := FLookupField^;
		SetKey;
		FLookupFld.Text := FDataLink.Field.Text;
		if GoToKey then
			Text := FDisplayFld.Text
		else
			Text := '';
		IndexName := OldIndexName;
		IndexFieldNames := OldIndexFieldNames;
		end;
	end;


{// Property Interface ////////////////////////////////////////////////////////}
function TDBLookupText.GetDataSource: TDataSource;
	begin
	Result := FDataLink.DataSource;
	end;


procedure TDBLookupText.SetDataSource(Value: TDataSource);
	begin
	FDataLink.DataSource := Value;
	end;


function TDBLookupText.GetDataField: string;
	begin
	Result := FDataLink.FieldName;
	end;


procedure TDBLookupText.SetDataField(const Value: string);
	begin
	FDataLink.FieldName := Value;
	end;


function TDBLookupText.GetLookupField: string;
	begin
	Result := FLookupField^;
	end;


function TDBLookupText.GetDisplayField: string;
	begin
	Result := FDisplayField^;
	end;


procedure TDBLookupText.SetLookupSource(Src: TDataSource);
	begin
	if FLookupSource <> Src then
		begin
		FLookupSource := Src;
		AssignFields;
		end;
	end;


procedure TDBLookupText.SetLookupField(Str: string);
	begin
	if FLookupField^ <> Str then
		begin
		AssignStr(FLookupField, Str);
		AssignFields;
		end;
	end;


procedure TDBLookupText.SetDisplayField(Str: string);
	begin
	if FDisplayField^ <> Str then
		begin
		AssignStr(FDisplayField, Str);
		AssignFields;
		end;
	end;


{// Registration //////////////////////////////////////////////////////////////}
procedure Register;
	begin
	RegisterComponents('Datensteuerung', [TDBLookupText]);
	end;

end.
