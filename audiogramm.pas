unit audiogramm;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ExtCtrls,stdctrls,spin;

type
	tashape =  class(tshape)  //class(TGraphicControl)
	public
	procedure mousedown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
	procedure WmLbuttondown( var parm : TWMlbuttondown) ; message WM_LButtondown;
	procedure WmRbuttondown( var parm : TWMrbuttondown) ; message WM_RButtondown;
end;

type
	Taudiogramm =  class(TPanel)
	private
	  { Private-Deklarationen }
	  fonLButtonDown : tnotifyevent;
	  fonrButtonDown: tnotifyevent;
	  luft_s: array[1..12] of tashape;
	  luft_db: array [1..12]of integer;
	  knochen_s: array[1..12] of tashape;
	  knochen_db: array [1..12]of integer;
	  frequenz:array[1..12] of real;
	  e_luft, e_knochen: array [1..12] of tspinedit;
	  f_lsumme,f_ksumme,f_snorm:integer;
	  nc:boolean;
	  f_geb_dat:tdate;
	  f_db_norm: string;

	  function xintofrequenz(x:integer):integer;
	  function yintodb(y:integer):integer;
	  function dbintoy(db:integer):integer;
	  function frequenzintox(frequenz:real):integer;
	  procedure zeichne_gitterlinien;
	  procedure zeichne_werte;
	  procedure p_kontrolle( var parm : TWMrbuttondown);
	  procedure punkte_neu;

	  function getl1: integer;
	  procedure setl1(wert:integer);
	  function getl2: integer;
	  procedure setl2(wert:integer);
	  function getl3: integer;
	  procedure setl3(wert:integer);
	  function getl4: integer;
	  procedure setl4(wert:integer);
	  function getl6: integer;
	  procedure setl6(wert:integer);

	  function getk1: integer;
	  procedure setk1(wert:integer);
	  function getk2: integer;
	  procedure setk2(wert:integer);
	  function getk3: integer;
	  procedure setk3(wert:integer);
	  function getk4: integer;
	  procedure setk4(wert:integer);
	  function getk6: integer;
	  procedure setk6(wert:integer);
	  procedure cluft(Sender: TObject);
	  procedure cknochen(Sender: TObject);
	  procedure punkt_luft(i:integer);
	  procedure punkt_knochen(i:integer);
	  procedure set_geb_dat(dat:tdate);
	  //procedure wmrezize(var parm :fwSizeType); message wm_size;
	protected
	  { Protected-Deklarationen }
	public
	  { Public-Deklarationen }
	  destructor Destroy; override;
	  constructor Create(Owner: TComponent); override;
	  procedure paint; override;
	  procedure resize;override;
	  procedure WmLbuttondown( var parm : TWMlbuttondown) ; message WM_LButtondown;
	  procedure WmRbuttondown( var parm : TWMrbuttondown) ; message WM_RButtondown;
	  procedure verbinde_punkte;
	  procedure werte_schreiben(luft, knochen:array of integer);
	  procedure werte_lesen(luft, knochen:array of integer);
	  procedure knochen_schreiben(s: string);
	  procedure luft_schreiben(s: string);
	  function  knochen_lesen: string;
	  function 	luft_lesen:string;
     procedure speichern(datei:string);

	published
	  { Published-Deklarationen }
		property l1: integer read getl1 write setl1;
		property l2: integer read getl2 write setl2;
		property l3: integer read getl3 write setl3;
		property l4: integer read getl4 write setl4;
		property l6: integer read getl6 write setl6;
		property lsumme:integer read f_lsumme write f_lsumme;

		property k1: integer read getk1 write setk1;
		property k2: integer read getk2 write setk2;
		property k3: integer read getk3 write setk3;
		property k4: integer read getk4 write setk4;
		property k6: integer read getk6 write setk6;
		property ksumme:integer read f_ksumme write f_ksumme;
		property geb_dat:tdate read f_geb_dat write set_geb_dat;

		property OnLButtonDown : tnotifyevent read fonLButtonDown write fonLButtonDown;
		property OnRButtonDown : tnotifyevent read fonrButtonDown write fonrButtonDown;
	end;


procedure Register;


implementation

procedure tashape.mousedown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

inherited ;
end;

procedure tashape.WmLbuttondown( var parm : TWMlbuttondown);
begin
parm.XPos:=left+width div 2;
parm.YPos:=top +height div 2;
taudiogramm(parent).wmlbuttondown(parm);
end;

procedure tashape.Wmrbuttondown( var parm : TWMrbuttondown);
begin
parm.XPos:=left +width div 2;
parm.YPos:=top +height div 2;
taudiogramm(parent).wmrbuttondown(parm);
end;

destructor taudiogramm.Destroy;
var
i:integer;
begin
for i:=1 to 12 do
begin
	luft_s[i].free;
	knochen_s[i].free;
	e_knochen[i].free;
	e_luft[i].free;
end;
inherited destroy;
end;

constructor  taudiogramm.Create(Owner: TComponent);
var i:integer;
	  shape:tashape;
	  edit:tspinedit;
begin
inherited create(owner);
	frequenz[1]:=0.125;
	frequenz[2]:=0.25;
	frequenz[3]:=0.5;
	frequenz[4]:=0.75;
	frequenz[5]:=1;
	frequenz[6]:=1.5;
	frequenz[7]:=2;
	frequenz[8]:=3;
	frequenz[9]:=4;
	frequenz[10]:=6;
	frequenz[11]:=8;
	frequenz[12]:=12;
for i:=1 to 12 do
begin
	  shape:=tashape.Create(self);
	  shape.Visible:=false;
	  shape.parent:=self;
	  shape.Shape:=stcircle	 ;
	  //shape.Left:=frequenzintox(frequenz[i])-3;
	  shape.Width:=13;
	  shape.top:=0;
	  shape.Height:=13;
	  shape.ShowHint:=true;
	  shape.Brush.Color:=clred;
	  shape.Brush.Style:=bsSolid	;
	  shape.Pen.Color:=clblack;
	  shape.Pen.Width:=2;
	  luft_s[i]:=shape;
	  luft_db[i]:=-20;

	  shape:=tashape.Create(self);
	  shape.Visible:=false;
	  shape.parent:=self;
	  shape.Shape:=stSquare ;
	  //shape.Left:=frequenzintox(frequenz[i])-3;;
	  shape.Width:=13;
	  shape.top:=0;
	  shape.Height:=13;
	  shape.ShowHint:=true;
	  shape.Brush.Color:=clblue;
	  shape.Brush.Style:=bsSolid	;
	  shape.Pen.Color:=clblack;
	  shape.Pen.Width:=2;
	  knochen_s[i]:=shape;
	  knochen_db[i]:=-20;
end;

for i:=1 to 12 do
begin
	 edit:=tspinedit.create(self);
	 edit.parent:=self;
	 edit.Font.Style:=[fsbold];
	 edit.Increment:=-5;
	 edit.Color:=clwhite;
	 edit.MaxValue:=90;
	 edit.MinValue:=-15;
	 edit.Width:=40;
	 edit.visible:=false;
	 edit.tag:=i;
	 edit.Onchange:=cluft;

	 e_luft[i]:=edit;
end;

for i:=1 to 12 do
begin
	 edit:=tspinedit.create(self);
	 edit.parent:=self;
	 edit.Color:=clwhite;
	 edit.Font.Style:=[fsbold];
	 edit.Width:=40;
	 edit.Increment:=-5;
	 edit.MaxValue:=90;
	 edit.MinValue:=-15;
	 edit.tag:=i;
	 edit.visible:=false;
	 edit.Onchange:=cknochen;
	 e_knochen[i]:=edit;
end;



end;

procedure taudiogramm.paint;
begin
	inherited paint;
	zeichne_gitterlinien;
	verbinde_punkte;
	zeichne_werte;

end;

procedure taudiogramm.resize;
begin
 inherited resize;
 punkte_neu;
 zeichne_werte;
end;

procedure taudiogramm.WmLbuttondown( var parm : TWMlbuttondown);
var
db,i:integer;
begin
	inherited;
	p_kontrolle( parm );

	db:=yintodb(parm.YPos);
	i:=xintofrequenz(parm.xPos);
	if luft_db[i]<>db then     //klick auf bestehenden punkt l�scht
	begin;
		luft_db[i]:=db;
	end
	else
	begin
		luft_db[i]:=-20;
	end;
	punkt_luft(i);

	tag:=1;
	repaint;
	zeichne_werte;
	if assigned(fonlbuttondown) then fonlbuttondown(self);

end;

procedure taudiogramm.punkt_luft(i:integer);
var
db:integer;
begin
	db:=luft_db[i];
	if  db>=-10 then
	begin
		luft_s[i].top:=dbintoy( db)-6;//-4
		luft_s[i].hint:=' '+inttostr(db)+' ';
		luft_s[i].visible:=true;
	end
	else
	begin
		luft_s[i].top:=0;
		luft_s[i].visible:=false;
	end;
end;

procedure taudiogramm.Wmrbuttondown( var parm : TWMrbuttondown);
var
db,i:integer;
begin
	inherited;
	p_kontrolle( parm );
	db:=yintodb(parm.YPos);
	i:=xintofrequenz(parm.xPos);
	if knochen_db[i]<>db then     //klick auf bestehenden punkt l�scht
	begin;
		knochen_db[i]:=db;
	end
	else
	begin
		knochen_db[i]:=-20;
	end;
	punkt_knochen(i);
	tag:=1;
	repaint;
	if assigned(fonrbuttondown) then fonrbuttondown(self);
	zeichne_werte;
end;

procedure taudiogramm.punkt_knochen(i:integer);
var db:integer;
begin
db:=knochen_db[i];
if db>=-10 then     //klick auf bestehenden punkt l�scht
	begin;
		knochen_s[i].top:=dbintoy( db)-6;//-4
		knochen_s[i].hint:=' '+inttostr(db)+' ';
		knochen_s[i].visible:=true;
	end
	else
	begin
		knochen_s[i].top:=0;
		knochen_s[i].visible:=false;
	end;
end;

function taudiogramm.xintofrequenz(x:integer):integer;
var
i:integer;
begin
	i:=1;
	while (frequenzintox(frequenz[i])<x) and (i<=12)do inc(i);
	if i>1 then
		if (x-(frequenzintox(frequenz[i-1]))>((frequenzintox(frequenz[i])-x))) then
		result:=i else result :=i-1
	else
		result:=1;
end;

function taudiogramm.yintodb(y:integer):integer;
begin
	result:=round((((y-30)*110/(height-200))-10)/5)*5;//140       -30
end;

function taudiogramm.dbintoy(db:integer):integer;
begin
	RESULT:=round(((height-200)/110)*(db+10) + 30);        //140
end;

function taudiogramm.frequenzintox(frequenz:real):integer;
begin
	result:=round((ln(frequenz)*(width-40)/5+width/2)-width/12+65); //20,35
end;

procedure taudiogramm.verbinde_punkte;
var xa,ya,x,y,i:integer;
begin

	canvas.Pen.Color:=clred;
	xa:=0;
	if luft_db[1]>-15 then
	 begin
		xa:=frequenzintox(frequenz[1]);
		ya:=dbintoy(luft_db[1]);
	  end;
	for i:=2 to 12 do
	begin
	 if luft_db[i]>-15 then
	 begin
		x:=frequenzintox(frequenz[i]);
		y:=dbintoy(luft_db[i]);
		if xa>0 then canvas.Polyline([point(xa,ya),point(x,y)]);
		xa:=x;
		ya:=y;
	  end;
	end;

	canvas.Pen.Color:=clblue;
	xa:=0;
	if knochen_db[1]>-15 then
	 begin
		xa:=frequenzintox(frequenz[1]);
		ya:=dbintoy(knochen_db[1]);
	  end;
	for i:=2 to 12 do
	begin
	 if knochen_db[i]>-15 then
	 begin
		x:=frequenzintox(frequenz[i]);
		y:=dbintoy(knochen_db[i]);
		if xa>0 then canvas.Polyline([point(xa,ya),point(x,y)]);
		xa:=x;
		ya:=y;
	  end;
	end;
end;

procedure taudiogramm.punkte_neu;
var x,xa,ya,i:integer;
s:string;
begin


	for i:=1 to 12 do
	begin
	 if luft_db[i]>-15 then
	 begin
		xa:=frequenzintox(frequenz[i]);
		ya:=dbintoy(luft_db[i]);
		luft_s[i].top:=ya-6;
		luft_s[i].left:=xa-6;
	  end;
	end;

	for i:=1 to 12 do
	begin
	 if knochen_db[i]>-15 then
	 begin
		xa:=frequenzintox(frequenz[i]);
		ya:=dbintoy(knochen_db[i]);
		knochen_s[i].top:=ya-6;
		knochen_s[i].left:=xa-6;
	  end;
	end;



end;


procedure taudiogramm.zeichne_gitterlinien;
var i:integer;
	  x,y,pw:integer;
	  s:string;
begin

pw:=1;
canvas.Brush.Color:=color;
canvas.FillRect(rect(left,top, left+width, top+height));
canvas.Pen.Color:=clblack;
canvas.Font.Size:=(height-30)div 45;

for i :=1 to 12 do //senkrecht
begin
		if i=5 then canvas.pen.width:=pw*2 else canvas.pen.width:=pw;
		x:=frequenzintox(frequenz[i]);
		canvas.Polyline([point(x,dbintoy(-10)), point(x,dbintoy(100))]);
		s:=floattostr(frequenz[i]);
		canvas.TextOut(x-length(s)*2,10,s);
		canvas.TextOut(x+2,dbintoy(92),s);

		knochen_s[i].Left:=x-6;
		luft_s[i].left:=x-6;


end;
	  canvas.TextOut(width-canvas.font.height*2-3,10,'kHz');
x:=frequenzintox(frequenz[12]);
for i :=-1 to 9 do //12    //quer
begin
		if i=0 then canvas.pen.width:=pw*2 else canvas.pen.width:=pw;
		y:=dbintoy(i*10);
		canvas.Polyline([point(frequenzintox(frequenz[1]),y),
		point(frequenzintox(frequenz[12]),y) ]);
		s:=trim(floattostr(i*10));
		canvas.TextOut(25,y-7,s);//2
		canvas.TextOut(width-35,y-7,s); //16
end;
		canvas.TextOut(2,height-canvas.font.height-3,'dB');
		canvas.font.color:=clred;

		canvas.Font.Size:=canvas.Font.Size;//+2
		canvas.TextOut(2*(width div 3),height-35,'Luftleitung');
		canvas.font.color:=clblue;
		canvas.TextOut(2*(width div 3) +110,height-35,'Knochenleitung');

		canvas.Pen.Color:=clblack;
		canvas.pen.Width:=2;
		canvas.Brush.Color:=clred;
		canvas.ellipse(2*(width div 3) -12,height-29,2*(width div 3)-1,height-17 );

		canvas.Brush.Color:=clblue;
		canvas.Rectangle( 2*(width div 3) +97,height-29,2*(width div 3)+109,height-17 );
end;

procedure taudiogramm.zeichne_werte;
var I,x:integer;
s:string;
begin
nc:=true;


for i :=1 to 12 do //senkrecht
begin

		x:=frequenzintox(frequenz[i]);

		e_luft[i].left:=x;
		e_luft[i].top:=dbintoy(100);
		if luft_db[i]>=-10 then e_luft[i].text:=inttostr(luft_db[i]) else e_luft[i].text:='';//-20
		e_luft[i].visible:=true;

		e_knochen[i].left:=x;
		e_knochen[i].top:=dbintoy(108);
		if knochen_db[i]>=-10 then e_knochen[i].text:=inttostr(knochen_db[i]) else e_knochen[i].text:='';
		e_knochen[i].visible:=true;

end;
canvas.Brush.Color:=self.Color;
canvas.Font.Color:=clblack;

canvas.TextOut(0,dbintoy(118),'H�rverlust Grenzwerte Erstuntersuchung:');
canvas.TextOut(0,dbintoy(110),'Knochen');
canvas.TextOut(0,dbintoy(102),'Luft');


canvas.Font.Style:=[fsbold];

for i:=1 to 6 do
begin

	 x:=frequenzintox(frequenz[i+4]);
	 s:=copy(f_db_norm,(i-1)*2+1,2);
	 canvas.TextOut(x+2,dbintoy(118),s);
end;

	f_lsumme:=0;
	if luft_db[7]>-15 then f_lsumme:=f_lsumme+luft_db[7];
	if luft_db[8]>-15 then f_lsumme:=f_lsumme+luft_db[8];
	if luft_db[9]>-15 then f_lsumme:=f_lsumme+luft_db[9];
	f_ksumme:=0;
	if knochen_db[7]>-15 then f_ksumme:=f_ksumme+knochen_db[7];
	if knochen_db[8]>-15 then f_ksumme:=f_ksumme+knochen_db[8];
	if knochen_db[9]>-15 then f_ksumme:=f_ksumme+knochen_db[9];



	s:=format('Summe H�rverluste 2,3,4KHz Luft:%0:d Knochen:%1:d (Max: %2:d)  ',[f_lsumme, f_ksumme, f_snorm]);

	canvas.TextOut(5,height-35,s);

canvas.Font.Style:=[];

nc:=false;
end;


procedure taudiogramm.p_kontrolle( var parm : TWMrbuttondown);
begin
	if parm.XPos<frequenzintox(frequenz[1]) then parm.XPos:=frequenzintox(frequenz[1]);
	if parm.XPos>frequenzintox(frequenz[12]) then parm.XPos:=frequenzintox(frequenz[12]);
	if parm.yPos<dbintoy(-10) then parm.yPos:=dbintoy(-10);
	if parm.yPos>dbintoy(90) then parm.yPos:=dbintoy(90); //120


end;


procedure taudiogramm.werte_schreiben(luft, knochen:array of integer);
var
i:integer;
begin
	for i:=1 to 12 do
	begin
	  luft_db[i]:=luft[i];
	  knochen_db[i]:=knochen[i];
	end;

end;

procedure taudiogramm.werte_lesen(luft, knochen:array of integer);
var
i:integer;
begin
	for i:=1 to 12 do
	begin
	  luft[i]:=luft_db[i];
	  knochen[i]:=knochen_db[i];
	end;
end;

procedure taudiogramm.knochen_schreiben(s: string);
var
i,db:integer;
s_db:string;
begin
try
for i:=1 to 12 do
begin
	  s_db:=trim(copy(s,i*3-2,3));
	  if s_db='' then db:=-20 else db:=strtoint(s_db);

	  knochen_s[i].top:=dbintoy( db)-6;
	  knochen_db[i]:=db;
	  knochen_s[i].hint:=' '+inttostr(db)+' ';
	  if db>-15 then knochen_s[i].visible:=true else knochen_s[i].visible:=false;

end;
except
end;
repaint;
end;


procedure taudiogramm.luft_schreiben(s: string);
var
i,db:integer;
s_db:string;
begin
try
for i:=1 to 12 do
begin
	 s_db:=trim(copy(s,i*3-2,3));
	 if s_db='' then db:=-20 else db:=strtoint(s_db);

	 luft_s[i].top:=dbintoy( db)-6;
	 luft_db[i]:=db;
	luft_s[i].hint:=' '+inttostr(db)+' ';
	if db>-15 then luft_s[i].visible:=true else luft_s[i].visible:=false; ;

end;
except
end;
repaint;
end;

function  taudiogramm.knochen_lesen: string;
var
i:integer;
begin

result:='';
try
for i:=1 to 12 do
begin
	result:=result+copy(inttostr(knochen_db[i])+'   ',0,3);
end;
except
end;
end;

function  taudiogramm.luft_lesen:string;
var
i:integer;
begin

result:='';
try
for i:=1 to 12 do
begin
	result:=result+copy(inttostr(luft_db[i])+'   ',0,3);
end;
except
end;
end;



procedure taudiogramm.speichern(datei:string);
var
   h: TBitmap;
   r: TRect;
begin
   //application.bringtofront;
   //self.BringToFront;
   //application.ProcessMessages;
   r:=rect(0,0,self.Width,self.Height);
   h:=NIL;
   try
      h:=TBitmap.Create;
      h.width:=r.right;
      h.height:=r.bottom;;
      h.Canvas.CopyRect(r,self.Canvas,r);
      h.savetofile(datei);
   finally
      h.free;
   end;

end;

function taudiogramm.getl1: integer;
begin
result:=luft_db[5];
end;

procedure taudiogramm.setl1(wert:integer);
begin
luft_db[5]:=wert;
luft_s[5].visible:=true;
punkte_neu;
repaint;

end;

function taudiogramm.getl2: integer;
begin
result:=luft_db[7];

end;

procedure taudiogramm.setl2(wert:integer);
begin
luft_db[7]:=wert;
luft_s[7].visible:=true;
punkte_neu;
repaint;

end;

function taudiogramm.getl3: integer;
begin

result:=luft_db[8];
end;

procedure taudiogramm.setl3(wert:integer);
begin
luft_db[8]:=wert;
luft_s[8].visible:=true;
repaint;
punkte_neu;
end;

function taudiogramm.getl4: integer;
begin
result:=luft_db[9];
end;

procedure taudiogramm.setl4(wert:integer);
begin
luft_db[9]:=wert;
luft_s[9].visible:=true;
repaint;
punkte_neu;
end;

function taudiogramm.getl6: integer;
begin
result:=luft_db[10];
end;

procedure taudiogramm.setl6(wert:integer);
begin
luft_db[10]:=wert;
luft_s[10].visible:=true;
repaint;
punkte_neu;
end;


function taudiogramm.getk1: integer;
begin
result:=knochen_db[5];
end;

procedure taudiogramm.setk1(wert:integer);
begin
knochen_db[5]:=wert;
end;

function taudiogramm.getk2: integer;
begin
result:=knochen_db[7];
end;

procedure taudiogramm.setk2(wert:integer);
begin
knochen_db[7]:=wert;
end;

function taudiogramm.getk3: integer;
begin
result:=knochen_db[8];
end;

procedure taudiogramm.setk3(wert:integer);
begin
knochen_db[8]:=wert;
end;

function taudiogramm.getk4: integer;
begin
result:=knochen_db[9];
end;

procedure taudiogramm.setk4(wert:integer);
begin
knochen_db[9]:=wert;
end;

function taudiogramm.getk6: integer;
begin
result:=knochen_db[10];
end;

procedure taudiogramm.setk6(wert:integer);
begin
knochen_db[10]:=wert;
end;


procedure taudiogramm.cluft(Sender: TObject);
var i:integer;
begin
	if nc then exit;
	if tspinedit(sender).text='' then exit;

	try
	i:=tspinedit(sender).tag;
	luft_db[i]:=tspinedit(sender).value;
	punkt_luft(i);
	repaint;
	self.onclick(self);
	except

	end;
	//punkte_neu;
end;

procedure taudiogramm.cknochen(Sender: TObject);
var i:integer;
begin
	if nc then exit;
	if tspinedit(sender).text='' then exit;
	
	try
	i:=tspinedit(sender).tag;
	knochen_db[i]:=tspinedit(sender).value;
	punkt_knochen(i);
	repaint;
	self.onclick(self);
	except
	end;
end;


procedure taudiogramm.set_geb_dat(dat:tdate);
var alter:integer;
begin
try
f_geb_dat:=dat;
alter:=trunc((date()-f_geb_dat) / 365);
 case alter of
	1..20: f_snorm:=65;
	21..25: f_snorm:=75 ;
	26..30: f_snorm:=85 ;
	31..35: f_snorm:=95;
	36..40: f_snorm:=105;
	41..45: f_snorm:=115;
	46..50: f_snorm:=130;
	51..200: f_snorm:=140;
	end;
 case alter of
 1..30:f_db_norm:='15  15202525';   //zwei leerzeiche f�r 1,5 khz
 31..35:f_db_norm:='15  20252530';
 36..40:f_db_norm:='15  20253035';
 41..45:f_db_norm:='20  25304040';
 46..200:f_db_norm:='20  25354550';
 end;
except
end;
end;

procedure Register;
begin
	RegisterComponents('Beispiele', [Taudiogramm]);
end;



end.
