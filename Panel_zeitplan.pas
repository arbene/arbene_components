unit Panel_zeitplan;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ExtCtrls,math, listen;



type
 Tshape_termin = class(Tshape)
 private
	fart:integer;
  fnummer:int64;
	ftext:string;
	ffarbe: tcolor;
	procedure zeigtext;
	procedure writetext(value:string);
 public
	  destructor Destroy; override;
	  constructor  Create(Owner: TComponent); override;
	  procedure paint; override;
 published
	 property art: integer read fart write fart;
	 property nummer:int64 read fnummer write fnummer;
	 property farbe: tcolor read ffarbe write ffarbe;
	 property text:string read ftext write writetext;
 end;


 TPanel_termin = class(TPanel)
 private
	fart, fnummer:integer;
	ffarbe: tcolor;
 public

 published
	 property art: integer read fart write fart;
	 property nummer:integer read fnummer write fnummer;
	 property farbe: tcolor read ffarbe write ffarbe;
 end;



	TPanel_zeitplan = class(TPanel)
	private
		fzeit_min, fzeit_max, fzeit_step,
		fdat_min, fdat_max:integer;

		fdatum:tdatetime;
		gitterhoehe:integer;

	
		procedure datum_write(value:tdatetime);
		function y_dat(tag:integer):integer;
		function x_zeit(zeit:ttime):integer;
		function zeit(x:integer):tdatetime;
		function max_dat(datum:tdatetime):integer;
		procedure zeichne_feiertage;
	  { Private-Deklarationen }
	protected
	  { Protected-Deklarationen }
	public
	  { Public-Deklarationen }
	  arfarbe: array of tcolor;
	  fmonats_feiertage:tstringintegerlist;
	  shape_aktuell:tshape_termin;
	  destructor Destroy; override;
	  constructor  Create(Owner: TComponent); override;
	  procedure paint; override;
	  procedure zeichne_gitterlinien;
	  procedure termin_zeichnern(datum:integer;beginn, ende:ttime;text:string; nummer:int64;art:integer);
	  procedure alle_termine_loeschen;
	  function zeit_start(shape:tshape):tdatetime;
	  function zeit_ende(shape:tshape):tdatetime;
	  function tage(y:integer):integer;
	  procedure shape_einpassen(panel:tshape);
	  procedure markiere_feld(dat: integer; text:string; farbe:tcolor );

	published
	  { Published-Deklarationen }
	  property datum: tdatetime read fdatum write datum_write;//dat_min_write;
	  property zeit_min: integer read fzeit_min write fzeit_min;//zeit_min_write;
	  property zeit_max: integer read fzeit_max write fzeit_max;//zeit_max_write;
	  property zeit_step: integer read fzeit_step write fzeit_step;//zeit_step_write;
	end;

procedure Register;

implementation

//SHAPE_termin
destructor tshape_termin.Destroy;
begin
	inherited destroy;
end;

constructor  tshape_termin.Create(Owner: TComponent);
begin
	inherited create(owner);
	shape:=stRectangle ;
	brush.Color:=clred;
	brush.Style:=bsclear ;

end;

procedure tshape_termin.zeigtext;
begin

	canvas.Pen.Color:=clblack;
	canvas.font.Style:=[fsbold];
	canvas.Brush.Color:=self.Color;
	//canvas.Textrect(rect(left, top,left+width,top+height),2,0,'hallo');
	canvas.TextOut(4,0,ftext);
end;

procedure tshape_termin.writetext(value:string);
begin
	ftext:=value;
	//zeigtext;
end;


procedure tshape_termin.paint;
begin
inherited paint;
zeigtext;
end;


constructor tpanel_zeitplan.create(owner:tcomponent);
begin
inherited create(owner);

end;

destructor tpanel_zeitplan.destroy;
begin
alle_termine_loeschen;
inherited destroy;

end;


procedure tpanel_zeitplan.paint;
begin
inherited paint;
zeichne_gitterlinien;

end;

procedure tpanel_zeitplan.zeichne_feiertage;
var i,wotag:integer;
		tagname:string;
		listenwert:pstringintegerliste;

begin
  if fmonats_feiertage=nil then exit;
	for i:=0 to fmonats_feiertage.count-1 do
	begin
		listenwert:=fmonats_feiertage[i];
		wotag:=listenwert^.int1;
		tagname:=listenwert^.str1;
		markiere_feld(wotag,tagname,clBtnFace	 );
	end;
end;



procedure tpanel_zeitplan.datum_write(value:tdatetime);
begin
fdatum:=value;
fdat_min:=1;
fdat_max:=max_dat(fdatum);
zeichne_gitterlinien;
end;

function tpanel_zeitplan.max_dat(datum:tdatetime):integer;
var
year, month, day:word;
tage: array[1..12] of integer;
monatalt:integer;
begin
tage[1]:=31;
tage[2]:=28;
tage[3]:=31;
tage[4]:=30;
tage[5]:=31;
tage[6]:=30;
tage[7]:=31;
tage[8]:=31;
tage[9]:=30;
tage[10]:=31;
tage[11]:=30;
tage[12]:=31;
decodedate(fdatum, year, month,day);
result:=tage[month];
if month=2 then         
begin
	datum:=encodedate(year, month, 28);
  monatalt:=month;
  datum:=datum+1;
	decodedate(datum, year, month,day);
  if month=monatalt then result:=29;
end;

end;

procedure tpanel_zeitplan.zeichne_gitterlinien;
var i:integer;
    x,y:integer;
    year, month, day:word;
begin
decodedate(fdatum,year, month, day);
if fzeit_min*fzeit_max*fzeit_step*fdat_min*fdat_max*fdatum=0 then exit;
//canvas.Pen.Color:=color;
canvas.Brush.Color:=color;
canvas.FillRect(rect(left,top, left+width, top+height));
canvas.Pen.Color:=clblack;
canvas.Font.Size:=(height-30)div 45;
gitterhoehe:=y_dat(2)-y_dat(1);
zeichne_feiertage;
canvas.Brush.Color:=color;

for i:=1 to fdat_max do
begin
     y:=y_dat(i);
		canvas.TextOut(13,y, inttostr(i));
     canvas.TextOut(32,y,copy(formatdatetime('dddd',encodedate(year, month, i)),1,2));
		canvas.Polyline([point(13,y), point(x_zeit(encodetime(fzeit_max+1,0,0,0)), y)]);
end;
		y:=y_dat(i);
	 canvas.Polyline([point(13,y), point(x_zeit(encodetime(fzeit_max+1,0,0,0)), y)]);
for i:=fzeit_min to fzeit_max do
begin
		x:=x_zeit(encodetime(i,0,0,0));
		canvas.textout(x+10,20,inttostr(i));
     canvas.Polyline([point(x,20), point(x,y_dat(fdat_max+1) )]);
end;
     x:=x_zeit(encodetime(i,0,0,0));
     canvas.Polyline([point(x,20), point(x,y_dat(fdat_max+1) )]);

canvas.TextOut((width div 2)-10, 2, 'U h r z e i t');
canvas.Textout(3, (height div 2)-30, 'D');
canvas.Textout(3, (height div 2)-15, 'a');
canvas.Textout(4, (height div 2)-0, 't');
canvas.Textout(3, (height div 2)+15, 'u');
canvas.Textout(2, (height div 2)+30, 'm');



end;

function tpanel_zeitplan.y_dat(tag:integer):integer;
begin
   result:=trunc(((self.Height-50)/(fdat_max-fdat_min+1))*tag)+30;
end;

function tpanel_zeitplan.x_zeit(zeit:ttime):integer;
var
hour, min, sec, msec:word;
stunden:real;
begin
   result:=50;
   decodetime(zeit, hour, min, sec, msec);
   if hour<fzeit_min then exit;
	 hour:=hour-fzeit_min;
   if min>0 then  stunden:=hour+min/60 else stunden :=hour;

   result:=trunc(((Width-50) /(fzeit_max-fzeit_min+1)) *stunden)+50;
end;

procedure tpanel_zeitplan.termin_zeichnern(datum:integer;beginn, ende:ttime;text:string; nummer:int64;art:integer);
var
shape: tshape_termin;
begin
	if gitterhoehe<=0 then exit;
	if beginn<=0 then exit;
	if ende<=0 then ende:=beginn+0.02;
	shape:=tshape_termin.create(self);
shape.Visible:=false;
shape.parent:=self;
try
	shape.farbe:=arfarbe[art];
except
	shape.farbe:=clred;
end;
shape.color:=shape.farbe;;
shape.nummer:=nummer;
shape.art:=art;
//panel.t farbe:=clred;
shape.Left:=x_zeit(beginn);
shape.Width:=x_zeit(ende)-shape.Left;
shape.top:=y_dat(datum);
shape.Height:=gitterhoehe;
shape.text:=text;
shape.Tag:=nummer;
shape.Brush.Color:=shape.farbe;
shape.Brush.Style:=bsbdiagonal;
shape.Pen.Color:=shape.farbe;
shape.Pen.Width:=2;
shape.OnClick:=self.OnClick;
//shape.OnEnter:=self.OnEnter;
//shape.OnExit:=self.OnExit;
shape.OnResize:=self.OnResize;
shape.OnConstrainedResize:=self.OnConstrainedResize;
shape.OndragDrop:=self.OndragDrop;
shape.OnDragOver:=self.OnDragOver;
shape.OnMouseMove:=self.OnMouseMove;
shape.OnMouseDown:=self.OnMouseDown;
shape.OnMouseUp:=self.OnMouseUp;
//panel.DragMode:=dmautomatic;

shape.Visible:=true;

end;

procedure tpanel_zeitplan.alle_termine_loeschen;
var
 i:integer;
begin
for I := controlcount - 1 downto 0 do
	begin
    controls[I].free;
   end; ;

end;

function tpanel_zeitplan.zeit_start(shape:tshape):tdatetime;
begin
	result:=zeit(shape.left);
end;

function tpanel_zeitplan.zeit_ende(shape:tshape):tdatetime;
begin
	result:=zeit(shape.left+shape.width);
end;

function tpanel_zeitplan.tage(y:integer):integer;

begin
	result:=round((y-30)*(fdat_max-fdat_min+1)/(self.height-50));
	result:=max(result,1);
	result:=min(result,31);
end;

procedure 	tpanel_zeitplan.shape_einpassen(panel:tshape);
begin
	panel.top:=y_dat(tage(panel.top));
	if panel.width<5 then panel.width:=5;
	if panel.left<x_zeit(x_zeit(encodetime(fzeit_min,0,0,0))) then
		panel.left:=x_zeit(x_zeit(encodetime(fzeit_min,0,0,0)));
	if panel.left>x_zeit(encodetime(fzeit_max,0,0,0)) then
		panel.left:=x_zeit(encodetime(fzeit_max-1,0,0,0));
	if panel.left+panel.Width>x_zeit(encodetime(fzeit_max,0,0,0)) then
		panel.width:=x_zeit(encodetime(fzeit_max,0,0,0))-panel.left;

end;

function tpanel_zeitplan.zeit(x:integer):tdatetime;
var stunden:real;
hour, minute:word;
begin
   if fzeit_max+1=fzeit_min then exit;
   if width=50 then exit;
	 //result:=trunc((Width-50) / (fzeit_max-fzeit_min+1) *stunden)+30;
	 stunden:=(x-50)*(fzeit_max-fzeit_min+1)/(width-50);
   hour:=trunc(stunden)+fzeit_min;
   minute:=trunc(frac(stunden)*60);
	 if minute>=60 then
	 begin
		minute:=0;
		hour:=hour+1;
	 end;
	 hour:=max(hour,0);
	 hour:=min(hour,24);
	 result:=encodetime(hour, minute, 0,0);
end;

procedure tpanel_zeitplan.markiere_feld(dat: integer; text:string; farbe:tcolor );
var xo,yo,xu,yu:word;

begin
yo:=y_dat(dat);
xo:=x_zeit(encodetime(fzeit_min,0,0,0));
yu:=y_dat(dat+1);
xu:=x_zeit(encodetime(fzeit_max+1,0,0,0));
canvas.Brush.Color:=farbe;
canvas.FillRect(rect(13,yo,xu,yu));
canvas.Font.Size:=7;
canvas.TextOut(xo+3,yo, text);

end;


procedure Register;
begin
	RegisterComponents('Beispiele', [TPanel_zeitplan ]);
end;

end.
