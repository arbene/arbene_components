unit a_namsuch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, dbgridEXT, StdCtrls, Buttons, ExtCtrls,a_soundex,a_datamodul;

type
  TForm_namsuch = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    DBgridEXT1: TDBgridEXT;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    CheckBox1: TCheckBox;
    procedure Edit1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DBgridEXT1DblClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure soundexneu;
  public
    { Public-Deklarationen }
  end;

var
  Form_namsuch: TForm_namsuch;

implementation

uses a_main;

{$R *.DFM}

procedure TForm_namsuch.Edit1Change(Sender: TObject);
var
	wort, sname, svorname:string;
begin
  if checkbox1.Checked then
  	datamodule1.Table_stammdaten_read.IndexName:='stamm_namvornam'
  else
  	datamodule1.Table_stammdaten_read.IndexName:='stamm_soundex';
	sname:=soundex_2(pchar(edit1.text),20) ;

	svorname:=soundex_2(pchar(edit2.text),20);
  if checkbox1.Checked then
    datamodule1.Table_stammdaten_read.Findnearest([edit1.text,edit2.text])
  else
	  datamodule1.Table_stammdaten_read.Findnearest([sname,svorname]);



end;


procedure TForm_namsuch.soundexneu;
var
	wort, s_name, s_vorname:string;
begin
  with datamodule1 do
  begin
     table_Stammdaten_read.IndexName:='';
     table_Stammdaten_read.First;
     while not table_Stammdaten_read.Eof do
     begin
        if table_Stammdaten_read['name']<>null then wort:=table_Stammdaten_read['name'];
			s_name:=soundex_2(wort,20);
			if table_Stammdaten_read['vorname']<>null then wort:=table_Stammdaten_read['vorname'];
			s_vorname:=soundex_2(wort,20);

        table_Stammdaten_read.edit;
        Table_stammdaten_read['s_name']:=s_name;
        Table_stammdaten_read['s_vorname']:=s_vorname;
        Table_stammdaten_read.Next;
     end;
   end;
end;

procedure TForm_namsuch.Button1Click(Sender: TObject);
begin
	soundexneu;
end;

procedure TForm_namsuch.DBgridEXT1DblClick(Sender: TObject);
begin
modalresult:=mrok;
end;

procedure TForm_namsuch.CheckBox1Click(Sender: TObject);
begin
  if checkbox1.Checked then
  	datamodule1.Table_stammdaten_read.IndexName:='stamm_namvornam'
  else
  	datamodule1.Table_stammdaten_read.IndexName:='stamm_soundex';
end;

procedure TForm_namsuch.FormCreate(Sender: TObject);
begin
   grid_einstellen(dbgridext1);
	form_main.form_positionieren(tform(sender));
end;

end.
