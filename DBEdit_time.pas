unit DBEdit_time;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls,variants;

type
  TDBEdit_time = class(TDBEdit)

  procedure KeyPress ( var Key: Char);override;
  procedure Change; override;
  private
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
  published
    { Published-Deklarationen }
  end;

procedure Register;

implementation
procedure TDBEdit_time.KeyPress( var Key: Char);
var
position : integer;
  begin
     inherited;
  end;

procedure TDBEdit_time.Change;
var
sel,p:integer;
t,s:string;
begin
//if length(tdbedit(sender).text)>8 then tdbedit(sender).text:=copy((tdbedit(sender).text),12,6);
// 00:00
t:=text;
if length(t)>10 then
begin
      t:=copy(t,12,5);
      text:=t;
      selstart:=0;
      exit;
end;
//doppelpunkt entfernen
//wenn l�nger asl 2 dann Doppelpunkt einf�gen
sel:=selstart;
p:=pos(':',t);
if p>0 then delete(t,p,1);
//rest mit 0 auff�llen
t:=t+'0000';
t:=copy(t,1,4);
insert(':',t,3);
if t='24:00' then t:='00:00';
text:=t;
if sel=2 then sel:=3;
selstart:=sel;


{
if pos(':',t)=0 then
begin
      text:='00:00';
      exit;
end;
if pos(':',t)=2 then t:='0'+t;

sel:=selstart;

if length(t)=8 then
 begin
      text:=copy(t,1,5);
      selstart:=0;
 end
else
  if length(t)>5 then
  begin
   if sel<>3
   then  delete(t,sel+1,1)
   else
   begin
        delete(t,sel+1,2);
        insert(':',t,sel);
        inc(sel);
   end;
   text:=t;
   selstart:=sel;
   if length(t)>5 then
   begin
        text:=copy(t,1,5);
        selstart:=5;
   end;
  end; }

inherited;
end;

procedure Register;
begin
  RegisterComponents('Datensteuerung', [TDBEdit_time]);
end;

end.
