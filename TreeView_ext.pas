unit TreeView_ext;

interface

uses
	{Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ComCtrls,WinTypes,  ZMySqlQuery,listen,math;}
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons,  Mask,
	Menus, ImgList,db,
	listen, Spin, vcfi, registry,
	CheckLst, ExtDlgs,math,  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZConnection,variants;


type tedit_tabelle = procedure(sender:tobject) of object;



type
	TTreeView_ext = class(TTreeView)
	private
	  { Private-Deklarationen }
		//TDragOverEvent = procedure(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean) of object;
		fonedit_tabelle : tedit_tabelle;
		ftabelle: tzquery;
		fdatasource: tdatasource;
		ffeldname:string;
		fbereich_s:string;
		fbereich_s2:string;
		fbereich_i:integer;
		fbereich_i2:integer;
		drag:ttreenode;
		fparentfeld, fkindfeld:string;
     sperren:boolean;
		function getbigint(tabelle: tdataset;feldname:string):int64;
		procedure setbigint(tabelle: tdataset;feldname:string;wert:int64);
		procedure tab_gehezu(Node: TTreeNode);
	protected
	  { Protected-Deklarationen }

	  procedure DragOver( Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);override;

	public
	  { Public-Deklarationen }
		change_allowed:boolean;
    gesperrt:boolean;
	  akt_nummer:int64;
	  constructor create(AOwner: TComponent); override;
	  destructor Destroy; override;
	  procedure dragdrop( Source: TObject; X, Y: Integer);override;
	  procedure neu(s_text:string;command:integer);
	  procedure loeschen;
	  procedure reihenfolge_neu;
    procedure liste_lesen_alphabetisch;
	  procedure liste_lesen;
	  procedure liste_lesen_parent(parentFeld,KindFeld:string);
	  procedure liste_loeschen;
	  procedure tabelle_post;
	  procedure markieren(parent_mark:boolean;sindex:integer);
	  procedure alles_markieren(sindex:integer);
	  procedure alles_demarkieren;
	  procedure tabelle_feld_abgleich;
	  procedure Change(Node: TTreeNode);override; //der knoten hat sich ge�nder die Tabelle muss akt. weden.
	  procedure hoch;
	  procedure runter;
	  procedure copy;
	  procedure insert;
    function suche_datensatz(wort:string): boolean;
    function suche_nummer(nr:int64): ttreenode;
	published
	  { Published-Deklarationen }
		property bereich_feldname: string read fbereich_s write fbereich_s;   //zum filtern �ber einen unterbereich
		property bereich_wert: integer read fbereich_i write fbereich_i;     //   s.o.
		property bereich_feldname2: string read fbereich_s2 write fbereich_s2;   //zum filtern �ber einen unterbereich
		property bereich_wert2: integer read fbereich_i2 write fbereich_i2;     //   s.o.
		property OnEdit_tabelle : tedit_tabelle read fonedit_tabelle write fonedit_tabelle;
		property mysqlquery: tzquery read ftabelle write ftabelle;
		property mysql_feld: string read ffeldname write ffeldname;
		property mysqldatasource :tdatasource read fdatasource write fdatasource;
	  //procedure Changing(Node: TTreeNode; var AllowChange: Boolean); override;

	end;

type
		pnodepointer=^nodewert;
		nodewert = record
		nummer: int64;
     ebene:integer;

end;
procedure Register;

implementation


constructor ttreeview_ext.create(AOwner: TComponent);
begin
inherited;
change_allowed:=true;
gesperrt:=false;
end;




destructor ttreeview_ext.Destroy;
begin
liste_loeschen;
inherited;// destroy;
end;


procedure ttreeview_ext.liste_loeschen;
var i:integer;
	  daten:pnodepointer;
begin
gesperrt:=true;
change_allowed:=false;
	for i:=0 to items.Count-1 do
	begin
		daten:=Items[i].Data;
		dispose(daten);
	end;
	items.Clear;
change_allowed:=true;
gesperrt:=false;
end;

procedure ttreeview_ext.liste_lesen_alphabetisch;
var
		 i_parent:int64;
      i_index,i: integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
begin
try
  gesperrt:=true;
	change_allowed:=false;
  enabled:=false;
	fparentfeld:='';
	fkindfeld:='';
	liste_loeschen;
  ftabelle.Refresh;
  ftabelle.Tag:=1;
	ftabelle.first;
   //showmessage(ftabelle.SQL.text);
   
	if  ftabelle.Eof then exit;
   //showmessage('anfang');
	//erster datensatz von hand
	{knoten:=Items.Add(Selected, ftabelle[ffeldname]);      //tree_bereich
	new(daten);
	daten^.nummer:=getbigint(ftabelle,'nummer');
	knoten.Data:=daten;
	knoten.SelectedIndex:=1;
	ftabelle.Next;}

	while not ftabelle.eof do
	begin
        //showmessage(ftabelle.findfield(ffeldname).asstring);
		  knoten:=Items.Add(selected, ftabelle.findfield(ffeldname).asstring);
			new(daten);
			daten^.nummer:=getbigint(ftabelle,'nummer');
			knoten.Data:=daten;
			knoten.SelectedIndex:=1;
			ftabelle.Next;
   end;
finally
		change_allowed:=true;
    gesperrt:=false;
     enabled:=true;
     ftabelle.Tag:=0;
end;
end;

procedure ttreeview_ext.liste_lesen;
var
		 i_parent:int64;
      i_index,i: integer;
		 knoten: ttreenode;
		 daten:pnodepointer;
begin
try
  gesperrt:=true;
	change_allowed:=false;
  enabled:=false;
	fparentfeld:='';
	fkindfeld:='';
	liste_loeschen;
  ftabelle.Refresh;
  ftabelle.Tag:=1;
	ftabelle.first;
   //showmessage(ftabelle.SQL.text);
   
	if  ftabelle.Eof then exit;
   //showmessage('anfang');
	//erster datensatz von hand
	knoten:=Items.Add(Selected, ftabelle[ffeldname]);      //tree_bereich
	new(daten);
	daten^.nummer:=getbigint(ftabelle,'nummer');
	knoten.Data:=daten;
	knoten.SelectedIndex:=1;
	ftabelle.Next;

	while not ftabelle.eof do
	begin
        //showmessage(ftabelle.findfield(ffeldname).asstring);
			i_parent:= getbigint(ftabelle,'parent');//ftabelle['parent'];
			i_index:=-1; //#-1
			for i:=0 to Items.count -1 do
			begin
				daten:=Items[i].Data;
				if daten^.nummer=i_parent then
				begin
					i_index:=i;
					break;
				end;
			end;

			if i_index>-1 then
				  knoten:=Items.Addchild(items[i_index], ftabelle.findfield(ffeldname).asstring)
         else
           knoten:=Items.Add(items[0], ftabelle.findfield(ffeldname).asstring);
			new(daten);
			daten^.nummer:=getbigint(ftabelle,'nummer');
			knoten.Data:=daten;
			knoten.SelectedIndex:=1;
			ftabelle.Next;
   end;
finally
		change_allowed:=true;
    gesperrt:=false;
     enabled:=true;
     ftabelle.Tag:=0;
end;
end;

procedure ttreeview_ext.liste_lesen_parent(ParentFeld,KindFeld:string);
var

		 //datumParent:tdate;
      parent_inhalt:variant;
		 knoten,knotenParent: ttreenode;
		 daten:pnodepointer;
begin
try
  gesperrt:=true;
	change_allowed:=false;
	if assigned(fdatasource) then fdatasource.DataSet:=nil;
	visible:=false;

	fparentfeld:=parentfeld;
	fkindfeld:=kindfeld;
	liste_loeschen;
	ftabelle.first;
	if  ftabelle.Eof then exit;
	//erster datensatz von hand

	if ftabelle[parentFeld]=null then exit;

	  knotenParent:=Items.Add(Selected, ftabelle[parentFeld]);
	  //datumParent:=ftabelle[parentFeld];
    parent_inhalt:=ftabelle[parentFeld];
	  new(daten);
	  daten^.nummer:=-1; //parent-felder
	  knotenparent.Data:=daten;
	  knotenParent.SelectedIndex:=0;

	if ftabelle[KindFeld]<>null then
	begin
	  knoten:=Items.Addchild(KnotenParent, ftabelle[KindFeld]);
	  new(daten);
	  daten^.nummer:=getbigint(ftabelle,'nummer');
	  knoten.Data:=daten;
	  knoten.SelectedIndex:=1;
	end;
	ftabelle.Next;

	while not ftabelle.eof do
	begin
		  if (ftabelle[parentfeld]<>null) and (ftabelle[parentfeld]<>parent_inhalt) then
		  begin
			knotenParent:=Items.Add(knotenParent, ftabelle[parentFeld]);
        parent_inhalt:=ftabelle[parentFeld];
			//datumParent:=ftabelle[parentFeld];
			new(daten);
			daten^.nummer:=-1;
			knotenparent.Data:=daten;
			knotenParent.SelectedIndex:=0;
		  end;
			if ftabelle[KindFeld]<>null then
			begin
			  knoten:=Items.Addchild(KnotenParent, ftabelle[KindFeld]);
			  new(daten);
			  daten^.nummer:=getbigint(ftabelle,'nummer');
			  knoten.Data:=daten;
			  knoten.SelectedIndex:=1;
			end;
			ftabelle.Next;
		 end;
finally
		change_allowed:=true;
    gesperrt:=false;
		if assigned(fdatasource) then fdatasource.DataSet:=ftabelle;
		visible:=true

end;
end;

procedure ttreeview_ext.copy;
begin
 if (drag=nil)  then
	 begin
	 drag:=selected; // Ausgangswert vom Ziehen
	 end;
end;


procedure ttreeview_ext.insert;
var
	AnItem: TTreeNode;
	AttachMode: TNodeAttachMode;
	i_nummer:int64;
	parentdata:pnodepointer;
begin
	anitem:=selected;
	if (drag <> nil) and (drag<>anitem) then
	begin
	  AttachMode := nainsert;
	  drag.MoveTo(AnItem, attachmode);
	  parentdata:=drag.data;
	  i_nummer:=parentdata^.nummer;
	  ftabelle.first;
	  //ftabelle.locate('nummer',inttostr(i_nummer),[]);
    ftabelle.locate('nummer',(i_nummer),[]);
	  ftabelle.edit;
	  setbigint(ftabelle,'parent',i_nummer);

	  ftabelle.Post;

	  reihenfolge_neu;
	  drag:=nil;
 end;
end;

procedure ttreeview_ext.dragdrop(Source: TObject; X, Y: Integer);
var
	AnItem: TTreeNode;
	AttachMode: TNodeAttachMode;
	HT: THitTests;
	parentnum, i_nummer:int64;
	parentdata:pnodepointer;
begin
  gesperrt:=true;
  change_allowed:=false;
  dragmode:=dmmanual;
  drag:=selected;
	HT := GetHitTestInfoAt(X, Y);
	AnItem := GetNodeAt(X, Y);
	attachMode:=naAddChild; //zur Definition
 if source is ttreeview then
 begin
 change_allowed:=false;
	//if (Selected = nil) or (selected=anitem) then Exit;
	if (drag <> nil) and (drag<>anitem) then
	begin
		  if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent, htonright, htonlabel,htonbutton] <> HT) then  //Mengenlehre
			 begin
			  if (anitem<>nil) and (anitem.parent<>nil) then
					begin
						parentdata:=anitem.parent.data;
						 parentnum:=parentdata^.nummer;
					end
					else parentnum:=0;//#-1
			  if (htOnItem in HT) or (htOnIcon in HT) or(htonlabel in ht) then
				begin
					AttachMode := naAddChild;
					parentdata:=anitem.data;
					parentnum:=parentdata^.nummer;
				end
			  else if (htNowhere in ht) or (htonright in ht) then
					begin
					AttachMode := naAdd;

					end
			  else if (htOnIndent in HT) or (htonbutton in HT) then
				 begin
					AttachMode := naInsert;
				 end;
              if Messagedlg('Soll der Datensatz verschoben werden?',mtConfirmation, [mbOK,mbcancel],0)=mrok then
              begin

                 drag.MoveTo(AnItem, AttachMode);

                 //bereich_parent_eintragen(drag,parentnum);
                 parentdata:=drag.data;
                 i_nummer:=parentdata^.nummer;
                 ftabelle.first;
                 //ftabelle.locate('nummer',inttostr(i_nummer),[]);
                 ftabelle.locate('nummer',(i_nummer),[]);
                 ftabelle.edit;
                 setbigint(ftabelle,'parent',parentnum);

                 ftabelle.Post;
                 reihenfolge_neu;
             end;

			 end;
	 end;

	end;

	drag:=nil;
	inherited ;
  dragmode:=dmautomatic;
  change_allowed:=true;
  gesperrt:=false;
end;

procedure ttreeview_ext.DragOver(Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);
var
sen:ttreenode;
begin

accept:=false;
if not change_allowed then exit;
 if (source is ttreeview_ext) then
	 begin
     sen:=GetNodeAt(X, Y);
     if (sen<>nil) or (sen<>selected) then accept:=true;
     if y<10 then topitem:=topitem.GetPrev  ;
     if (y>(height-10)) then topitem:=topitem.getnext;
  end;
end;


procedure ttreeview_ext.reihenfolge_neu;
//neueinlesen der Reihenfolge
var
i: integer;
knoten, parent,sknoten: ttreenode;
daten:pnodepointer;
i_nummer, p_nummer:int64;
begin
	 //datamodul.q_bereich.IndexName:='';
	 sknoten:=selected;
	 if ftabelle.FindField('reihenfolge')=nil then exit;
	 //k_alt:=selected.absoluteindex;
	 for i:=0 to items.Count-1 do
	 begin
		knoten:=items[i];
		daten:=knoten.Data;
		i_nummer:=daten^.nummer;
		if assigned(knoten.parent) then
		begin
		  parent:=knoten.Parent;
		  daten:=parent.Data;
		  p_nummer:=daten^.nummer;
		end
		else p_nummer:=0;
		//ftabelle.first;
		//ftabelle.locate('nummer', inttostr(i_nummer),[]);
    ftabelle.locate('nummer', (i_nummer),[]);
     if (ftabelle.FindField('reihenfolge').asinteger<>i) or (ftabelle.findfield('parent').asstring<>inttostr(p_nummer)) then
     begin
       ftabelle.edit;
       ftabelle.findfield('reihenfolge').asinteger:=i;
       setbigint(ftabelle,'parent',p_nummer);
       ftabelle.Post;
     end;
	 end;
	 selected:=sknoten;
	 //change(selected); tut nicht wenn not  change_allowed .........
	 tab_gehezu(selected);
inherited ;
end;

procedure ttreeview_ext.Change(Node: TTreeNode);
var
i_nummer: int64;
daten:pnodepointer;
R:boolean;
begin
try
	 if not (change_allowed) or (node=nil) then exit;
	 if not ftabelle.Active then exit;
	 if change_allowed then
	 begin
			daten:=node.Data;
			i_nummer:=daten^.nummer;
        akt_nummer:=i_nummer;

			//r:=ftabelle.Locate('nummer',inttostr(i_nummer),[]);
      r:=ftabelle.Locate('nummer',i_nummer,[]);
      //if not r then showmessage(inttostr(i_nummer)) ;
			node.Expand(false);
	 end;
finally
	inherited;
end
end;


procedure ttreeview_ext.tab_gehezu(Node: TTreeNode);
var
i_nummer: int64;
daten:pnodepointer;
begin
try
        if node=nil then exit;
			daten:=node.Data;
			i_nummer:=daten^.nummer;
			akt_nummer:=i_nummer;
			//ftabelle.Locate('nummer',inttostr(i_nummer),[]);
      ftabelle.Locate('nummer',(i_nummer),[]);
			node.Expand(false);
finally
	inherited;
end
end;

procedure ttreeview_ext.neu(s_text:string;command:integer);
var
nodedata,parentdata:pnodepointer;
parentnum:integer;
knoten:TTreeNode;
nummer:int64;
begin
try
  knoten:=nil;
  gesperrt:=true;
  change_allowed:=false;
	if items.Count=0 then command:=1; //neuer Basisknoten

 if (Selected=nil) and (items.Count>0) then
	begin
		showmessage('Bitte zuerst ausw�hlen.');
		change_allowed:=true;

		exit;
	end;

	try
		//nodedata:=Selected.data;
		//bernum:=nodedata^.nummer;
	 except
		//bernum:=0;
	 end;

	  case command of
	  1:
		begin
			knoten:= Items.insert( Selected, '');
		end;
		2: begin
			 knoten:= Items.Addchildfirst( Selected, '');
			  Selected.Expand(false);
			end;

	  end;  //case
		Selected:=knoten;
	  try   //neu bei 1 oder 2
		if knoten.parent<>nil then
		 begin
			parentdata:=knoten.Parent.data;
			parentnum:=parentdata^.nummer;
			end
		 else
		 parentnum:=0  ;
	  except
		parentnum:=0;
	  end;

	  ftabelle.append;
    
	  if ftabelle.FindField(ffeldname)<>nil then ftabelle[ffeldname]:=s_text;
	  if ftabelle.FindField('parent')<>nil then parentnum:=getbigint(ftabelle,'parent');
	  if ftabelle.FindField('reihenfolge')<>nil then ftabelle['reihenfolge']:=knoten.absoluteindex;
	  if ftabelle.FindField(fbereich_s)<>nil then ftabelle[fbereich_s]:=fbereich_i;
    if ftabelle.FindField(fbereich_s2)<>nil then ftabelle[fbereich_s2]:=fbereich_i2;

	  //neuer_datensatz(datamodul.q_bereich,[nil,knoten.text,parentnum,knoten.absoluteindex,0, '','']);
    ftabelle.post;

	  //richspeichern(datamodul.q_bereich,form_bereich.richedit_bereich);
	  new(nodedata);
	  nodedata.nummer:=getbigint(ftabelle,'nummer');
    //nodedata.ebene:=
	  knoten.Data:=nodedata;
	  knoten.SelectedIndex:=1;
	  knoten.Text:=ftabelle[ffeldname];

	  //bereich_edit;          //hier aufruf des formulars
    if assigned(fonedit_tabelle) then fonedit_tabelle(self);
    tabelle_feld_abgleich;

	  reihenfolge_neu;
finally
	change_allowed:=true;
  gesperrt:=false;
	change(selected);

end
end;


procedure ttreeview_ext.tabelle_post;
var
nodedata:pnodepointer;
begin
if selected=nil then exit;
	if ftabelle.State in [dsedit,dsinsert] then
	begin
		ftabelle.post;
     //ftabelle.Refresh;
		nodedata:=Selected.data;
		nodedata.nummer:=getbigint(ftabelle,'nummer');
     selected.data:=nodedata;
		selected.Text:=ftabelle[ffeldname];
	end;
end;

procedure ttreeview_ext.tabelle_feld_abgleich;
var
nodedata:pnodepointer;
begin
	if ftabelle.State in [dsedit,dsinsert] then
	begin
		nodedata:=Selected.data;
		nodedata.nummer:=getbigint(ftabelle,'nummer');
		selected.Text:=ftabelle[ffeldname];
	end;
end;



procedure ttreeview_ext.loeschen;
var
nodedata:pnodepointer;
bernum:int64;
parent:TTreeNode;
begin
  parent:=nil;
	if (Selected=nil) and (items.Count>0) then
	begin
		showmessage('Bitte zuerst ausw�hlen.');
		exit;
	end;
 if Selected.HasChildren then
	 begin
		showmessage('Zuerst untergeordnete Eintr�ge l�schen.');
		exit;
	 end;
	if MessageDlg('L�schen des Eintrags?', mtConfirmation	,[mbYes, mbNo], 0)<>mryes then
	begin
		exit;
	end;
try
	change_allowed:=false;
	if items.Count=0 then exit;

	try
		parent:=selected.parent;
		nodedata:=Selected.data;
		bernum:=nodedata^.nummer;
		dispose(nodedata);
	 except
		bernum:=0;
	 end;
		if getbigint(ftabelle,'nummer')=bernum then
		begin
         if ftabelle.findfield('storno')<>nil
         	then
            	begin
                  ftabelle.edit;
               	ftabelle.findfield('storno').asinteger:=1;
                  ftabelle.post;
                end
         	else
            	ftabelle.delete;

       Selected.Delete;
 	    if fparentfeld='' then reihenfolge_neu       //bei laborlist mit datum als parent
		 else
		 begin
			if not parent.HasChildren then
			begin
				nodedata:=parent.data;
				dispose(nodedata);
				parent.Delete;
			end;
		 //liste_lesen_parent(fparentfeld,fkindfeld);
		 end;
	 end;

finally
	change_allowed:=true;
  gesperrt:=false;
	change(selected);
end;
end;

procedure ttreeview_ext.markieren(parent_mark:boolean;sindex:integer);  //markiert untergeordnete knoten
var i:integer;
knoten:ttreenode;

function hasparent(p,f:integer):boolean;
var j:integer;
begin
	 j:=f;
	 while (items[j].parent<>nil) and(j>p) do
	 begin
		 j:=items[j].parent.absoluteindex;
	 end;
	 if j=p then result:=true else result:=false;

end;

begin //main
	selected.StateIndex:=sindex;
	if parent_mark then
	begin
	  knoten:=selected.parent;
	  while knoten<>nil do
	  begin
		  knoten.StateIndex:=sindex;
		  knoten:=knoten.Parent;
	  end;
	  i:=selected.absoluteindex;
	  //knoten:=selected;
	  inc(i);
	  //while (i<items.count) and(items[i].parent=selected) do
	  while (i<items.count) and hasparent(selected.absoluteindex,i) do
	  begin
		 items[i].StateIndex:=sindex;
		 inc(i);
	  end;
	end;
end;


procedure ttreeview_ext.alles_markieren(sindex:integer);
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=sindex;
	end;
end;

procedure ttreeview_ext.alles_demarkieren;
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=-1;
	end;
end;

procedure ttreeview_ext.runter;
begin
	if selected.absoluteindex >0 then  selected:=items[selected.absoluteindex-1];
end;

procedure ttreeview_ext.hoch;
begin
	if selected.absoluteindex <items.count-1 then  selected:=items[selected.absoluteindex+1];
end;

function ttreeview_ext.getbigint(tabelle: tdataset;feldname:string):int64;
var r:string;
begin
try
result:=0;
if tabelle.findfield(feldname)<>nil then
      r:= tabelle.FindField(feldname).AsString;
except
end;
if r='' then result:=0 else result:=strtoint64(r);
end;

procedure ttreeview_ext.setbigint(tabelle: tdataset;feldname:string;wert:int64);
begin
try
if tabelle.findfield(feldname)<>nil then //tabelle[feldname]:=inttostr(wert);
       tabelle.FindField(feldname).AsString := inttostr(wert);
except
end;
end;



function ttreeview_ext.suche_datensatz(wort:string): boolean;
var knoten,k_start: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
wort:=lowercase(wort);
if (items.count<=1) or (wort='') then exit;
if selected=nil then selected:=items[0];
  k_start:=selected;
knoten:=selected.GetNext;
if knoten=nil then
begin
     knoten:=items[0];
end;
while knoten<> k_start do
begin
	if  pos( wort,lowercase(knoten.Text))>0 then
	begin
	  selected:=knoten;
    change(selected);
	  result:=true;
	  exit;
	end;
	knoten:=knoten.getNext;
  if knoten=nil then knoten:=items[0];
end;
except
end;
end;

function ttreeview_ext.suche_nummer(nr:int64): ttreenode;
var knoten: ttreenode;
	  daten:pnodepointer;
begin
try
result:=nil;
if items.count=0 then exit;
knoten:=items[0];
while knoten<> nil do
begin
	daten:=knoten.data;
	if (daten<>nil) and (daten^.nummer=nr)  then
	begin
	  selected:=knoten;
	  result:=knoten;
	  exit;
	end;
	knoten:=knoten.GetNext;
   
end;
except
end;
end;


procedure Register;
begin
	RegisterComponents('Datensteuerung', [TTreeView_ext]);
end;

end.
