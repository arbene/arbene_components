unit histlist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  thistorybox = class(TlistBox)
  private

    { Private-Deklarationen }
     flistenlaenge: integer;
  protected
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
     procedure add (eintrag: string);

  published
    { Published-Deklarationen }
    property ListenLaenge: integer read flistenlaenge write flistenlaenge;
  end;

procedure Register;

implementation

procedure thistorybox.add(eintrag: string);
begin
while  items.count > listenlaenge do
begin
	items.delete(items.count-1);
end;
	if items.indexof(eintrag)=-1 then items.insert(0,eintrag)
  else items.move(items.indexof(eintrag),0);
end;


procedure Register;
begin
  RegisterComponents('Beispiele', [thistorybox]);
end;

end.
