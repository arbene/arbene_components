unit DBGrid_pic;

{
// DBPICGRD.PAS (C) 1995 W. Raike
//              ALL RIGHTS RESERVED.
//
//    DESCRIPTION:
//      Data-aware grid that can display graphic fields.
//    REVISION HISTORY:
//      15/04/95  Created.    W. Raike
}


interface

uses
  DBGrids, DB, DBTables, Grids, WinTypes, Classes, Graphics;

type
  tdbgrid_pic = class(TDBGrid)
  protected
    procedure DrawDataCell(const Rect: TRect;
      Field: TField; State: TGridDrawState); override;
  public
    constructor Create(AOwner : TComponent); override;
  published
    property DefaultDrawing default False;
  end;

procedure Register;

implementation

constructor tdbgrid_pic.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  DefaultDrawing := false;
end;

procedure tdbgrid_pic.DrawDataCell(const Rect: TRect; Field: TField;
State: TGridDrawState);
var
  bmp : TBitmap;
begin
  with Canvas do
  begin
    FillRect(Rect);
    if Field.IsBlob then // is TGraphicField then
        try
          bmp := TBitmap.Create;
          bmp.Assign(Field);
          Draw(Rect.Left, Rect.Top, bmp);
        finally
          bmp.Free;
        end
    else
      TextOut(Rect.Left, Rect.Top, Field.Text);
  end;
end;




procedure Register;
begin
  RegisterComponents('Datensteuerung', [TDBGrid_pic]);
end;

end.
