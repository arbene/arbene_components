unit xygraph;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ExtCtrls, listen,Math;

type
	txygraph = class(TPanel)
	private
	  { Private-Deklarationen }
	  f_log:boolean;
	  f_liste:tstrstrlist;
	  f_yachse:string;
	  f_farbe:tcolor;
	  f_titel:string;
	  zh, zb,emin, emax, ediff,x0,x1,y0,y1,xalt,yalt:integer;
	  yFaktor, xfaktor:real;
	  procedure koordinaten(xma:integer);
	  procedure zeichnepunkt(x:integer;y:real; text:string;objekt:boolean);
	  procedure objekte_loeschen;
	protected
	  { Protected-Deklarationen }
	public
	  { Public-Deklarationen }
	  destructor Destroy; override;
	  constructor  Create(Owner: TComponent); override;
	  procedure paint; override;
	  procedure clear;
	  procedure add(text:string; wert:real);
	  procedure graph_zeichnen;
	  procedure shape_zeichnen;
     procedure speichern(datei:string);
	published
	  { Published-Deklarationen }
	  property logaritmus :boolean read f_log write f_log;

	  property farbe: tcolor read f_farbe write f_farbe;
	  property y_achse: string read f_yachse write f_yachse;
	  property titel:string read f_titel write f_titel;
	end;

procedure Register;

implementation

constructor  txygraph.Create(Owner: TComponent);
begin
inherited create(owner);
f_liste:=tstrstrlist.create;
//canvas.Font.


end;

destructor txygraph.Destroy;
begin
	f_liste.clear;
	f_liste.Free;
	inherited destroy;
end;

procedure txygraph.graph_zeichnen;
var
i:integer;
listenwert:pstrstrliste;
y:real;
begin
	yalt:=-1;
	for i:=0 to f_liste.Count-1 do
	begin
		listenwert:=f_liste.items[i];
		y:=strtofloat(listenwert^.str2);
		zeichnepunkt(i,y,listenwert^.str1,false);
	end;

end;


procedure txygraph.shape_zeichnen;
var
i:integer;
listenwert:pstrstrliste;
y:real;
begin
	objekte_loeschen;
  yalt:=-1;
	for i:=0 to f_liste.Count-1 do
	begin
		listenwert:=f_liste.items[i];
		y:=strtofloat(listenwert^.str2);
		zeichnepunkt(i,y,listenwert^.str1,true);
	end;

end;

procedure txygraph.paint;
var
i :integer;
y,imin, imax:real;
listenwert:pstrstrliste;
begin
 	inherited paint;
	if f_liste.Count=0 then exit ;

	imin:=99999;
	imax:=-99999;
	for i:=0 to f_liste.Count-1 do
	begin
		listenwert:=f_liste.items[i];
		y:=strtofloat(listenwert^.str2);
		imin:=min(imin, y);
		imax:=max(imax, y);
	end;
	 imin:=max(imin,0);
	 zh:=height-70;
	 zb:=width-60;
	 x0:=40;
	 x1:=width-20;
	 y0:=height-40;
	 y1:=30;

	if f_log then
	begin
	  if imax>0 then emax:=trunc((ln(imax)/ln(10))+1) else emax:=0;
	  if imin>1 then emin:=trunc(ln(imin)/ln(10)) else
	  if imin =0 then emin:=-1 else 	emin:=trunc(ln(imin)/ln(10))-1;
	end
	else
	begin
	  emax:=round(imax+0.5);
	  emin:=trunc(imin);
	end;
	ediff:=emax-emin;

	if ediff<>0 then yfaktor:=zh/(ediff) ;

	xfaktor:=zb/f_liste.Count;
	canvas.pen.Color:=clblack;
	canvas.Font.size:=canvas.Font.size+2;
	if f_titel<> '' then canvas.TextOut(width div 2 -canvas.TextWidth(f_titel)div 2 ,5,f_titel);
	canvas.Font.size:=canvas.Font.size-2;
	koordinaten(f_liste.Count);
	graph_zeichnen;
	//shape_zeichnen;


end;

procedure txygraph.clear;
begin
	objekte_loeschen;
  f_liste.clear;
end;

procedure txygraph.objekte_loeschen;
var i:integer;
begin

for I := controlcount - 1 downto 0 do
	begin
    controls[I].free;
   end; ;
end;

procedure txygraph.add(text:string; wert:real);
begin
f_liste.hinzufuegen(text, floattostr(wert),'','');
//graph_zeichnen;
end;

procedure txygraph.koordinaten( xma:integer);
var i,y,lz: integer;
wert,d:real;
begin
canvas.pen.Color:=clblack;
canvas.polyline([point(x0, y0),point(x0,y1)]);
canvas.Polyline([point(x0,y0), point(x1,y0)]);

if f_log then
begin
	lz:=10;
	d:=(emax-emin)/lz;
	for i:=emin to emax do
	begin
	  y:=y0-trunc((i-emin)*yfaktor);
	  //y:=y0-trunc((i*d)*yfaktor);
	  wert:=exp(i*ln(10));
	  //wert:=exp((i*d+emin)*ln(10))
	  canvas.Polyline([point(x0-3,y),point(x1,y)]);
	  canvas.TextOut(5,y-7,floattostr(wert));
	end;
end
else
begin
	lz:=10;
	d:=(emax-emin)/lz;
	for i:=0 to lz do
	begin
	  y:=y0-trunc((i*d)*yfaktor);
	  wert:=i*d+emin;
	  canvas.Polyline([point(x0-3,y),point(x1,y)]);
	  canvas.TextOut(5,y-7,floattostr(wert));
	end;
end;
canvas.textout(5,height-35,'Tag');
canvas.textout(5,height+canvas.Font.Size-34,'Monat');
canvas.textout(5,height+canvas.Font.Size*2-33,'Jahr');

end;

procedure txygraph.zeichnepunkt(x:integer;y:real; text:string;objekt:boolean);
var
px,py,fsize:integer;
shape:tshape;
begin
	canvas.pen.Color:=clred;
	px:=x0+trunc(xfaktor*x);
	if f_log then
	begin
		if y>0 then py:=y0-trunc(yfaktor*(ln(y)/ln(10)-emin))
		else py:=y0;
	end
	else
	begin
		if y>0 then py:=y0-trunc(yfaktor*(y-emin))	else py:=y0;
	end;
	if (yalt>=0) and (y>=0) then canvas.polyline([point(xalt,yalt), point(px,py)]);
	if objekt then
	begin
	  shape:=tshape.Create(self);
		shape.Visible:=false;
		shape.parent:=self;
		shape.Shape:=stCircle ;
		shape.Left:=px-5;
		shape.Width:=10;
		shape.top:=py-5;
		shape.Height:=10;
		if y>=0 then shape.hint:=' '+text+':  '+floattostr(y)+' '
				else shape.hint:=' '+text+': (geplant) ';
		shape.ShowHint:=true;
		shape.Pen.Color:=clblack;
		shape.Brush.Color:=clred;
		shape.Brush.Style:=bsSolid	;
		shape.Pen.Color:=clred;
		shape.Pen.Width:=2;
		shape.visible:=true;
	 end;
	 if y>=0 then canvas.TextOut(px+8,py-10,floattostr(y))
	 else canvas.TextOut(px+8,py+10,'geplant');
	//canvas.Ellipse(px-3,py-3,px+3,py+3);
	canvas.pen.Color:=clblack;
	//CANVAS.Polyline([point(px,y0),point(px,y0-5)]);
	CANVAS.Polyline([point(px,y0),point(px,py+5)]);
	fsize:=canvas.Font.size;//:=canvas.Font.size-1;

	canvas.TextOut(px-5,y0+5,copy(text,1,2));
	canvas.TextOut(px-5,y0+6+fsize,copy(text,4,2));
	canvas.TextOut(px-5,y0+7+fsize*2,copy(text,9,2));
	//canvas.Font.size:=canvas.Font.size+1;
	xalt:=px;
	yalt:=py;

end;

procedure txygraph.speichern(datei:string);
var
   h: TBitmap;
   r: TRect;
begin

   r:=rect(0,0,self.Width,self.Height);
   h:=NIL;
   try
      h:=TBitmap.Create;
      h.width:=r.right;
      h.height:=r.bottom;;
      h.Canvas.CopyRect(r,self.Canvas,r);
      h.savetofile(datei);
   finally
      h.free;
   end;

end;



procedure Register;
begin
	RegisterComponents('Beispiele', [txygraph]);
end;

end.

