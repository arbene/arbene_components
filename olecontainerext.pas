unit olecontainerext;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, extctrls,
  OleCtnrs, OleDlg, CommCtrl, Menus, ComObj, ActiveX;
  
type
  TOLeContainerext = class(TOleContainer)

  private
    { Private-Deklarationen }

    fonLButtonDown : tnotifyevent;
    fonrButtonDown: tnotifyevent;
    fonrButtonUp: tnotifyevent;
    timer:ttimer;

     procedure WmLbuttondown( var parm : TWMlbuttondown) ; message WM_LButtondown;
     procedure WmRbuttondown( var parm : TWMrbuttondown) ; message WM_RButtondown;
     procedure WmRbuttonUp( var parm : TWMrbuttonup) ; message WM_RButtonup;

     procedure zeitereignis(sender: tobject);
  protected
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }

	  destructor Destroy; override;
	  constructor  Create(Owner: TComponent); override;
  published
    { Published-Deklarationen }
     //procedure LoadFromStream(Stream: TStream);
    property OnLButtonDown : tnotifyevent read fonLButtonDown write fonLButtonDown;
    property OnRButtonDown : tnotifyevent read fonrButtonDown write fonrButtonDown;
    property OnRButtonUp : tnotifyevent read fonrButtonUp write fonrButtonUp;

    procedure modified_reset(t:integer);
  end;

procedure Register;

implementation
 uses OleConst;

const
  DataFormatCount = 2;
  StreamSignature = $434F4442; {'BDOC'}

type
  TStreamHeader = record
    case Integer of
      0: ( { New }
        Signature: Integer;
        DrawAspect: Integer;
        DataSize: Integer);
      1: ( { Old }
        PartRect: TSmallRect);
  end;

{ Private variables }


constructor tolecontainerext.create( owner: tcomponent);
begin
  inherited create(owner);
  timer:=ttimer.Create(self);
  timer.interval:=0;
  timer.enabled:=false;
  timer.OnTimer:=zeitereignis;

end;

destructor tolecontainerext.destroy;
var i:integer;
begin
timer.free;
inherited destroy;
end;

procedure tolecontainerext.zeitereignis(sender: tobject);
begin
  timer.enabled:=false;
  modified:=false;
end;

procedure tolecontainerext.WmLbuttondown( var parm : TWMlbuttondown);
begin
	inherited;
	if assigned(fonlbuttondown) then fonlbuttondown(self);
end;

procedure tolecontainerext.Wmrbuttondown( var parm : TWMrbuttondown);
begin
	inherited;
	if assigned(fonrbuttondown) then fonrbuttondown(self);
end;

procedure tolecontainerext.Wmrbuttonup( var parm : TWMrbuttonUp);
begin
	inherited;
	if assigned(fonrbuttonup) then fonrbuttonup(self);
end;

procedure tolecontainerext.modified_reset(t:integer);
begin
  if t>0 then
  begin
    timer.Interval:=t;
    timer.Enabled:=true;
  end;
end;


{procedure TOleContainerext.LoadFromStream(Stream: TStream);
var
  DataHandle: HGlobal;
  Buffer: Pointer;
  Header: tstreamheader;
begin
  DestroyObject;
  Stream.ReadBuffer(Header, SizeOf(Header));
  if (Header.Signature <> StreamSignature) and not OldStreamFormat then
    raise EOleError.Create(SInvalidStreamFormat);
  DataHandle := GlobalAlloc(GMEM_MOVEABLE, Header.DataSize);
  if DataHandle = 0 then OutOfMemoryError;
  try
    Buffer := GlobalLock(DataHandle);
    try
      Stream.Read(Buffer^, Header.DataSize);
    finally
      GlobalUnlock(DataHandle);
    end;
    OleCheck(CreateILockBytesOnHGlobal(DataHandle, True, fLockBytes));
    DataHandle := 0;
    OleCheck(StgOpenStorageOnILockBytes(FLockBytes, nil, STGM_READWRITE or
      STGM_SHARE_EXCLUSIVE, nil, 0, FStorage));
    OleCheck(OleLoad(FStorage, IOleObject, Self, FOleObject));
    FDrawAspect := Header.DrawAspect;
    InitObject;
    UpdateView;
  except
    if DataHandle <> 0 then GlobalFree(DataHandle);
    DestroyObject;
    raise;
  end;
end;}


procedure Register;
begin
  RegisterComponents('System', [TOLeContainerext]);
end;

end.
