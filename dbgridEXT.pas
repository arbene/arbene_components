unit dbgridEXT;

interface

uses
	Windows, Messages, SysUtils,   Controls, Forms, Dialogs,
	StdCtrls, DBCtrls, extctrls,
  DBGrids, DB, DBTables, Grids, WinTypes, Classes, Graphics,comctrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZConnection,variants;
type
	TDBgridEXT = class(TDBgrid)

	private
	  { Private-Deklarationen }
		fkeine_farbe:boolean;
		fdatverzeichnis: string;
		fonLButtonDown : tnotifyevent;
		fonlbuttongehalten: tnotifyevent;
		fonrButtonDown: tnotifyevent;
		fonrButtonUp: tnotifyevent;
		fEingabetext: string;
		fZeitdiff_L_B: integer;
		fdarf_ziehen: boolean;
		fmysqlquery: tzquery;
		zeit: ttimer;
		timebeginn: tdatetime;
		bmp_cache:tstringlist;

		procedure WmLbuttondown( var parm : TWMlbuttondown) ; message WM_LButtondown;
		procedure WmLbuttonup( var parm : TWMlbuttonup) ; message WM_LBUTTONup ;
		procedure WmRbuttondown( var parm : TWMrbuttondown) ; message WM_RButtondown;
		procedure WmRbuttonUp( var parm : TWMrbuttonup) ; message WM_RButtonup;
		procedure zeitereignis(sender: tobject);
		procedure bmp_in_cache(nummer:integer;bmp:tbitmap);
		function  bmp_aus_cache(nummer:integer):tbitmap;
 protected
   {procedure DrawDataCell(const Rect: TRect;
      Field: TField; State: TGridDrawState); override;}
   procedure drawcolumncell(const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
	  { Protected-Deklarationen }

	public
	  { Public-Deklarationen }

	  destructor Destroy; override;
	  constructor  Create(Owner: TComponent); override;

	published
	  { Published-Deklarationen }
	  property DefaultDrawing default False;
	  property keine_farbe:boolean read fkeine_farbe write fkeine_farbe;
	  property Eingabetext: string  read fEingabetext write fEingabetext;
	  property Zeitdiff_L_B: integer read fZeitdiff_l_B write fZeitdiff_l_B;
	  property OnLButtonDown : tnotifyevent read fonLButtonDown write fonLButtonDown;
	  property OnLbuttongehalten: tnotifyevent read fonlbuttongehalten write fonlbuttongehalten;
	  property OnRButtonDown : tnotifyevent read fonrButtonDown write fonrButtonDown;
	  property OnRButtonUp : tnotifyevent read fonrButtonUp write fonrButtonUp;
    property darf_ziehen: boolean read fdarf_ziehen write fdarf_ziehen;
    property Bmp_Verzeichnis: string read fdatverzeichnis write fdatverzeichnis;
    property Bmp_mysqlquery: tzquery read fmysqlquery write fmysqlquery;
	  procedure keydown(var key: word; shift: tshiftstate) ; override;
	  procedure keypress(var key: char); override;
	end;

procedure Register;

implementation


constructor tdbgridext.create( owner: tcomponent);
begin
inherited create(owner);
//DefaultDrawing := false;
timebeginn:=0;
zeit:=ttimer.Create(self);
zeit.interval:=Zeitdiff_l_B;
zeit.enabled:=false;
zeit.OnTimer:=zeitereignis;
bmp_cache:=tstringlist.create;
end;

destructor tdbgridext.destroy;
var i:integer;
begin
zeit.free;
for i:=0 to bmp_cache.Count-1 do bmp_cache.Objects[i].free;
bmp_cache.Free;
inherited destroy;

end;



procedure tdbgridext.drawcolumncell(const Rect: TRect; DataCol: Integer;
		Column: TColumn; State: TGridDrawState);
var
field: tfield;
bimp:tbitmap;
stream: tstream;
brush: tbrush;
datei,t:string;
textsize:tsize;
f:integer;
begin  //1

if column.Field=nil then exit;
 brush:=canvas.Brush;
with Canvas do
begin   //2
	  field:=column.Field;

    if (datalink.dataset.findfield('storno')<>nil) and (brush.Color<>clHighlight) then
    begin  //3
    	try   //4
        case  datalink.dataset.findfield('storno').asinteger of //5
				 0: brush.color:=self.color;
				 1: brush.Color:=clsilver	;
        end;   //5
		except
		end;     //4
	  end;      //3


    //if (datalink.dataset.findfield('farbe')<>nil) and (brush.Color<>clHighlight) and (not fkeine_farbe) and (brush.Color<>clsilver) then     //storno geht vor
    if (datalink.dataset.findfield('farbe')<>nil)  and (not fkeine_farbe) and (brush.Color<>clsilver) then     //storno geht vor
    begin  //3
    	try   //4
        if datalink.DataSet['farbe']<>null then
        begin
          f:=datalink.DataSet['farbe'];
          if (brush.Color=clHighlight) then
          begin
          	if (f>0) then brush.Color:=clfuchsia;
          end
          else
			 case  f of //5
				 0: brush.color:=self.color;
				 1: brush.Color:=clred;
				 2: brush.Color:=clyellow;
				 3: brush.Color:=clfuchsia;
				 4: brush.Color:=claqua;
         5: brush.Color:=TColor( $FDF5E6); //wei�
         6: brush.Color:=Tcolor($FFDAB9); //hellorange
         7:brush.Color:=Tcolor($9AFF9A); //hellgr�n
			 else
				 brush.Color:=datalink.DataSet['farbe'];
			 end;   //5
        end;
		except
		end;     //4
	  end;      //3
	  //if datalink.ActiveRecord=2 then brush.Color:=clred;


    if (datalink.dataset.findfield('archiv')<>nil)and (datalink.dataset.findfield('archiv').asstring ='X')  and (brush.Color<>clHighlight) then
        brush.Color:=clsilver	;
   if fkeine_farbe then brush.Color:=clsilver;


	  FillRect(Rect);

    //if (datalink.DataSet['nummer']<>null) and
    if (lowercase(column.FieldName)='bmp_pfad') then //in bmp ist ein Verweis auf ein bmp gespeichert.
    begin   //3
        try  //4
          bimp := TBitmap.Create;
          if field.value<>null then datei:=field.value else datei:='';
          if fdatverzeichnis<>null then datei:=trim(fdatverzeichnis)+'\'+datei;
          if fileexists(datei) then
          begin //5
				 bimp.LoadFromFile(datei);
                 Draw(Rect.Left, Rect.Top, bimp);
          end;  //5
        finally
          bimp.Free;
			  canvas.Brush:=brush;
			end;   //4
        exit;
    end; //3

    if (lowercase(column.FieldName)='bmp_nr') then //in bmp ist ein Verweis auf ein bmp gespeichert.
	  begin   //3
			if (not assigned(fmysqlquery)) or (field.value=null) then exit;
			bimp:=bmp_aus_cache(field.value);
			if bimp<>nil then Draw(Rect.Left, Rect.Top, bimp)
			else //noch nicht in der liste
			try  //4
			  bimp := TBitmap.Create;
			  begin //5
				 if fmysqlquery.Locate('nummer',field.value,[]) then
				 begin
					try
					 stream:=fmysqlquery.CreateBlobStream(fmysqlquery.FieldByName('bmp'),bmRead);
					 bimp.LoadFromStream(stream) ;
					 Draw(Rect.Left, Rect.Top, bimp);
					 bmp_in_cache(field.value,bimp);
					finally
						stream.Free;
					end;
				  end;
			  end;  //5
			finally
			  bimp.Free;
			end;   //4
			exit;
	  end; //3

	  if (lowercase(column.FieldName)='bmp') then // is TGraphicField then
	  begin       //3
			try      //4
			  bimp := TBitmap.Create;
			  bimp.Assign(Field);
			  Draw(Rect.Left, Rect.Top, bimp);
			finally
			  bimp.Free;
			end ;    //4
	  end  //3
	  else
	  begin
		 textsize:=TextExtent(Field.Text);

     t:=field.AsString;
		 case column.Alignment of
		 taleftJustify: TextOut(Rect.Left, Rect.Top, t);
		 taCenter:		 TextOut(Rect.Left, Rect.Top, t);
		 taRightJustify:TextOut(rect.Right-textsize.cx, Rect.Top, t);
		 end;
	  end;
end;
canvas.Brush:=brush;
end;           //1



procedure tdbgridext.WmLbuttondown( var parm : TWMlbuttondown);
begin
	 //Starten des Timers, der nach 400 ms pr�ft ob die Taste immer noch gedr�ckt ist.
	timebeginn:=time();
	inherited;
	zeit.interval:=Zeitdiff_l_B;
	zeit.enabled:=true;
	if assigned(fonlbuttondown) then fonlbuttondown(self);

end;

procedure tdbgridext.Wmrbuttondown( var parm : TWMrbuttondown);
begin
	inherited;
	if assigned(fonrbuttondown) then fonrbuttondown(self);
end;

procedure tdbgridext.Wmlbuttonup( var parm : TWMlbuttonup);
begin
	zeit.enabled:=false; // Timer ausschalten
	inherited;
end;

procedure tdbgridext.Wmrbuttonup( var parm : TWMrbuttonUp);
begin
	inherited;
	if assigned(fonrbuttonup) then fonrbuttonup(self);
end;


procedure tdbgridext.keypress(var key: char);
var ordkey: integer;
begin
ordkey:=ord(key);
case ordkey of
	48..256 : fEingabetext:=fEingabetext+key ; //Umlaute
end;
inherited;
end;


procedure tdbgridext.keydown(var key: word; shift: tshiftstate);
begin
case key of
			8: fEingabetext:=copy(fEingabetext,0,length(fEingabetext)-1) ;// backspace
			13: ; //enter
			46: if ssShift in shift then feingabetext:=''; //del
			33..40: feingabetext:='';//Cursortasten

			end;
inherited;

end;


procedure tdbgridext.zeitereignis(sender: tobject);
var
differenz : tdatetime;
Hour, Min, Sec, MSec: word;
begin

	 zeit.Enabled:=false;  //Timer ausschalten
	 differenz:= (time()-timebeginn);
	 DecodeTime(differenz, Hour, Min, Sec, MSec);
	 if  (sec*1000+msec < zeit.Interval+70 ) and assigned(fonlbuttongehalten) then
	 if fdarf_ziehen then fonlbuttongehalten(self);

end;


procedure tdbgridext.bmp_in_cache(nummer:integer;bmp:tbitmap);
var
	bitmap:tbitmap;
	i:integer;
begin
	if bmp_cache.Count>12 then
	begin
		i:=random(bmp_cache.count);
		bmp_cache.Objects[i].free;
		bmp_cache.Delete(i);
	end;
	bitmap:=tbitmap.Create;
	bitmap.Assign(bmp);
	bmp_cache.AddObject(inttostr(nummer), bitmap);
end;


function  tdbgridext.bmp_aus_cache(nummer:integer):tbitmap;
var
index:integer;
begin
	index:=bmp_cache.indexof(inttostr(nummer));
	if index>=0 then result:=tbitmap(bmp_cache.objects[index]) else
	result:=nil;

end;


procedure Register;
begin
	RegisterComponents('Datensteuerung', [TDBgridEXT]);
end;

end.
