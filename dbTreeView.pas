unit dbTreeView;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons,  Mask,
	Menus, ImgList,db,
	listen, Spin, vcfi, registry,
	CheckLst, ExtDlgs,math,  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZConnection,variants;//{, ZTransact, ZMySqlTr, ZConnect, ZMySQLCon};


type
	TdbTreeView = class(TTreeView)
	private
	  { Private-Deklarationen }
		//TDragOverEvent = procedure(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean) of object;
		ftabelle_bef_vorlage,ftabelle_quelle:tzquery;
     ffeldname, ftabelle_quelle_name:string;
		ftabellen: array[1..12] of tzquery;
		//frubriken:array[1..9] of string;
		flookup_feld: array[1..12] of string;
		flookup_key: array[1..12] of string;
		flookup_tab:array[1..12] of tzquery;
		frubrik_namen: array [1..12] of string;
		frubrik,febene: integer;
		fnummer:int64;
		fstorno:boolean;
     fkopfrubrik1_name:string;
     fkopfrubrik1_rubrik:integer;
		fparentfeld, fkindfeld:string;

		function getbigint(tabelle: tdataset;feldname:string):int64;
		//procedure setbigint(tabelle: tzzdataset;feldname:string;wert:int64);
		function gehezudatum(datum: tdate): ttreenode;
		function gehezurubrik(tab: tzquery;datum: tdate; rubrik: integer;i_num:int64):ttreenode;
		function befunde_lesen(knoten:ttreenode;nummer:int64):ttreenode;
     function kopfrubrik(r_name:string;rubrik:integer):ttreenode;
	protected
	  { Protected-Deklarationen }


	public
	  { Public-Deklarationen }
	  change_allowed,aktiv:boolean;
	  oldnummer:int64;
	  oldrubrik:integer;
	  constructor create(AOwner: TComponent); override;
	  destructor Destroy; override;
	  //procedure neu(s_text:string;command:integer);
	  //procedure loeschen;
	  procedure liste_lesen;
	  procedure liste_loeschen;
	  procedure markieren(sindex:integer);
	  procedure alles_markieren(sindex:integer);
	  procedure alles_demarkieren;
	  //procedure tabelle_feld_abgleich;
	  procedure Change(Node: TTreeNode);override; //der knoten hat sich ge�nder die Tabelle muss akt. weden.
	  procedure name_neu(node:ttreenode);
	  procedure hoch;
	  procedure runter;
	  function  gehe_zu_datensatz(rubrik:integer;nummer:int64): boolean;
    function gehe_zu_nummer_bereich(nummer,bereich:int64): boolean;
	  function  einhaengen(tab: tzquery;nummer:int64;rubrik,status:integer):ttreenode;
	  procedure knoten_entfernen(knoten:ttreenode);
	  procedure knoten_neuordnen(knoten:ttreenode);
	  procedure Expand_e1;
	  procedure markiere_parent(knoten:ttreenode);
	  procedure statindex(sindex:integer);
	  function  suche_datensatz(wort:string): boolean;
	  function  first_date:tdate;
	  function  last_date:tdate;

	published
	  { Published-Deklarationen }
		property kopfrubrik1_name:string read fkopfrubrik1_name write fkopfrubrik1_name;
		property kopfrubrik1_rubrik:integer read fkopfrubrik1_rubrik write fkopfrubrik1_rubrik;
		property a_rubrik: integer read frubrik write frubrik;
		property a_ebene: integer read febene write febene;
		property a_nummer:int64 read fnummer write fnummer;
		property a_storno: boolean read fstorno write fstorno;
		property mysqlquery_be_feld:string read ffeldname write ffeldname;
		property mysqlquery_bef_vorlage: tzquery read ftabelle_bef_vorlage write ftabelle_bef_vorlage;
		property mysqlquery_bef_quelle_name: string read  ftabelle_quelle_name write ftabelle_quelle_name;
		property mysqlquery_bef_quelle: tzquery read ftabelle_quelle write ftabelle_quelle;
		property mysqlquery1: tzquery read ftabellen[1] write ftabellen[1];
		property mysqlquery2: tzquery read ftabellen[2] write ftabellen[2];
		property mysqlquery3: tzquery read ftabellen[3] write ftabellen[3];
		property mysqlquery4: tzquery read ftabellen[4] write ftabellen[4];
		property mysqlquery5: tzquery read ftabellen[5] write ftabellen[5];
		property mysqlquery6: tzquery read ftabellen[6] write ftabellen[6];
		property mysqlquery7: tzquery read ftabellen[7] write ftabellen[7];
		property mysqlquery8: tzquery read ftabellen[8] write ftabellen[8];
		property mysqlquery9: tzquery read ftabellen[9] write ftabellen[9];
		property mysqlquery10: tzquery read ftabellen[10] write ftabellen[10];
		property mysqlquery11: tzquery read ftabellen[11] write ftabellen[11];
		property mysqlquery12: tzquery read ftabellen[12] write ftabellen[12];
		property mysqllookup_tab1: tzquery read flookup_tab[1] write flookup_tab[1];
		property mysqllookup_tab2: tzquery read flookup_tab[2] write flookup_tab[2];
		property mysqllookup_tab3: tzquery read flookup_tab[3] write flookup_tab[3];
		property mysqllookup_tab4: tzquery read flookup_tab[4] write flookup_tab[4];
		property mysqllookup_tab5: tzquery read flookup_tab[5] write flookup_tab[5];
		property mysqllookup_tab6: tzquery read flookup_tab[6] write flookup_tab[6];
		property mysqllookup_tab7: tzquery read flookup_tab[7] write flookup_tab[7];
		property mysqllookup_tab8: tzquery read flookup_tab[8] write flookup_tab[8];
		property mysqllookup_tab9: tzquery read flookup_tab[9] write flookup_tab[9];
		property mysqllookup_tab10: tzquery read flookup_tab[10] write flookup_tab[10];
		property mysqllookup_tab11: tzquery read flookup_tab[11] write flookup_tab[11];
		property mysqllookup_tab12: tzquery read flookup_tab[12] write flookup_tab[12];
		property mysqllookup_key1: string read flookup_key[1] write flookup_key[1];
		property mysqllookup_key2: string read flookup_key[2] write flookup_key[2];
		property mysqllookup_key3: string read flookup_key[3] write flookup_key[3];
		property mysqllookup_key4: string read flookup_key[4] write flookup_key[4];
		property mysqllookup_key5: string read flookup_key[5] write flookup_key[5];
		property mysqllookup_key6: string read flookup_key[6] write flookup_key[6];
		property mysqllookup_key7: string read flookup_key[7] write flookup_key[7];
		property mysqllookup_key8: string read flookup_key[8] write flookup_key[8];
		property mysqllookup_key9: string read flookup_key[9] write flookup_key[9];
		property mysqllookup_key10: string read flookup_key[10] write flookup_key[10];
		property mysqllookup_key11: string read flookup_key[11] write flookup_key[11];
		property mysqllookup_key12: string read flookup_key[12] write flookup_key[12];
		property mysqllookup_feld1: string read flookup_feld[1] write flookup_feld[1];
		property mysqllookup_feld2: string read flookup_feld[2] write flookup_feld[2];
		property mysqllookup_feld3: string read flookup_feld[3] write flookup_feld[3];
		property mysqllookup_feld4: string read flookup_feld[4] write flookup_feld[4];
		property mysqllookup_feld5: string read flookup_feld[5] write flookup_feld[5];
		property mysqllookup_feld6: string read flookup_feld[6] write flookup_feld[6];
		property mysqllookup_feld7: string read flookup_feld[7] write flookup_feld[7];
		property mysqllookup_feld8: string read flookup_feld[8] write flookup_feld[8];
		property mysqllookup_feld9: string read flookup_feld[9] write flookup_feld[9];
		property mysqllookup_feld10: string read flookup_feld[10] write flookup_feld[10];
		property mysqllookup_feld11: string read flookup_feld[11] write flookup_feld[11];
		property mysqllookup_feld12: string read flookup_feld[12] write flookup_feld[12];
		property mysql_rubrik1: string read frubrik_namen[1] write frubrik_namen[1];
		property mysql_rubrik2: string read frubrik_namen[2] write frubrik_namen[2];
		property mysql_rubrik3: string read frubrik_namen[3] write frubrik_namen[3];
		property mysql_rubrik4: string read frubrik_namen[4] write frubrik_namen[4];
		property mysql_rubrik5: string read frubrik_namen[5] write frubrik_namen[5];
		property mysql_rubrik6: string read frubrik_namen[6] write frubrik_namen[6];
		property mysql_rubrik7: string read frubrik_namen[7] write frubrik_namen[7];
		property mysql_rubrik8: string read frubrik_namen[8] write frubrik_namen[8];
		property mysql_rubrik9: string read frubrik_namen[9] write frubrik_namen[9];
		property mysql_rubrik10: string read frubrik_namen[10] write frubrik_namen[10];
		property mysql_rubrik11: string read frubrik_namen[11] write frubrik_namen[11];
		property mysql_rubrik12: string read frubrik_namen[12] write frubrik_namen[12];



	  //procedure Changing(Node: TTreeNode; var AllowChange: Boolean); override;

	end;

type
		pnodepointer=^nodewert;
		nodewert = record
		nummer: int64;
		bereich:int64;
    ebene:int64;
    rubrik:int64;
		//ebene:integer;
    //rubrik:integer;
    datum:tdate;
		mysqlquery:tzquery;
end;

procedure Register;

implementation

constructor TdbTreeView.create(AOwner: TComponent);
begin
inherited;
change_allowed:=true;

end;

destructor TdbTreeView.Destroy;
begin
liste_loeschen;
inherited;
end;

procedure TdbTreeView.liste_loeschen;
var i:integer;
daten:pnodepointer;
begin
change_allowed:=false;
	for i:=0 to items.Count-1 do
	begin
		daten:=items[i].data;
		dispose(daten);
	end;
	items.Clear;
change_allowed:=true;
end;

procedure TdbTreeView.liste_lesen;
var
t,stat: integer;
tab:tzquery;
begin
change_allowed:=false;
try
liste_loeschen;
if fkopfrubrik1_rubrik>12 then
begin
kopfrubrik(fkopfrubrik1_name,fkopfrubrik1_rubrik);
end;
for t:=1 to 12 do
begin
try
	if ftabellen[t]<>nil then
	begin
	  tab:=ftabellen[t];
	  tab.tag:=2;
	  tab.first;

	  while ((not tab.eof) {and (getbigint(tab,'nummer')<>0)}) do
	  begin
		  //aktu_untersuchung

		  if tab.FindField('i_status')<> nil then stat:=tab['i_status'] else stat:=-1;
      if  (tab.FindField('grenzwertindikator')<> nil) then
      begin
        if tab.findfield('grenzwertindikator').asstring<>'' then stat:= 7 ;    //sonst wie oben auff�lliges labor
      end;
		  einhaengen(tab,getbigint(tab,'nummer'),t ,stat);
		  tab.next;
	 end;

	end;
finally
tab.tag:=0;
end;
end;


finally
change_allowed:=true;
end;
end;

function TdbTreeView.kopfrubrik(r_name:string;rubrik:integer):ttreenode;
var
		 knoten: ttreenode;
		 daten:pnodepointer;
		 //anzeigename:string;
		 storno:boolean;
begin
  knoten:=Items. AddFirst(nil,r_name);      //tree_bereich
	new(daten);
	daten^.rubrik:=rubrik;
  daten^.datum:=strtodate('01.01.9999');
  daten^.ebene:=0;
  knoten.data:=daten;
	//if storno then knoten.stateindex:=9 else
	knoten.stateindex:=rubrik;
	//knoten.ImageIndex:=status;
	//knoten.selectedindex:=status;
	result:=knoten;
end;

function TdbTreeView.gehezudatum(datum: tdate):ttreenode;
var
daten:pnodepointer;
//knoten:ttreenode;
procedure neuesdatum_davor;
begin
	result:=Items.Insert(result, datetostr(datum));      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=0;
	daten^.bereich:=0;
	daten^.ebene:=0;
	daten^.rubrik:=0;
	result.Data:=daten;
	result.imageIndex:=-1;
	result.SelectedIndex:=-1;
	result.StateIndex:=-1;
end;

procedure neuesdatum_dahinter;
begin
	result:=Items.add(result, datetostr(datum));      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=0;
	daten^.bereich:=0;
	daten^.ebene:=0;
	daten^.rubrik:=0;
	result.Data:=daten;
	result.imageIndex:=-1;
	result.selectedindex:=-1;
	result.StateIndex:=-1;
end;

begin //main
if items.count=0 then
begin
 result:=nil;
 neuesdatum_dahinter;
 exit;
end;

result:=items[0];

while true  do
begin
	 if result=nil then  //hinten angekommen
	 begin
	  neuesdatum_dahinter;
	  exit;
	 end;
	 daten:=result.Data;
	 if daten^.datum =datum then exit;
	 if daten^.datum <datum then
		begin
			neuesdatum_davor;
			exit;
		end;
	 result:=result.getnextSibling ;
end;
end;


function TdbTreeView.gehezurubrik(tab: tzquery;datum: tdate; rubrik: integer;i_num:int64):ttreenode;
var
daten:pnodepointer;
knoten: ttreenode;
//knoten:ttreenode;
procedure neuerubrik_child;
begin
	result:=Items.AddChildFirst(knoten,frubrik_namen[rubrik] );      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=i_num;
	daten^.bereich:=0;
	daten^.ebene:=1;
	daten^.rubrik:=rubrik;
	daten^.mysqlquery:=tab;
	result.Data:=daten;
	if tab['storno']=1 then result.stateindex:=9 else result.stateindex:=rubrik;
	result.ImageIndex:=-1;
	result.SelectedIndex:=-1;
end;

procedure neuerubrik_davor;
begin
	result:=Items.Insert(result,frubrik_namen[rubrik] );      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=i_num;
	daten^.bereich:=0;
	daten^.ebene:=1;
	daten^.rubrik:=rubrik;
	result.Data:=daten;
	daten^.mysqlquery:=tab;
	result.stateindex:=rubrik;
	result.ImageIndex:=-1;
	result.selectedindex:=-1;
end;

procedure neuerubrik_dahinter;
begin
	result:=Items.add(knoten, frubrik_namen[rubrik]);      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=i_num;
	daten^.bereich:=0;
	daten^.ebene:=1;
	daten^.rubrik:=rubrik;
	result.Data:=daten;
	daten^.mysqlquery:=tab;
	result.stateindex:=rubrik;
	result.ImageIndex:=-1;
	result.selectedindex:=-1;
end;

begin //main
knoten:=gehezudatum(datum);
if not knoten.HasChildren then
	 begin
		neuerubrik_child;
		exit;
	 end;

result:=knoten.getFirstChild;

while true  do
begin
	 if result=nil then
	 begin
	  neuerubrik_dahinter;
	  exit;
	 end;

	 daten:=result.Data;
	 if daten^.rubrik =rubrik then exit;
	 if daten^.rubrik >rubrik then
		begin
			neuerubrik_davor;
			exit;
		end;
	 knoten:=result;
	 result:=result.getNextSibling ;

end;
end;


function TdbTreeView.einhaengen(tab: tzquery;nummer:int64;rubrik,status:integer):ttreenode;
var
		 lookupwert: integer;
		 knoten,child,child_last, parent: ttreenode;
		 daten:pnodepointer;
		 sdatum,anzeigename:string;
		 datum:tdate;
		 lookuptab:tzquery;
		 storno:boolean;

procedure new_child(ebene:integer);
begin
	knoten:=Items.AddChildFirst(parent,anzeigename);      //tree_bereich
	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
	daten^.ebene:=ebene;
	daten^.datum:=tab['datum'];
	daten^.rubrik:=rubrik;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	if storno then knoten.stateindex:=9 else
	knoten.stateindex:=rubrik;
	knoten.ImageIndex:=status;
	knoten.selectedindex:=status;
	result:=knoten;
end;

procedure new_sibling_insert(ebene:integer);
begin
	knoten:=Items.Insert(child_last,anzeigename);      //tree_bereich child_last
	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
	daten^.ebene:=ebene;
	daten^.datum:=tab['datum'];
	daten^.rubrik:=rubrik;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	if storno then knoten.stateindex:=9 else
	knoten.stateindex:=rubrik;
	knoten.ImageIndex:=status;
	knoten.selectedindex:=status;
	result:=knoten;
end;

procedure new_sibling_add(ebene:integer);
begin
	knoten:=Items.AddChild(parent,anzeigename);      //tree_bereich
	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
	daten^.ebene:=ebene;
	daten^.datum:=tab['datum'];
	daten^.rubrik:=rubrik;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	if storno then knoten.stateindex:=9 else
	knoten.stateindex:=rubrik;
	knoten.ImageIndex:=status;
	knoten.selectedindex:=status;
	result:=knoten;
end;


begin  //main
try
  change_allowed:=false;
	fparentfeld:='';
	fkindfeld:='';

	lookuptab:=flookup_tab[rubrik];
	anzeigename:=flookup_feld[rubrik];
	if lookuptab<>nil then
	begin
		if flookup_key[rubrik]<>'' then
		begin
			lookupwert:=tab[flookup_key[rubrik]];
			lookuptab.Locate('nummer',lookupwert,[]);
        if (lookuptab[flookup_feld[rubrik]]<>null) then anzeigename:=lookuptab[flookup_feld[rubrik]] else anzeigename:='';
		end
		else if lookuptab[flookup_feld[rubrik]]<>null then anzeigename:=lookuptab[flookup_feld[rubrik]] else anzeigename:='';  //selbe tabelle

	end
	else if trim(flookup_key[rubrik])<>'' then anzeigename:=tab[flookup_key[rubrik]];

	sdatum:=datetostr(tab['datum']);
	datum:=tab['datum'];
	storno:=tab['storno']=1;

	case rubrik of
	1:
	begin
		parent:=gehezurubrik(tab,datum, rubrik,nummer);
		result:= befunde_lesen(parent,nummer);
		exit;
	end;
	2:parent:=gehezudatum(datum); //vorsorge
	3:parent:=gehezudatum(datum); //impfung
	4:                            //labor
	begin
      
		parent:=gehezurubrik(tab,datum, rubrik,nummer);
		child_last:=nil;
		if parent.HasChildren then
		begin
			child:=parent.getFirstChild;
			daten:=child.Data;

			while (child<>nil) and (daten^.rubrik<=rubrik+1) do //erster der n�chsten Rubrik
			begin
				child_last:=child;
				child:=parent.GetNextChild(child);
			end;
			if child=nil then new_sibling_add(2)
			else
				if child_last=nil then new_sibling_add(2) else new_sibling_insert(2);
		end
		else new_child(2);
		exit;
	end;
	5:parent:=gehezudatum(datum);
	6:parent:=gehezudatum(datum);
	7:parent:=gehezudatum(datum);
	8:parent:=gehezudatum(datum);
	9:parent:=gehezudatum(datum);
	10:parent:=gehezudatum(datum);
	11:parent:=gehezudatum(datum);
	12:parent:=gehezudatum(datum);

	end;

	// gehe zum letzten der Rubrik
	child_last:=nil;
	if parent.HasChildren then
	begin
		child:=parent.getFirstChild; //besser w�re getlastchild 20141127
		daten:=child.Data;
    child_last:=child;
		while (child<>nil) and (daten^.rubrik<rubrik+1) do //erster der n�chsten Rubrik
		begin
			child_last:=child;
			child:=parent.GetNextChild(child);
		end;
		if child=nil then new_sibling_add(1)
		else
		if child_last=nil then new_sibling_add(1) else new_sibling_insert(1);

	end
	else new_child(1);
finally
change_allowed:=true;
end;
end;



function tdbtreeview.befunde_lesen(knoten:ttreenode;nummer:int64):ttreenode;
var
		 i_parent: integer;
		 child, child_first, parent: ttreenode;
		 daten:pnodepointer;
		 query:string;

function gehezu_parent(i_parent:integer):ttreenode;
begin
result:=child_first;
daten:=result.data;
while (result<>nil) do
begin
	daten:=result.data;
	if daten^.rubrik<>1 then
		begin
			result:=nil;
			exit;
		end;
	if daten^.bereich=i_parent then exit;
	result:=result.getnext;
end;
end;

procedure knotenmarkieren(knoten:ttreenode);

begin
	daten:=knoten.data;
  
	if not ftabelle_quelle.Locate('i_untersuchung,i_bereich',
				vararrayof([daten^.nummer,inttostr(daten^.bereich)]),[]) then exit;         //120523 inttostr(daten...
	knoten.StateIndex:=ftabelle_quelle['auffaellig']+13;//+10;//+9
  if (ftabelle_quelle.FieldByName('ole')<>nil) and (ftabelle_quelle.FieldByName('ole').asstring<>'') then
  begin
       knoten.ImageIndex:=6;
       knoten.SelectedIndex:=6;
  end;

	markiere_parent(knoten);
end;

var
and_storno:string;
begin //main #################################################################
try

	 query:=format('select * from %s where i_untersuchung=%s and storno=0' ,
			[ftabelle_quelle_name,inttostr(nummer)]);
	 ftabelle_quelle.Close;
	 ftabelle_quelle.Sql.Text:=query;
	 ftabelle_quelle.Open;


	fparentfeld:='';
	fkindfeld:='';
	ftabelle_bef_vorlage.first;

	if  ftabelle_bef_vorlage.Eof then exit;
	//erster datensatz von hand
	child_first:=Items.AddChildFirst(knoten,ftabelle_bef_vorlage[ffeldname]);      //tree_bereich

	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=getbigint(ftabelle_bef_vorlage,'nummer');
	daten^.rubrik:=1;
	daten^.ebene:=2;
	child_first.Data:=daten;

  if ftabelle_bef_vorlage['parent']=0 then child_first.imageIndex:=5 else child_first.imageIndex:=-1;
  child_first.SelectedIndex:=child_first.imageIndex;
  child_first.StateIndex:=-1;
	result:=child_first;
	knotenmarkieren(child_first);
	ftabelle_bef_vorlage.Next;

	while not ftabelle_bef_vorlage.eof do
	begin
		 i_parent:= ftabelle_bef_vorlage['parent'];
		 parent:=gehezu_parent(i_parent);
		 if parent<>nil then
         begin
			 child:=Items.Addchild(parent, ftabelle_bef_vorlage[ffeldname]);
         end
		 else
         begin
			 child:=Items.Addchild(knoten, ftabelle_bef_vorlage[ffeldname]);
         end;
        new(daten);
        daten^.nummer:=nummer;
        daten^.bereich:=getbigint(ftabelle_bef_vorlage,'nummer');
        daten^.rubrik:=1;
        daten^.ebene:=2;
        child.Data:=daten;
        child.stateindex:=-1;
        if ftabelle_bef_vorlage['parent']=0 then child.imageIndex:=5 else child.imageIndex:=-1;
        child.selectedindex:=child.imageIndex;
        knotenmarkieren(child);
        ftabelle_bef_vorlage.Next;
	 end;


	 if fstorno then // gel�schte Datens�tze
	 begin
		query:=format('select * from %s where i_untersuchung=%s and storno="1"' ,
			  [ftabelle_quelle_name,inttostr(nummer),and_storno]);
		ftabelle_quelle.Close;
		ftabelle_quelle.Sql.Text:=query;
		ftabelle_quelle.Open;
		ftabelle_quelle.first;
		while not ftabelle_quelle.eof do
		begin

			ftabelle_quelle.next;
		end;
	 end;
finally

end;
end;


procedure tdbtreeview.knoten_neuordnen(knoten:ttreenode);
var daten:pnodepointer;
	  tab: tzquery;
	  nummer:int64;
	  rubrik:integer;
begin
	if knoten=nil then exit;
	daten:=knoten.data;
	tab:=daten^.mysqlquery;
	nummer:=daten^.nummer;
	rubrik:=daten^.rubrik;
	if tab.State in [dsedit, dsinsert] then
	begin
		tab.Post;
	end;
	knoten_entfernen(knoten);
	einhaengen( tab,nummer, rubrik,-1);

end;

procedure tdbtreeview.knoten_entfernen(knoten:ttreenode);
var daten:pnodepointer;
	  parent:ttreenode;
begin
	daten:=knoten.data;
	dispose(daten);
	parent:=knoten.Parent;
	knoten.Free;
	if parent=nil then exit;
	if (not parent.HasChildren) then knoten_entfernen(parent);
end;




procedure TdbTreeView.Change(Node: TTreeNode);
var
nummer,bereich: int64;
rubrik:integer;
daten:pnodepointer;
query:string;
//and_storno:string;
begin
//if fstorno then and_storno:='' else and_storno:=' and storno =""';
//inherited;
try
	 if (not change_allowed) or (node=nil) then exit;
   frubrik:=-1;
   febene:=-1;
	 begin
		daten:=node.Data;
		nummer:=daten^.nummer;
		bereich:=daten^.bereich;
		rubrik:=daten^.rubrik;

		if daten^.ebene>=1 then
		begin
		  if not ftabellen[rubrik].active then exit;
		  if rubrik=1 then  //aktuntersuchung
		  begin

			 //ftabellen[1].locate('nummer',inttostr(nummer),[]); // q_befunde
       ftabellen[1].locate('nummer',(nummer),[]); // q_befunde
			 //ftabelle_bef_vorlage.Locate('nummer',inttostr(bereich),[]);//befundvorlage
       ftabelle_bef_vorlage.Locate('nummer',(bereich),[]);//befundvorlage
			 query:=format('select * from %s where i_untersuchung=%s and i_bereich=%s and storno=0' ,
			[	ftabelle_quelle_name,inttostr(nummer),inttostr(bereich)]); //akt_untersuchung
			ftabelle_quelle.Close;
			ftabelle_quelle.Sql.Text:=query;
			ftabelle_quelle.Open;
		  end
		  else
			 //ftabellen[rubrik].Locate('nummer',inttostr(nummer),[]);
       ftabellen[rubrik].Locate('nummer',(nummer),[]);
		end;
     
		frubrik:=rubrik;
		fnummer:=daten^.nummer;
		febene:=daten^.ebene;
		node.Expand(false);
	 end;

finally
inherited;
end
end;

procedure tdbtreeview.name_neu(node:ttreenode);
var
nummer,bereich: int64;
rubrik:integer;
daten:pnodepointer;
lookuptab:tzquery;
anzeigename:string;
begin
{if node<>nil then  //funktioniert nicht richtig, keine priorit�t
begin
  daten:=node.Data;
  nummer:=daten^.nummer;
  bereich:=daten^.bereich;
  rubrik:=daten^.rubrik;
  case rubrik of
  2,3,4:
    begin
    lookuptab:=flookup_tab[rubrik];
    //anzeigename:=flookup_tab[rubrik][flookup_feld[rubrik]];
    anzeigename:=ftabellen[rubrik][frubrik_namen[rubrik]];
    node.Text:=anzeigename;
    end;
   end;
end;}
end;

procedure TdbTreeView.markieren(sindex:integer);
var i:integer;

function hasparent(p,f:integer):boolean;
	var j:integer;
	begin
		j:=f;
		while (items[j].parent<>nil) and(j>p) do
		begin
			j:=items[j].parent.absoluteindex;
		end;
		if j=p then result:=true else result:=false;

	end;

begin //main
	selected.StateIndex:=sindex;
	i:=selected.absoluteindex;
	//knoten:=selected;
	inc(i);
	//while (i<items.count) and(items[i].parent=selected) do
	while (i<items.count) and hasparent(selected.absoluteindex,i) do
	begin
	  items[i].StateIndex:=sindex;
	  inc(i);
	end;
end;


procedure TdbTreeView.alles_markieren(sindex:integer);
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=sindex;
	end;
end;

procedure TdbTreeView.statindex(sindex:integer);
begin
		selected.stateindex:=sindex;
end;

procedure TdbTreeView.alles_demarkieren;
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=-1;
	end;
end;

procedure TdbTreeView.runter;
begin
	if selected.absoluteindex >0 then  selected:=items[selected.absoluteindex-1];
end;

procedure TdbTreeView.hoch;
begin
	if selected.absoluteindex <items.count-1 then  selected:=items[selected.absoluteindex+1];
end;

function TdbTreeView.getbigint(tabelle: tdataset;feldname:string):int64;
var
r:string;
begin
try
if tabelle.findfield(feldname)<>nil then
	//tabelle.GetFieldData(tabelle.fieldbyname(feldname),@result);
  r:= tabelle.FindField(feldname).AsString;
except
end;
if r='' then result:=0 else result:=strtoint64(r);
end;

function TdbTreeView.gehe_zu_datensatz(rubrik:integer;nummer:int64): boolean;
var knoten: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
if items.count=0 then exit;
knoten:=items[0];
while knoten<> nil do
begin
	daten:=knoten.data;
	if (daten<>nil) and (daten^.nummer=nummer) and (daten^.rubrik=rubrik) then
	begin
	  selected:=knoten;
	  result:=true;
	  exit;
	end;
	knoten:=knoten.GetNext;
end;
except
end;
end;

function TdbTreeView.gehe_zu_nummer_bereich(nummer,bereich:int64): boolean;
var knoten: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
if items.count=0 then exit;
knoten:=items[0];
while knoten<> nil do
begin
	daten:=knoten.data;
	if (daten<>nil) and (daten^.nummer=nummer) and (daten^.bereich=bereich) then
	begin
	  selected:=knoten;
	  result:=true;
	  exit;
	end;
	knoten:=knoten.GetNext;
end;
except
end;
end;
procedure tdbtreeview.Expand_e1;
var knoten,sel: ttreenode;
begin
if items.count=0 then exit;
sel:=selected;
knoten:=items[0];
while knoten<> nil do
begin
	knoten.Expand(false);
	knoten:=knoten.getNextSibling;
end;
 if sel<>nil then selected:=sel else  selected:=items[0];
end;


procedure tdbtreeview.markiere_parent(knoten:ttreenode);
var
si,im:integer;
daten:pnodepointer;
begin
try
	si:=knoten.StateIndex;
  im:=knoten.ImageIndex;
	knoten:=knoten.Parent;
	while knoten<>nil do
	begin
	  daten:=knoten.Data;
	  if daten^.ebene<=1 then
    begin
     if im>0 then
     begin
         knoten.ImageIndex:=im;
         knoten.SelectedIndex:=im;
     end;
     exit; //datum, befund
    end;
	  if knoten.StateIndex<si then knoten.StateIndex:=si;
    if im>0 then
    begin
         knoten.ImageIndex:=im;
         knoten.SelectedIndex:=im;
    end;
	  knoten:=knoten.Parent;
	end;
except
end;
end;


function Tdbtreeview.suche_datensatz(wort:string): boolean;
var knoten,k_start: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
  result:=false;

  if (items.count<=1) or (wort='') then exit;
  wort:=lowercase(wort);

  if selected=nil then selected:=items[0];
  k_start:=selected;

  knoten:=selected.GetNext;
  if knoten=nil then
  begin
       knoten:= items[0];
  end;
  while knoten<> k_start do
  begin
    if  pos( wort,lowercase(knoten.Text))>0 then
    begin
      selected:=knoten;
      change(selected);
      result:=true;
      exit;
    end;
    knoten:=knoten.getNext;
    if knoten=nil then knoten:=items[0];
  end;
  except
  end;
end;

function  Tdbtreeview.first_date:tdate;
var
daten:pnodepointer;
knoten: ttreenode;
begin
knoten:=items[items.count-1];
daten:=knoten.Data;
result:=daten.datum;
end;

function  Tdbtreeview.last_date:tdate;
var
daten:pnodepointer;
knoten: ttreenode;
begin
knoten:=items[1];
daten:=knoten.Data;
result:=daten.datum;
end;


procedure Register;
begin
	RegisterComponents('Datensteuerung', [TdbTreeView]);
end;

end.
