unit dbtreeview_kaskade;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons,  Mask,
	Menus, ImgList,db,
	listen, Spin, vcfi, registry,
	CheckLst, ExtDlgs,math,  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZConnection,variants;

type
	Tdbtreeview_k = class(TTreeView)
	private
	  { Private-Deklarationen }
		//TDragOverEvent = procedure(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean) of object;
		ftabelle_bef_vorlage,ftabelle_quelle:tzquery;
     ffeldname, ftabelle_quelle_name:string;
		ftabellen: array[1..6] of tzquery;
		fanzeige_feld: array[1..6] of string;
		fkey: array[1..6] of string;
		febene: integer;
		fnummer:int64;
		fstorno:boolean;
    fall:boolean;
    fzconnection:TZconnection;
		fparentfeld, fkindfeld:string;
     ftab_v,fa_tab:tzquery;
     fkey_v,fanzeige_feld_v,febene_feld_v:string;

     function tab_nr(tab:tzquery):integer;
		function getbigint(tabelle: tdataset;feldname:string):int64;
		function gehezudatum(datum: tdate): ttreenode;
		function befunde_lesen(knoten:ttreenode;nummer:int64):ttreenode;
     procedure varknoten_einfuegen(knoten:ttreenode;nr:int64;ebene:integer);
	protected
	  { Protected-Deklarationen }
    procedure DragOver( Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);override;
    procedure dragdrop( Source: TObject; X, Y: Integer);override;

	public
	  { Public-Deklarationen }
	  change_allowed,aktiv:boolean;
	  oldnummer:int64;
	  oldrubrik:integer;
    akt_knoten: array [1..6] of ttreenode;
	  constructor create(AOwner: TComponent); override;
	  destructor Destroy; override;
	  //procedure neu(s_text:string;command:integer);
	  //procedure loeschen;
    //procedure DragOver(Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean); override;
    //procedure DragDrop( Source: TObject;X, Y: Integer);override;
	  procedure liste_lesen(ebene:integer);
	  procedure liste_loeschen;
	  procedure markieren(sindex:integer);
	  procedure alles_markieren(sindex:integer);
	  procedure alles_demarkieren;
	  //procedure tabelle_feld_abgleich;
	  procedure Change(Node: TTreeNode);override; //der knoten hat sich ge�nder die Tabelle muss akt. weden.
    procedure name_neu(node:ttreenode);
	  procedure hoch;
	  procedure runter;
	  function  gehe_zu_datensatz(ebene:integer;nummer:int64): boolean;
    function gehe_zu_datensatz_zquery(zquery:tzquery;nummer:int64): boolean;
    function  suche_datensatz(wort:string): boolean;
    function  kind_einfuegen(knoten:ttreenode;tab:tzquery;nummer:int64;anzeigename:string;ebene,position:integer):ttreenode;
    procedure kinder_lesen(knoten:ttreenode;nr:int64;ebene:integer);
    function  parent_einfuegen(knoten:ttreenode;tab:tzquery;nummer:int64;anzeigename:string;ebene:integer):ttreenode;
	  function  knoten_entfernen(knoten:ttreenode):ttreenode;
	  procedure Expand_e1;
	  procedure markiere_parent(knoten:ttreenode);
	  procedure statindex(sindex:integer);
    function  get_index(tab_ind:integer):int64  ;

	published
	  { Published-Deklarationen }

    property zconnection: tzconnection read fzconnection write fzconnection ;
    property a_all: boolean read fall write fall;
		property a_ebene: integer read febene write febene;
    property a_tabelle: tzquery read fa_tab write fa_tab;
		property a_nummer:int64 read fnummer write fnummer;
		property a_storno: boolean read fstorno write fstorno;


		property tab1: tzquery read ftabellen[1] write ftabellen[1];
		property tab2: tzquery read ftabellen[2] write ftabellen[2];
		property tab3: tzquery read ftabellen[3] write ftabellen[3];
		property tab4: tzquery read ftabellen[4] write ftabellen[4];
		property tab5: tzquery read ftabellen[5] write ftabellen[5];
		property tab6: tzquery read ftabellen[6] write ftabellen[6];
		property key1: string read fkey[1] write fkey[1];
		property key2: string read fkey[2] write fkey[2];
		property key3: string read fkey[3] write fkey[3];
		property key4: string read fkey[4] write fkey[4];
		property key5: string read fkey[5] write fkey[5];
		property key6: string read fkey[6] write fkey[6];
		property anzeige_feld1: string read fanzeige_feld[1] write fanzeige_feld[1];
		property anzeige_feld2: string read fanzeige_feld[2] write fanzeige_feld[2];
		property anzeige_feld3: string read fanzeige_feld[3] write fanzeige_feld[3];
		property anzeige_feld4: string read fanzeige_feld[4] write fanzeige_feld[4];
		property anzeige_feld5: string read fanzeige_feld[5] write fanzeige_feld[5];
		property anzeige_feld6: string read fanzeige_feld[6] write fanzeige_feld[6];

     property tab_v: tzquery read ftab_v write ftab_v; //tabelle f�r frei plazierbaren Knoten
     property key_v: string read fkey_v write fkey_v;
     property anzeige_feld_v: string read fanzeige_feld_v write fanzeige_feld_v;
     property ebene_feld_v:string read febene_feld_v write febene_feld_v; //tab-feld mit der ebene
	  //procedure Changing(Node: TTreeNode; var AllowChange: Boolean); override;

	end;

type
		pnodepointer=^nodewert;
		nodewert = record
		nummer: int64;
		bereich:int64;
		ebene:int64;
    rubrik:int64;
    datum:tdate;
		mysqlquery:tzquery;
end;

procedure Register;

implementation

constructor Tdbtreeview_k.create(AOwner: TComponent);
begin
  change_allowed:=false;
  inherited;
  change_allowed:=true;

end;

destructor Tdbtreeview_k.Destroy;
begin
  liste_loeschen;
  inherited;
end;

procedure Tdbtreeview_k.liste_loeschen;
var i:integer;
daten:pnodepointer;
begin
change_allowed:=false;
	for i:=0 to items.Count-1 do
	begin
		daten:=items[i].data;
		dispose(daten);
	end;
	items.Clear;
change_allowed:=true;
end;

procedure Tdbtreeview_k.liste_lesen(ebene:integer);
var
t,stat: integer;
nr:int64;
nom:string;
knoten:ttreenode;
begin
change_allowed:=false;
try
    liste_loeschen;
    ftabellen[ebene].first;
    knoten:=nil;
    if fall then
    knoten:=parent_einfuegen(knoten,nil,0,'alle',-1);
    while not ftabellen[ebene].eof do
    begin
       nr:=getbigint(ftabellen[ebene],'nummer');
       if ftabellen[ebene][fanzeige_feld[ebene]]<>null
       then nom:=ftabellen[ebene][fanzeige_feld[ebene]] else nom :='';
       knoten:=parent_einfuegen(knoten,ftabellen[ebene],nr,nom,ebene);
       varknoten_einfuegen(knoten,nr,ebene);
       kinder_lesen(knoten,nr,ebene+1);

       ftabellen[ebene].next;
   end;
finally
change_allowed:=true;
end;
end;


procedure Tdbtreeview_k.kinder_lesen(knoten:ttreenode;nr:int64;ebene:integer);
var
q:tzquery;
nom:string;
nr_kind:int64;
knoten_kind:ttreenode;
begin
  if ftabellen[ebene]=nil then exit;
  //ftabellen[ebene].locate(fkey[ebene],inttostr(nr),[]);
  ftabellen[ebene].locate(fkey[ebene],(nr),[]);
  while not (ftabellen[ebene].eof or (getbigint(ftabellen[ebene],fkey[ebene])<>nr)) do
  begin
     nr_kind:=getbigint(ftabellen[ebene],'nummer');
     if ftabellen[ebene][fanzeige_feld[ebene]]<>null
     then  nom:=ftabellen[ebene][fanzeige_feld[ebene]] else nom:='';
     knoten_kind:=kind_einfuegen(knoten,ftabellen[ebene],nr_kind,nom,ebene,1);
     varknoten_einfuegen(knoten_kind,nr_kind,ebene);
     kinder_lesen(knoten_kind,nr_kind,ebene+1);

     ftabellen[ebene].next;
 end;
end;


function Tdbtreeview_k.gehezudatum(datum: tdate):ttreenode;
var
daten:pnodepointer;
//knoten:ttreenode;
procedure neuesdatum_davor;
begin
	result:=Items.Insert(result, datetostr(datum));      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=0;
	daten^.bereich:=0;
	daten^.ebene:=0;
	daten^.rubrik:=0;
	result.Data:=daten;
	result.imageIndex:=-1;
	result.SelectedIndex:=-1;
	result.StateIndex:=-1;
end;

procedure neuesdatum_dahinter;
begin
	result:=Items.add(result, datetostr(datum));      //tree_bereich
	new(daten);
	daten^.datum :=datum;
	daten^.nummer:=0;
	daten^.bereich:=0;
	daten^.ebene:=0;
	daten^.rubrik:=0;
	result.Data:=daten;
	result.imageIndex:=-1;
	result.selectedindex:=-1;
	result.StateIndex:=-1;
end;

begin //main
if items.count=0 then
begin
 result:=nil;
 neuesdatum_dahinter;
 exit;
end;

result:=items[0];

while true  do
begin
	 if result=nil then  //hinten angekommen
	 begin
	  neuesdatum_dahinter;
	  exit;
	 end;
	 daten:=result.Data;
	 if daten^.datum =datum then exit;
	 if daten^.datum <datum then
		begin
			neuesdatum_davor;
			exit;
		end;
	 result:=result.getnextSibling ;
end;
end;


procedure tdbtreeview_k.varknoten_einfuegen(knoten:ttreenode;nr:int64;ebene:integer);
var
nr_kind:int64;
nom:string;
begin
  if ftab_v=nil then exit;
  //if not ftab_v.locate(fkey_v+','+febene_feld_v,vararrayof([inttostr(nr),ebene]),[]) then exit;
  if not ftab_v.locate(fkey_v+','+febene_feld_v,vararrayof([(nr),ebene]),[]) then exit;
  while (getbigint(ftab_v,fkey_v)=nr) and (ftab_v[febene_feld_v]=ebene) and (not ftab_v.eof) do
  begin
       nr_kind:=getbigint(ftab_v,'nummer');
       if ftab_v[fanzeige_feld_v]<>null then nom:=ftab_v[fanzeige_feld_v] else nom:='Projekt';
       kind_einfuegen(knoten,ftab_v,nr_kind,nom,ebene,1);
       ftab_v.Next;
  end;
end;



function  Tdbtreeview_k.kind_einfuegen(knoten:ttreenode;tab:tzquery;nummer:int64;anzeigename:string;ebene,position:integer):ttreenode;
var
   daten:pnodepointer;
procedure new_child;
begin
  if position=0 then
     knoten:=Items.AddChildfirst(knoten,anzeigename)
  else
	 knoten:=Items.AddChild(knoten,anzeigename);
       //tree_bereich  addchildfirst
  knoten.StateIndex:=tab_nr(tab);     
	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
  if tab['datum']<>null then daten^.datum:=tab['datum'] else daten^.datum:=date;
	daten^.ebene:=ebene;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	//knoten.ImageIndex:=status;
	//knoten.selectedindex:=status;
	result:=knoten;
end;


procedure new_sibling_add;
begin
	knoten:=Items.AddChild(knoten,anzeigename);      //tree_bereich
	new(daten);
  knoten.StateIndex:=tab_nr(tab);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
	daten^.ebene:=ebene;
  if tab['datum']<>null then daten^.datum:=tab['datum'] else daten^.datum:=date;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	result:=knoten;
end;


begin

if knoten=nil then new_sibling_add else new_child;

end;


function  Tdbtreeview_k.parent_einfuegen(knoten:ttreenode;tab:tzquery;nummer:int64;anzeigename:string;ebene:integer):ttreenode;
var
   daten:pnodepointer;

begin
	//knoten:=Items.Insert(knoten,anzeigename);      //tree_bereich child_last
  knoten:=Items.Add(knoten,anzeigename);
  knoten.StateIndex:=tab_nr(tab);
	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=0;
	daten^.ebene:=ebene;
  if tab<>nil then if tab['datum']<>null then daten^.datum:=tab['datum']  else daten^.datum:=date;
	daten^.mysqlquery:=tab;
	knoten.Data:=daten;
	result:=knoten;
end;





function tdbtreeview_k.befunde_lesen(knoten:ttreenode;nummer:int64):ttreenode;
var
		 i_parent: integer;
		 child, child_first, parent: ttreenode;
		 daten:pnodepointer;
		 query:string;

function gehezu_parent(i_parent:integer):ttreenode;
begin
result:=child_first;
daten:=result.data;
while (result<>nil) do
begin
	daten:=result.data;
	if daten^.rubrik<>1 then
		begin
			result:=nil;
			exit;
		end;
	if daten^.bereich=i_parent then exit;
	result:=result.getnext;
end;
end;

procedure knotenmarkieren(knoten:ttreenode);

begin
	daten:=knoten.data;
	if not ftabelle_quelle.Locate('i_untersuchung,i_bereich',
				vararrayof([inttostr(daten^.nummer),inttostr(daten^.bereich)]),[]) then exit;
	knoten.StateIndex:=ftabelle_quelle['auffaellig']+13;//+10;//+9
	markiere_parent(knoten);
end;

var
and_storno:string;
begin //main #################################################################
try
	 //if fstorno then and_storno:='' else and_storno:=' and storno ="0"';

	 query:=format('select * from %s where i_untersuchung=%s and storno="0"' ,
			[ftabelle_quelle_name,inttostr(nummer)]);
	 ftabelle_quelle.Close;
	 ftabelle_quelle.Sql.Text:=query;
	 ftabelle_quelle.Open;


	fparentfeld:='';
	fkindfeld:='';
	ftabelle_bef_vorlage.first;
	if  ftabelle_bef_vorlage.Eof then exit;
	//erster datensatz von hand
	child_first:=Items.AddChildFirst(knoten,ftabelle_bef_vorlage[ffeldname]);      //tree_bereich

	new(daten);
	daten^.nummer:=nummer;
	daten^.bereich:=getbigint(ftabelle_bef_vorlage,'nummer');
	daten^.rubrik:=1;
	daten^.ebene:=2;
	child_first.Data:=daten;
	child_first.SelectedIndex:=-1;
	child_first.imageindex:=-1;
	child_first.StateIndex:=-1;
	result:=child_first;
	knotenmarkieren(child_first);
	ftabelle_bef_vorlage.Next;

	while not ftabelle_bef_vorlage.eof do
	begin
		 i_parent:= ftabelle_bef_vorlage['parent'];
		 parent:=gehezu_parent(i_parent);
		 if parent<>nil then
			 child:=Items.Addchild(parent, ftabelle_bef_vorlage[ffeldname])
		 else
			 child:=Items.Addchild(knoten, ftabelle_bef_vorlage[ffeldname]);
		 new(daten);
		 daten^.nummer:=nummer;
		 daten^.bereich:=getbigint(ftabelle_bef_vorlage,'nummer');
		 daten^.rubrik:=1;
		 daten^.ebene:=2;
		 child.Data:=daten;
		 child.SelectedIndex:=-1;
		 child.imageindex:=-1;
		 knotenmarkieren(child);
		 ftabelle_bef_vorlage.Next;
	 end;


	 if fstorno then // gel�schte Datens�tze
	 begin
		query:=format('select * from %s where i_untersuchung=%s and storno="1"' ,
			  [ftabelle_quelle_name,inttostr(nummer),and_storno]);
		ftabelle_quelle.Close;
		ftabelle_quelle.Sql.Text:=query;
		ftabelle_quelle.Open;
		ftabelle_quelle.first;
		while not ftabelle_quelle.eof do
		begin

			ftabelle_quelle.next;
		end;
	 end;
finally

end;
end;




function tdbtreeview_k.knoten_entfernen(knoten:ttreenode):ttreenode;
var daten:pnodepointer;
	  parent:ttreenode;
begin
  try
  result:=knoten.parent;
  except
  result:=items[0];      
  end;
	daten:=knoten.data;
	dispose(daten);
	parent:=knoten.Parent;
	knoten.Free;

end;




procedure Tdbtreeview_k.Change(Node: TTreeNode);
var
nummer,bereich: int64;
rubrik,i:integer;
daten,d1:pnodepointer;
query:string;
and_storno:string;
begin

febene:=-1;
try
	 if (not change_allowed) or (node=nil) then exit;
   daten:=node.Data;
   febene:=daten^.ebene;
   fa_tab:=daten^.mysqlquery;
   if fa_tab=nil then
   begin
    inherited;
    exit; // alle
   end;
   i:=febene;
   akt_knoten[i]:=node;
   fnummer:=daten^.nummer;
   node.Expand(false);
   //fa_tab.Locate('nummer',inttostr(fnummer),[]);
   fa_tab.Locate('nummer',(fnummer),[]);

   i:=i-1;
   while i>0 do    //positionieren der �bergeordnente Ebenen
   begin
        akt_knoten[i]:=akt_knoten[i+1].parent;
        if akt_knoten[i]=nil then exit;
        d1:=akt_knoten[i].data;
        nummer:=d1^.nummer;
        //ftabellen[i].Locate('nummer',inttostr(nummer),[]);
        ftabellen[i].Locate('nummer',(nummer),[]);
        i:=i-1;
   end;

   if (not change_allowed) then exit;

except
end;
inherited;
end;

procedure tdbtreeview_k.name_neu(node:ttreenode);
var
nummer: int64;
ebene:integer;
daten:pnodepointer;
tab:tzquery;
begin
if not change_allowed then exit;
if node=nil then exit; //funktioniert nicht richtig, keine priorit�t

  daten:=node.Data;
  if daten=nil then exit; //funktioniert nicht richtig, keine priorit�t

  nummer:=daten^.nummer;
  ebene:=daten^.ebene;
  if ebene=-1 then exit; //f�r alle
  if getbigint(fa_tab,'nummer')<>nummer then exit;
  if fa_tab<>ftab_v then
    begin
         if ftabellen[ebene][fanzeige_feld[ebene]]<>null then node.Text:=ftabellen[ebene][fanzeige_feld[ebene]];
    end
  else
    if ftab_v[fanzeige_feld_v]<>null then node.Text:=ftab_v[fanzeige_feld_v];

end;

procedure Tdbtreeview_k.markieren(sindex:integer);
var i:integer;

function hasparent(p,f:integer):boolean;
	var j:integer;
	begin
		j:=f;
		while (items[j].parent<>nil) and(j>p) do
		begin
			j:=items[j].parent.absoluteindex;
		end;
		if j=p then result:=true else result:=false;

	end;

begin //main
	selected.StateIndex:=sindex;
	i:=selected.absoluteindex;
	//knoten:=selected;
	inc(i);
	//while (i<items.count) and(items[i].parent=selected) do
	while (i<items.count) and hasparent(selected.absoluteindex,i) do
	begin
	  items[i].StateIndex:=sindex;
	  inc(i);
	end;
end;


procedure Tdbtreeview_k.alles_markieren(sindex:integer);
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=sindex;
	end;
end;

procedure Tdbtreeview_k.statindex(sindex:integer);
begin
		selected.stateindex:=sindex;
end;

procedure Tdbtreeview_k.alles_demarkieren;
var i:integer;
begin
	for i:=0 to items.count-1 do
	begin
		items[i].stateindex:=-1;
	end;
end;

procedure Tdbtreeview_k.runter;
begin
	if selected.absoluteindex >0 then  selected:=items[selected.absoluteindex-1];
end;

procedure Tdbtreeview_k.hoch;
begin
	if selected.absoluteindex <items.count-1 then  selected:=items[selected.absoluteindex+1];
end;

function Tdbtreeview_k.getbigint(tabelle: tdataset;feldname:string):int64;
var
r:string;
begin
try
result:=0;
if tabelle.findfield(feldname)<>nil then
  r:= tabelle.FindField(feldname).AsString;
except
end;
if r='' then result:=0 else result:=strtoint64(r);
end;

function Tdbtreeview_k.gehe_zu_datensatz(ebene:integer;nummer:int64): boolean;
var knoten: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
if items.count=0 then exit;
knoten:=items[0];
while knoten<> nil do
begin
	daten:=knoten.data;
	if (daten<>nil) and (daten^.nummer=nummer) and (daten^.ebene=ebene) then
	begin
	  selected:=knoten;
	  result:=true;
    //selected.Focused:=true;
	  exit;
	end;
	knoten:=knoten.getNext;
end;
except
end;
end;


function Tdbtreeview_k.gehe_zu_datensatz_zquery(zquery:tzquery;nummer:int64): boolean;
var knoten: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
if items.count=0 then exit;
knoten:=items[0];
while knoten<> nil do
begin
	daten:=knoten.data;
	if (daten<>nil) and (daten^.nummer=nummer) and (daten^.mysqlquery=zquery) then
	begin
	  selected:=knoten;

	  result:=true;
	  exit;
	end;
	knoten:=knoten.getNext;

end;
except
end;
end;


function Tdbtreeview_k.suche_datensatz(wort:string): boolean;
var knoten,k_start: ttreenode;
	  daten:pnodepointer;
	  mess:string;
begin
try
result:=false;
wort:=lowercase(wort);
if (items.count<=1) or (wort='') then exit;
if selected=nil then selected:=items[0];
k_start:=selected;
knoten:=selected.GetNext;
if knoten=nil then knoten:=items[0];
while knoten<> k_start do
begin
	if  pos( wort,lowercase(knoten.Text))>0 then
	begin
	  selected:=knoten;
	  result:=true;
	  exit;
	end;
	knoten:=knoten.getNext;
  if knoten=nil then knoten:=items[0];
end;
except
end;
end;

procedure tdbtreeview_k.Expand_e1;
var knoten,sel: ttreenode;
begin
if items.count=0 then exit;
sel:=selected;
knoten:=items[0];
while knoten<> nil do
begin
	knoten.Expand(false);
	knoten:=knoten.getNextSibling;
end;
 if sel<>nil then selected:=sel else  selected:=items[0];
end;


procedure tdbtreeview_k.markiere_parent(knoten:ttreenode);
var
si:integer;
daten:pnodepointer;
begin
try
	si:=knoten.StateIndex;
	knoten:=knoten.Parent;
	while knoten<>nil do
	begin
	  daten:=knoten.Data;
	  if daten^.ebene<=1 then exit; //datum, befund
	  if knoten.StateIndex<si then knoten.StateIndex:=si;
	  knoten:=knoten.Parent;
	end;
except
end;
end;



{procedure tdbtreeview_k.DragDrop( Source: TObject; X, Y: Integer);
var
   daten_sender, daten_source,d:pnodepointer;
   e_sender, e_source:integer;
   nr:int64;
  AnItem,p: TTreeNode;
  AttachMode: TNodeAttachMode;
  HT: THitTests;
begin
inherited;
try
  if Selected = nil then Exit;
  HT := GetHitTestInfoAt(X, Y);
  AnItem := GetNodeAt(X, Y);
  if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent] <> HT) then
  begin
    daten_sender:=anitem.data;
    e_sender:=daten_sender^.ebene;
    daten_source:=selected.data;
    e_source:=daten_source^.ebene;

    case e_source of
    1: begin
           exit;
       end;
    2: begin
        case e_sender of
         1:begin
             AttachMode := naAddChild;
             nr:=daten_sender^.nummer;
           end;
         2:begin
             AttachMode := naAdd;
             p:=anitem.parent;
             d:=p.data;
             nr:=d^.nummer;
           end;
         3: exit;
        end;
    end;
    3: begin
         case e_sender of
         1: exit;
         2:begin
             AttachMode := naAddChild;
             nr:=daten_sender^.nummer;
           end;
         3:begin
             AttachMode := naAdd;
             p:=anitem.parent;
             d:=p.data;
             nr:=d^.nummer;
           end;

         end;
       end;
    end;

    if selected.parent<>anitem then
    begin
      ftabellen[e_source].edit;
      ftabellen[e_source].FindField(fkey[e_source]).AsString := inttostr(nr);
      ftabellen[e_source].post;
      Selected.MoveTo(AnItem, AttachMode);
    end;
    //if (htOnItem in HT) or (htOnIcon in HT) then AttachMode := naAddChild

    //else if htNowhere in HT then AttachMode := naAdd
    //else if htOnIndent in HT then AttachMode := naInsert;

    //Selected.MoveTo(AnItem, AttachMode);
  end;
finally
       inherited;
end;
end; }

function tdbtreeview_k.tab_nr(tab:tzquery):integer;
var i:integer;
begin
result:=0;
for i:=1 to 6 do
begin
     if tab=ftabellen[i] then
     begin
          result:=i;
          exit;
     end;
     if tab=ftab_v then result :=7;
end;


end;


procedure tdbtreeview_k.dragdrop(Source: TObject; X, Y: Integer);
var
	AnItem: TTreeNode;
	AttachMode: TNodeAttachMode;
	HT: THitTests;
	num, i_nummer,p_nummer,num1,num2:int64;
	data:pnodepointer;
  tabelle:tzquery;
  ebene,rf,e1,e2,j:integer;
  drag,nknoten,eknoten:ttreenode;
  parentfeld:string;
begin
  change_allowed:=false;
  dragmode:=dmmanual;
  drag:=selected;
	HT := GetHitTestInfoAt(X, Y);
	AnItem := GetNodeAt(X, Y);
  data:=drag.Data;
  e1:=data^.ebene;
  num1:=data^.nummer;
  data:=anitem.Data;
  e2:=data^.ebene;
  num2:=data^.nummer;
	attachMode:=naAddChild; //zur Definition
 if source is ttreeview then
 begin
 change_allowed:=false;
	//if (Selected = nil) or (selected=anitem) then Exit;
	if (drag <> nil) and (drag<>anitem) then
	begin
		  if (HT - [htOnItem, htOnIcon, htNowhere, htOnIndent, htonright, htonlabel,htonbutton] <> HT) then  //Mengenlehre
			 begin

              if Messagedlg('Soll der Datensatz verschoben werden?',mtConfirmation, [mbOK,mbcancel],0)=mrok then
              begin

                 // data:=anitem.data;
                 // num:=data^.nummer;
                 // ebene:=data^.ebene;

                 tabelle:=ftabellen[e1];
                 parentfeld:=fkey[e1];

                 if e1=e2 then
                 begin
                    tabelle.locate('nummer',(num2),[]);
                    if tabelle.FindField(parentfeld)<>nil then p_nummer:=strtoint64(tabelle.findfield(parentfeld).asstring) else p_nummer:=0;
                    rf:=tabelle.findfield('reihenfolge').asinteger;
                    if rf=0 then rf:=1;
                    //
                    {
                    j:=rf;
                    inc(j);
                    nknoten:=anitem;

                    while nknoten<>nil do
                    begin

                      data:=nknoten.data;
                      num:=data^.nummer;
                      ebene:=data^.ebene;
                      tabelle.Locate('nummer',num,[]);
                      tabelle.edit;
                      tabelle.findfield('reihenfolge').asinteger:=j;// tabelle.findfield('reihenfolge').asinteger+1;
                       tabelle.Post;
                       inc(j);
                      nknoten:=nknoten.getNextSibling;
                    end;
                    //
                    }


                    AttachMode := naInsert;
                   end
                 else   //e1<>e2
                 begin
                    p_nummer:=num2;
                    rf:=0;
                   attachMode:=naAddChild;
                 end;
                   drag.MoveTo(AnItem, AttachMode);
                    nknoten:=anitem;
                    while nknoten<>nil do
                    begin
                     eknoten:=nknoten;
                     nknoten:=nknoten.getPrevSibling;
                    end;
                    nknoten:=eknoten;
                    j:=1;
                    while nknoten<>nil do
                    begin

                      data:=nknoten.data;
                      num:=data^.nummer;
                      ebene:=data^.ebene;
                      tabelle.Locate('nummer',num,[]);
                      tabelle.edit;
                      tabelle.findfield('reihenfolge').asinteger:=j;// tabelle.findfield('reihenfolge').asinteger+1;
                       tabelle.Post;
                       inc(j);
                      nknoten:=nknoten.getNextSibling;
                    end;

                   tabelle.locate('nummer',(num1),[]);
                   tabelle.edit;
                   if tabelle.FindField(parentfeld)<>nil then tabelle.FindField(parentfeld).asstring:=inttostr(p_nummer);
                   tabelle.findfield('reihenfolge').asinteger:=rf; //
                   tabelle.Post;
                   //reihenfolge_neu(tabelle,parentfeld);      //
             end;
			 end;
	 end;

	end;

	drag:=nil;
	inherited ;
  dragmode:=dmautomatic;
  change_allowed:=true;
  change(selected);
end;

procedure tdbtreeview_k.DragOver(Source: TObject; X,Y: Integer; State: TDragState; var Accept: Boolean);
var
sen,drag:ttreenode;
e1,e2:integer;
daten:pnodepointer;
begin

accept:=false;
if not change_allowed then exit;
 if (source is tdbtreeview_k) then
	 begin
     sen:=GetNodeAt(X, Y);
     daten:=sen.Data;
     e2:=daten^.ebene;
     drag:=selected;
     daten:=drag.Data;
     e1:=daten^.ebene;
     if (sen<>nil) and (sen<>selected) and ((e1=e2) or (e2=e1-1)) then accept:=true;
     if y<10 then topitem:=topitem.GetPrev  ;
     if (y>(height-10)) then topitem:=topitem.getnext;
  end;
inherited;  
end;


function tdbtreeview_k.get_index(tab_ind:integer):int64  ;
var
daten:pnodepointer;
ebene:integer;
begin
  daten:=selected.data;
   ebene:=daten^.ebene;
   result:=-1;
  if (ebene>0) and (ebene>=tab_ind) then
  try
      result:=getbigint(ftabellen[tab_ind],'nummer');
  except
      result:=-1;
  end;
end;


procedure Register;
begin
	RegisterComponents('Datensteuerung', [Tdbtreeview_k]);
end;

end.
