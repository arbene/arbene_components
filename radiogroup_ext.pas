unit radiogroup_ext;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	ExtCtrls,stdctrls,spin;

type
	TRadiogroup_ext = class(TRadiogroup)
	private
  //Tondblclick = procedure(Sender:TObject) of object;

  fOndblClick: TNotifyEvent;
  procedure WMNCLBUTTONDBLCLK(var msg: TMessage);
  published
  property OndblClick: TNotifyEvent read fOndblClick write fOndblClick;

end;

procedure Register;

implementation

procedure TRadiogroup_ext.WMNCLBUTTONDBLCLK(var msg: TMessage);
begin
 // Caption := ' Click!';
  // Message.Result := 0; {to ignore the message}
  if assigned(fOndblClick) then fOndblClick(self);
  inherited;
end;


procedure Register;
begin
	RegisterComponents('Beispiele', [TRadiogroup_ext]);
end;
end.
 